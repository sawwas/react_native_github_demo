import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';
import reducers from '../reducer';

//============================================redux的自定义中间件======================================
/**
 * 自定义
 * @param store
 * @returns {function(*): function(...[*]=)}
 */
const logger = store => next => action => {
  //如果action 类型是function
  if (typeof action === 'function') {
    console.log('dispatching a function'); //打印function
  } else {
    console.log('dispatching', action); //打印action
  }
  const result = next(action);
  console.log('nextState', store.getState()); //打印下一次的状态state
  return result;
};
//============================================redux的自定义中间件======================================

const middlewares = [thunk, logger];

/**
 * 2 创建store
 */
export default createStore(reducers, applyMiddleware(...middlewares));
