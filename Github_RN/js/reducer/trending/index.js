import Types from '../../action/types';

/**
 * 1 创建一个reducer  action 函数
 */
const defaultState = {}; //state 默认值 必须创建 否则报错显示不合法的reducer
/**
 * TRENDING:{
 *     java:{
 *         items:[],
 *         isLoading:false
 *     },
 *     ios:{
 *         items:[],
 *         isLoading:false
 *     },
 *
 * }
 * @param state
 * @param action
 * @returns {{}|{theme: *}}
 */
export default function onAction(state = defaultState, action) {
  switch (action.type) {
    case Types.TRENDING_REFRESH_SUCCESS: //下拉刷新成功
      //不能修改state ，只能返回新的state 或之前的state
      return {
        ...state,
        //动态设置store（storekey 不固定）
        [action.storeName]: {
          ...state[action.storeName], //====================!!!===========================...延展修饰符 必须使用 否则每次刷新会白一下=================
          items: action.items, //========!!!!!!!!下拉刷新默认返回所有数据，为了在加载更多能取到所有数据，需要携带items*（）原始数据
          projectModels: action.projectModels, //========!!!!!!!!*（）此次要展示的数据
          isLoading: false,
          hideLoadingMore: false, //是否隐藏底部的加载更多
          pageIndex: action.pageIndex, //===============！！！======== 用于接收TRENDINGPage 设置defaultState 时传入的pageIndex，否则NAN
        },
      }; //不能修改state ，只能返回新的state 或之前的state

    case Types.TRENDING_REFRESH: //下拉刷新中
      return {
        ...state,
        [action.storeName]: {
          ...state[action.storeName],
          isLoading: true,
          //=======修复上拉刷新调用2次和第一次进入页面时不用触发上拉加载 loadMore为true的问题--伴随reducer修改========================
          hideLoadingMore: true, //=========！！！==========!!! 正在下拉刷新时必须隐藏 上拉加载ui
        },
      }; //复制之前的节点副本 做一个mergin 新的覆盖老的
    case Types.TRENDING_REFRESH_FAIL: //下拉刷新失败
      return {
        ...state,
        [action.storeName]: {
          ...state[action.storeName],
          isLoading: false,
          hideLoadingMore: true, //=========！！！==========!!! 下拉刷新失败时必须隐藏 上拉加载ui
        },
      }; //复制之前的节点副本 做一个mergin 新的覆盖老的

    case Types.TRENDING_LOAD_MORE_SUCCESS: //上拉加载更多成功
      //不能修改state ，只能返回新的state 或之前的state
      return {
        ...state,
        //动态设置store（storekey 不固定）
        [action.storeName]: {
          ...state[action.storeName], //====================!!!===========================...延展修饰符 必须使用 否则每次刷新会白一下=================
          projectModels: action.projectModels,
          hideLoadingMore: false, //是否隐藏底部的加载更多
          pageIndex: action.pageIndex, //===============！！！======== 用于接收TRENDINGPage 设置defaultState 时传入的pageIndex，否则NAN
        },
      }; //不能修改state ，只能返回新的state 或之前的state

    case Types.TRENDING_LOAD_MORE_FAIL: //上拉加载更多失败
      //不能修改state ，只能返回新的state 或之前的state
      return {
        ...state,
        //动态设置store（storekey 不固定）
        [action.storeName]: {
          ...state[action.storeName], //====================!!!===========================...延展修饰符 必须使用 否则每次刷新会白一下=================
          hideLoadingMore: true, //是否隐藏底部的加载更多
          pageIndex: action.pageIndex, //===============！！！======== 用于接收TRENDINGPage 设置defaultState 时传入的pageIndex，否则NAN
        },
      }; //不能修改state ，只能返回新的state 或之前的state

    case Types.TRENDING_FLUSH_FAVORITE: //刷新收藏状态发生变化时同步到趋势页面
      return {
        ...state,
        [action.storeName]: {
          ...state[action.storeName],
          projectModels: action.projectModels,
        },
      };

    default:
      return state;
  }
}
