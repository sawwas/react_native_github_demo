import {
  BackHandler,
  Platform,
  View,
  Image,
  Text,
  Dimensions,
  StyleSheet,
  DeviceInfo,
} from 'react-native';
import BackPressComponent from '../../common/BackPressComponent';
import NavigationUtil from '../../navigator/NavigationUtil';
import config from '../../res/data/config';
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import GlobalStyles from '../../res/styles/GlobalStyles';
import React from 'react';
import ViewUtil from '../../util/ViewUtil';
import ShareUtil from '../../util/ShareUtil';
import share from '../../res/data/share.json';

const THEME_COLOR = '#678';

export const FLAG_ABOUT = {flag_about: 'about', flag_about_me: 'about_me'};

const window = Dimensions.get('window');
const AVATAR_SIZE = 90; //头像大小
const PARALLAX_HEADER_HEIGHT = 270;

//==============================沉浸式顶部导航栏样式===================================
const TOP =
  Platform.os === 'ios' ? 20 + (DeviceInfo.isIPhoneX_deprecated ? 24 : 0) : 35;

const STICKY_HEADER_HEIGHT =
  Platform.OS === 'ios'
    ? GlobalStyles.nav_bar_height_ios + TOP
    : GlobalStyles.nav_bar_height_android + 30;
//==============================沉浸式顶部导航栏样式===================================

//==============返回监听==============
function onBack(ts) {
  //===============返回堆栈的上一页================
  NavigationUtil.goBack(ts.props.navigation);
}

//==============返回监听==============

//==============关于页面==============
export default class AboutCommon {
  constructor(props, updateState) {
    this.props = props;
    this.updateState = updateState;
    this.backPress = new BackPressComponent({backPress: this.onBackPress});
    //=================================加载本地json============================
    // this.updateState({
    //   config,
    // });
    //=================================加载本地json============================
  }
  //=================================分享============================
  onShare() {
    //=================================第三方社会化分享==============================
    let shareApp = share.share_app;
    this.url = 'https://www.baidu.com';

    ShareUtil.shareboard(
      shareApp.content,
      shareApp.imgUrl,
      this.url,
      shareApp.title,
      [0, 1, 2, 3, 4, 5, 6],
      (code, message) => {
        console.log('result:' + code + message);
      },
    );
    //=================================第三方社会化分享==============================
  }
  //=================================分享============================

  /**
   * 处理 Android 中的物理返回键
   * https://reactnavigation.org/docs/en/redux-integration.html#handling-the-hardware-back-button-in-android
   * @returns {boolean}
   */
  onBackPress() {
    onBack(this);
    return true; //===============================防止返回事件继续传递下去=============！！！！！！！！！！！！！！！
  }

  componentDidMount() {
    this.backPress.componentDidMount();
    //=================================加载网络json============================

    fetch('http://www.devio.org/io/GitHubPopular/json/github_app_config.json')
      .then(response => {
        if (response.ok) {
          return response.json();
        }
        throw new Error('Network error');
      })
      .then(data => {
        if (data) {
          this.updateState({
            //将数据传出
            data: data,
          });
        }
      })
      .catch(e => {
        console.log(e);
      });
    //=================================加载网络json============================
  }

  componentWillUnmount() {
    this.backPress.componentWillUnmount();
  }
  //=================================加载网络json============================

  getParallaxRenderConfig(params) {
    let config = {};
    //================判断头像是否是string============
    let avatar =
      typeof params.avatar === 'string' ? {uri: params.avatar} : params.avatar;
    //================判断头像是否是string============

    //底部的图片
    config.renderBackground = () => (
      <View key="background">
        <Image
          source={{
            uri: params.backgroundImg,
            width: window.width,
            height: PARALLAX_HEADER_HEIGHT,
          }}
        />
        {/*黑色蒙版*/}
        <View
          style={{
            position: 'absolute',
            top: 0,
            width: window.width,
            backgroundColor: 'rgba(0,0,0,.4)',
            height: PARALLAX_HEADER_HEIGHT,
          }}
        />
      </View>
    );
    //前景图片         {/*头像布局*/}  //头像图片 //标题  //描述
    config.renderForeground = () => (
      <View key="parallax-header" style={styles.parallaxHeader}>
        <Image style={styles.avatar} source={avatar} />
        <Text style={styles.sectionSpeakerText}>{params.name}</Text>
        <Text style={styles.sectionTitleText}>{params.description}</Text>
      </View>
    );
    //悬停的标题
    config.renderStickyHeader = () => (
      <View key="sticky-header" style={styles.stickySection}>
        <Text style={styles.stickySectionText}>{params.name}</Text>
      </View>
    );
    config.renderFixedHeader = () => (
      <View key="fixed-header" style={styles.fixedSection}>
        {/*  左上角按钮*/}
        {ViewUtil.getLeftBackButton(() =>
          NavigationUtil.goBack(this.props.navigation),
        )}
        {/*  右上角按钮*/}
        {ViewUtil.getShareButton(() => this.onShare())}
      </View>
    );
    return config;
  }
  //=================================加载网络json============================

  render(contentView, params) {
    const renderConfig = this.getParallaxRenderConfig(params);
    return (
      <ParallaxScrollView
        backgroundColor={THEME_COLOR}
        contentBackgroundColor={GlobalStyles.backgroundColor}
        parallaxHeaderHeight={PARALLAX_HEADER_HEIGHT} // 整体高度
        stickyHeaderHeight={STICKY_HEADER_HEIGHT - 85} // ！！！！！！！！！！！！！！！！！！！！！！！！！顶部悬浮的高度
        backgroundScrollSpeed={10} //前景滚动速度
        {...renderConfig} //ES7 解构赋值 呈现ui
      >
        {contentView}
      </ParallaxScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
  },
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: window.width,
    height: PARALLAX_HEADER_HEIGHT,
  },
  stickySection: {
    //！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！悬停标题的样式
    height:
      Platform.OS === 'ios'
        ? GlobalStyles.nav_bar_height_ios - 20
        : GlobalStyles.nav_bar_height_android - 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    // marginTop: 35,
    marginTop: TOP, //!!!!!!!!!!!!!!!
  },
  stickySectionText: {
    color: 'white',
    fontSize: 20,
    margin: 10,
    marginTop: 0,
  },
  fixedSection: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    paddingRight: 8,
    paddingTop: Platform.OS === 'ios' ? TOP : TOP - 10, //~~~~~~~~!!!!!!!!!!!!!!!!!!!!!
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  fixedSectionText: {
    color: '#999',
    fontSize: 20,
  },
  parallaxHeader: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
    paddingTop: 30,
  },
  avatar: {
    marginTop: 5,
    marginBottom: 10,
    width: AVATAR_SIZE,
    height: AVATAR_SIZE,
    borderRadius: AVATAR_SIZE / 2,
  },
  sectionSpeakerText: {
    color: 'white',
    fontSize: 24,
    paddingVertical: 5,
    marginBottom: 10,
  },
  sectionTitleText: {
    color: 'white',
    fontSize: 16,
    marginRight: 10,
    marginLeft: 10,
  },
});
