export default class NavigationUtil {
  //FIX DynamicTabNavigator中的页面无法跳转到外层导航器页面的问题
  static navigation = '';
  /**
   * 重置到首页  传入导航器
   * @param params
   */
  static resetToHomePage(params) {
    const {navigation} = params;
    navigation.navigate('Main');
  }

  /**
   * 跳转到指定页面
   * @param params 要传递的参数
   * @param page 要传递的页面名 ————路由名
   */
  static goPage(params, page) {
    const navigation = NavigationUtil.navigation;
    if (!navigation) {
      console.log('NavigationUtil.navigation can not be null');
    }
    navigation.navigate(page, {
      ...params,
    });
  }
  /**
   * 返回上一页
   * @param navigation
   */
  static goBack(navigation) {
    navigation.goBack();
  }
}
