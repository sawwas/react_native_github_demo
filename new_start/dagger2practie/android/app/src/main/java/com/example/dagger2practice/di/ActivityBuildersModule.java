package com.example.dagger2practice.di;

import com.example.dagger2practice.di.auth.AuthModule;
import com.example.dagger2practice.di.auth.AuthViewModelsModule;
import com.example.dagger2practice.di.main.MainFragmentBuildersModule;
import com.example.dagger2practice.di.main.MainViewModelsModule;
import com.example.dagger2practice.ui.auth.AuthActivity;
import com.example.dagger2practice.ui.main.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuildersModule {

//    @Binds
//    @IntoMap
//    @ClassKey(AuthActivity.class)
//    abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
//           AuthActivitySubcomponent.Factory builder);
//
//    @Subcomponent(modules = AuthViewModelsModule.class)
//    public interface AuthActivitySubcomponent extends AndroidInjector<AuthActivity> {
//        @Subcomponent.Factory
//        interface Factory extends AndroidInjector.Factory<AuthActivity> {
//        }
//    }

    @ContributesAndroidInjector(
            modules = {
                    AuthViewModelsModule.class,
                    AuthModule.class
            }
    )
    abstract AuthActivity contributeAuthActiity();

    @ContributesAndroidInjector(
            modules = {
                    MainFragmentBuildersModule.class,
                    MainViewModelsModule.class
            }
    )
    abstract MainActivity contributeMainActivity();

}
