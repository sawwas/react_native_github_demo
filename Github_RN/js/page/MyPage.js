/**
 *我的
 */
import React, {Component} from 'react';
import {
  ScrollView,
  Button,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import actions from '../action';
import {connect} from 'react-redux';
import NavigationUtil from '../navigator/NavigationUtil';
import NavigationBar from '../common/NavigationBar';
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {PlatformSelects} from '../utils/PlatformSelects';
import {MORE_MENU} from '../common/MORE_MENU';
import GlobalStyles from '../res/styles/GlobalStyles';
import ViewUtil from '../util/ViewUtil';
import {FLAG_LANGUAGE} from '../expand/dao/LanguageDao';
let ts = '';

function onClick(menu, theme) {
  let RouteName, //将要跳转到的页面名称
    params = {};
  params.theme = theme; //========================!!!!!!!!!!!!将自定义主题属性传入

  switch (menu) {
    case MORE_MENU.Tutorial: //=================跳转H5
      RouteName = 'WebViewPage';
      params.title = '教程';
      params.url = 'http://www.hao123.com';
      break;
    case MORE_MENU.About: //=================跳转关于
      RouteName = 'AboutPage';
      break;
    case MORE_MENU.About_Author: //=================跳转关于作者
      RouteName = 'AboutMePage';
      break;
    case MORE_MENU.Custom_Key: //=================跳转到自定义标签
    case MORE_MENU.Custom_Language: //=================跳转到自定义语言
    case MORE_MENU.Remove_Key: //=================跳转到标签移除
      params.isRemoveKey = menu === MORE_MENU.Remove_Key; //是否是标签移除
      params.flag =
        menu !== MORE_MENU.Custom_Language
          ? FLAG_LANGUAGE.flag_key
          : FLAG_LANGUAGE.flag_language;
      RouteName = 'CustomKeyPage';
      break;
    case MORE_MENU.Sort_Key: //==================跳转到排序标签
      RouteName = 'SortKeyPage';
      params.flag = FLAG_LANGUAGE.flag_key;
      break;
    case MORE_MENU.Sort_Language: //==================跳转到排序语言
      RouteName = 'SortKeyPage';
      params.flag = FLAG_LANGUAGE.flag_language;
      break;
    case MORE_MENU.Custom_Theme: //==============弹出自定义主题
      const {onShowCustomThemeView} = ts.props; // 调用action创建函数改变visible状态
      onShowCustomThemeView(true);
      break;

    case MORE_MENU.CodePush: //================跳转热更页面
      RouteName = 'CodePushPage';
      break;
  }
  if (RouteName) {
    NavigationUtil.goPage(params, RouteName);
  }
}

class MyPage extends Component {
  constructor(props) {
    super(props);
    ts = this;
  }

  getItem(menu) {
    const {theme} = this.props; //自定义主题
    return ViewUtil.getMenuItem(
      () => onClick(menu, theme),
      menu,
      theme.themeColor,
    );
  }
  render():
    | React.ReactElement<any>
    | string
    | number
    | {}
    | React.ReactNodeArray
    | React.ReactPortal
    | boolean
    | null
    | undefined {
    const {navigation} = this.props;
    //==================订阅自定义主题===============
    const {theme} = this.props;
    //==================订阅自定义主题===============
    let statusBar = {
      backgroundColor: theme.themeColor,
      barStyle: 'light-content',
    };
    let navigationBar = (
      <NavigationBar
        title={'我的'}
        statusBar={statusBar}
        style={theme.styles.navBar}
      />
    );

    return (
      <View
        style={
          PlatformSelects()
            ? styles.containerForAndroid
            : styles.containerForAndroid
        }>
        {navigationBar}

        <ScrollView style={GlobalStyles.root_container}>
          <TouchableOpacity
            onPress={() => onClick(MORE_MENU.About, this)}
            style={styles.item}>
            <View style={styles.about_left}>
              <Ionicons
                name={MORE_MENU.About.icon}
                size={40}
                style={{marginRight: 10, color: theme.themeColor}}
              />

              <Text>Github popular</Text>
            </View>
            <Ionicons
              name={'ios-arrow-forward'}
              size={16}
              style={{
                marginRight: 10,
                alignSelf: 'center', //自己内部的居中方式
                color: theme.themeColor,
              }}
            />
          </TouchableOpacity>
          <View style={GlobalStyles.line} />
          {/*教程*/}
          {this.getItem(MORE_MENU.Tutorial)}
          {/*趋势管理*/}
          <Text style={styles.groupTitle}>趋势管理</Text>
          {/*自定义语言*/}
          {this.getItem(MORE_MENU.Custom_Language)}
          {/*语言排序*/}
          <View style={GlobalStyles.line} />
          {this.getItem(MORE_MENU.Sort_Language)}

          {/*最热管理*/}
          <Text style={styles.groupTitle}>最热管理</Text>
          {/*自定义标签*/}
          {this.getItem(MORE_MENU.Custom_Key)}
          {/*标签排序*/}
          <View style={GlobalStyles.line} />
          {this.getItem(MORE_MENU.Sort_Key)}
          {/*标签移除*/}
          <View style={GlobalStyles.line} />
          {this.getItem(MORE_MENU.Remove_Key)}

          {/*设置*/}
          <Text style={styles.groupTitle}>设置</Text>
          {/*自定义主题*/}
          {this.getItem(MORE_MENU.Custom_Theme)}
          {/*关于作者*/}
          <View style={GlobalStyles.line} />
          {this.getItem(MORE_MENU.About_Author)}
          <View style={GlobalStyles.line} />
          {/*反馈*/}
          {this.getItem(MORE_MENU.Feedback)}
          <View style={GlobalStyles.line} />
          {/*热更*/}
          {this.getItem(MORE_MENU.CodePush)}
        </ScrollView>

        {/*修改主题 fetch AsyncStorage DataStorage*/}
        {/*<View style={styles.container}>*/}
        {/*  <Text style={styles.welcome}>MyPage</Text>*/}
        {/*  <Button*/}
        {/*    title={'修改主题'}*/}
        {/*    onPress={() => this.props.onThemeChange('#821')}*/}
        {/*  />*/}
        {/*  <Button*/}
        {/*    title={'fetch 使用'}*/}
        {/*    onPress={() => {*/}
        {/*      NavigationUtil.goPage({}, 'FetchDemoPage');*/}
        {/*    }}*/}
        {/*    FetchDemoPage*/}
        {/*  />*/}
        {/*  <Button*/}
        {/*    title={'AsyncStorage 使用'}*/}
        {/*    onPress={() => {*/}
        {/*      NavigationUtil.goPage({}, 'AsyncStorageDemoPage');*/}
        {/*    }}*/}
        {/*    FetchDemoPage*/}
        {/*  />*/}
        {/*  <Button*/}
        {/*    title={'DataStorage 离线缓存框架 使用'}*/}
        {/*    onPress={() => {*/}
        {/*      NavigationUtil.goPage({}, 'DataStorageDemoPage');*/}
        {/*    }}*/}
        {/*    FetchDemoPage*/}
        {/*  />*/}
        {/*</View>*/}
        {/*修改主题 fetch AsyncStorage DataStorage*/}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FcFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  containerForIos: {
    flex: 1,
    marginTop: 30,
    // justifyContent: 'center',去掉：不然导航器显示不出来
    // alignItems: 'center',去掉：不然导航器显示不出来
    // backgroundColor: '#F5FcFF',去掉：不然导航器显示不出来
  },
  containerForAndroid: {
    flex: 1,
    marginTop: 0,
    // justifyContent: 'center',去掉：不然导航器显示不出来
    // alignItems: 'center',去掉：不然导航器显示不出来
    // backgroundColor: '#F5FcFF',去掉：不然导航器显示不出来
  },

  about_left: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  item: {
    backgroundColor: 'white',
    padding: 10,
    height: 90,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  groupTitle: {
    marginLeft: 10,
    marginTop: 10,
    marginBottom: 5,
    fontSize: 12,
    color: 'gray',
  },
});
//============================================redux的dispatch到页面的props转换======================================
const mapStateToProps = state => ({
  theme: state.theme.theme,
});
const mapDispatchToProps = dispatch => ({
  onThemeChange: theme => dispatch(actions.onThemeChange(theme)),
  onShowCustomThemeView: show => dispatch(actions.onShowCustomThemeView(show)),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MyPage);
//============================================redux的dispatch到页面的props转换======================================
