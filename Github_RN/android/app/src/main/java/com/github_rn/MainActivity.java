package com.github_rn;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.facebook.react.ReactActivity;
import com.facebook.react.ReactActivityDelegate;
import com.facebook.react.ReactRootView;
import com.sandaohaizi.trackshare.ShareModule;
import com.swmansion.gesturehandler.react.RNGestureHandlerEnabledRootView;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.UMShareAPI;
import com.github_rn.util.PermissionsUtil;

import org.devio.rn.splashscreen.SplashScreen;

import java.util.List;

public class MainActivity extends ReactActivity {


    private final String[] permission = new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.BLUETOOTH};
    private final String[] permission_P = new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.FOREGROUND_SERVICE, Manifest.permission.BLUETOOTH};

    PermissionsUtil.PermissionCallbacks permissionCallbacks = new PermissionsUtil.PermissionCallbacks() {
        @Override
        public void onPermissionsGranted() {
//            initData();
        }

        @Override
        public void onPermissionsDenied(int requestCode, List<String> perms) {

        }
    };
    /**
     * Returns the name of the main component registered from JavaScript. This is used to schedule
     * rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "Github_RN";
    }

    @Override
    protected ReactActivityDelegate createReactActivityDelegate() {
        return new ReactActivityDelegate(this, getMainComponentName()) {
            @Override
            protected ReactRootView createRootView() {
                return new RNGestureHandlerEnabledRootView(MainActivity.this);
            }
        };
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //开启白屏优化
        SplashScreen.show(this);
        super.onCreate(savedInstanceState);


        //获取读写权限
//         if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P) {
//             PermissionsUtil.checkAndRequestPermissions(MainActivity.this, null, permissionCallbacks, permission);
//         } else {
//             PermissionsUtil.checkAndRequestPermissions(MainActivity.this, null, permissionCallbacks, permission_P);
//         }

        //设置统计场景 和发送间隔
        MobclickAgent.setSessionContinueMillis(1000);
        MobclickAgent.setScenarioType(this, MobclickAgent.EScenarioType.E_DUM_NORMAL);

        //初始化社会化分享
        ShareModule.initSocialSDK(this);


    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //第三方登录成功后的回调
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }

    PermissionsUtil.LoadedAlertDialogCallbacks loadedAlertDialogCallbacks = new PermissionsUtil.LoadedAlertDialogCallbacks() {
        @Override
        public void cancel() {
            MainActivity.this.finish();
        }

        @Override
        public void confirm() {

        }

    };
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//         if (requestCode == PermissionsUtil.REQUEST_STATUS_CODE && permissions.length > 0) {
//             if (permissions[0].equals(Manifest.permission.READ_PHONE_STATE)) {
//                 if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {//同意
//
//                     if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P) {
//                         PermissionsUtil.checkAndRequestPermissions(this, null, permissionCallbacks, permission);
//                     } else {
//                         PermissionsUtil.checkAndRequestPermissions(this, null, permissionCallbacks, permission_P);
//                     }
//                 } else {//不同意-提示信息
//                     PermissionsUtil.createLoadedAlertDialog("通话",MainActivity.this, loadedAlertDialogCallbacks);
//                 }
// //                return;
//             }
//             if (permissions[0].equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {//读写权限
//                 if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {//同意
//
//                     if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P) {
//                         PermissionsUtil.checkAndRequestPermissions(this, null, permissionCallbacks, permission);
//                     } else {
//                         PermissionsUtil.checkAndRequestPermissions(this, null, permissionCallbacks, permission_P);
//                     }
//                 } else {//不同意-提示信息
//                     PermissionsUtil.createLoadedAlertDialog("存储读取",MainActivity.this, loadedAlertDialogCallbacks);
//                 }
//             }
//             if (permissions[0].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {//读写权限
//                 if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {//同意
//
//                     if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P) {
//                         PermissionsUtil.checkAndRequestPermissions(this, null, permissionCallbacks, permission);
//                     } else {
//                         PermissionsUtil.checkAndRequestPermissions(this, null, permissionCallbacks, permission_P);
//                     }
//                 } else {//不同意-提示信息
//                     PermissionsUtil.createLoadedAlertDialog("存储写入",MainActivity.this, loadedAlertDialogCallbacks);
//                 }
//             }
//             if (permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION)) {//
//                 if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {//同意
//
//                     if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P) {
//                         PermissionsUtil.checkAndRequestPermissions(this, null, permissionCallbacks, permission);
//                     } else {
//                         PermissionsUtil.checkAndRequestPermissions(this, null, permissionCallbacks, permission_P);
//                     }
//                 } else {//不同意-提示信息
//                     PermissionsUtil.createLoadedAlertDialog("位置读取",MainActivity.this, loadedAlertDialogCallbacks);
//                 }
//             }
//             if (permissions[0].equals(Manifest.permission.FOREGROUND_SERVICE)) {//
//                 if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {//同意
//
//                     if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P) {
//                         PermissionsUtil.checkAndRequestPermissions(this, null, permissionCallbacks, permission);
//                     } else {
//                         PermissionsUtil.checkAndRequestPermissions(this, null, permissionCallbacks, permission_P);
//                     }
//                 } else {//不同意-提示信息
//                     PermissionsUtil.createLoadedAlertDialog("前台显示",MainActivity.this, loadedAlertDialogCallbacks);
//                 }
//             }
//             if (permissions[0].equals(Manifest.permission.BLUETOOTH)) {//
//                 if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {//同意
//
//                     if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P) {
//                         PermissionsUtil.checkAndRequestPermissions(this, null, permissionCallbacks, permission);
//                     } else {
//                         PermissionsUtil.checkAndRequestPermissions(this, null, permissionCallbacks, permission_P);
//                     }
//                 } else {//不同意-提示信息
//                     PermissionsUtil.createLoadedAlertDialog("蓝牙开启",MainActivity.this, loadedAlertDialogCallbacks);
//                 }
//             }
//         }
    }

}
