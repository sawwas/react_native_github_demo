import Types from '../../action/types';
import {FLAG_LANGUAGE} from '../../expand/dao/LanguageDao';

/**
 * 1 创建一个reducer  action 函数
 */
const defaultState = {
  languages: [],
  keys: [],
}; //state 默认值 必须创建 否则报错显示不合法的reducer
/**
 * @param state
 * @param action
 * @returns {{}|{theme: *}}
 */
export default function onAction(state = defaultState, action) {
  switch (action.type) {
    case Types.LANGUAGE_LOAD_SUCCESS: //获取自定义数据成功
      if (FLAG_LANGUAGE.flag_key === action.flag) {
        return {
          //===========最热===============
          ...state,
          keys: action.languages,
        };
      } else {
        return {
          //===========趋势===============
          ...state,
          languages: action.languages,
        };
      }

    default:
      return state;
  }
}
