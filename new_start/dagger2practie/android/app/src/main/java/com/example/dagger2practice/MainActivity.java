package com.example.dagger2practice;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.dagger2practice.ui.auth.AuthActivity;
import com.facebook.react.ReactActivity;

/**
 * react native application
 */
public class MainActivity extends ReactActivity {



    /**
     * Returns the name of the main component registered from JavaScript. This is used to schedule
     * rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "dagger2practie";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(MainActivity.this, AuthActivity.class));
            }
        }, 3000);

    }
}
