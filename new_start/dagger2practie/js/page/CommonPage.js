import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
/**
 * 引导页
 */
export default class CommonPage extends Component {
  componentDidMount(): void {
    this.timer = setTimeout(() => {
      // SplashScreen.hide(); //==============隐藏白屏优化=================
      //跳转到首页
      // NavigationUtil.resetToHomePage(this.props);
    }, 200);
  }

  componentWillUnmount(): void {
    //页面销毁时，清空计时器
    // this.timer && clearTimeout(this.timer);
  }

  render():
    | React.ReactElement<any>
    | string
    | number
    | {}
    | React.ReactNodeArray
    | React.ReactPortal
    | boolean
    | null
    | undefined {
    return (
      <View style={styles.container}>
        <Text>{this.props.name}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
