/**
 *关于作者
 */
import React, {Component} from 'react';
import {
  ScrollView,
  Button,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  StatusBar,
  Linking,
  Clipboard,
} from 'react-native';
import Toast from 'react-native-easy-toast';
import actions from '../../action';
import {connect} from 'react-redux';
import NavigationUtil from '../../navigator/NavigationUtil';
import NavigationBar from '../../common/NavigationBar';
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {PlatformSelects} from '../../utils/PlatformSelects';
import {MORE_MENU} from '../../common/MORE_MENU';
import GlobalStyles from '../../res/styles/GlobalStyles';

import ViewUtil from '../../util/ViewUtil';
import AboutCommon, {FLAG_ABOUT} from './AboutCommon';
import config from '../../res/data/config';

function onclick(tab, theme) {
  //=============取出自定义主题
  //xxxxxxxxxxxxxxxxx
  if (!tab) {
    return;
  }
  if (tab.url) {
    // =================跳转到H5-==============
    NavigationUtil.goPage(
      {
        theme: theme,
        title: tab.title,
        url: tab.url,
        marginTops: 25, // android 适配
      },
      'WebViewPage',
    );
  }
  //===========================开启邮箱=======================
  if (tab.account && tab.account.indexOf('@') > -1) {
    let url = 'mailto:' + tab.account;
    Linking.canOpenURL(url)
      .then(supported => {
        if (!supported) {
          console.log("Can't handle url: " + url);
        } else {
          return Linking.openURL(url);
        }
      })
      .catch(err => console.error('An error occurred', err));
    return;
  }
  //===========================开启邮箱=======================

  //===========================开启剪贴板=======================

  if (tab.account) {
    Clipboard.setString(tab.account);
    ts.toast.show(tab.title + tab.account + '已复制到剪切板。');
  }
  //===========================开启剪贴板=======================
}

export default class AboutMePage extends Component {
  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;

    //=========================设置组件====================
    this.aboutCommon = new AboutCommon(
      {
        ...this.params,
        navigation: this.props.navigation,
        flagAbout: FLAG_ABOUT.flag_about_me,
      },
      data =>
        this.setState({
          // 将AboutCommon 中的数据传递到当前
          ...data,
        }),
    );
    //=========================设置组件====================

    this.state = {
      data: config, //获取AboutCommon中的网络数据之前默认显示本地数据
      //=======================控制扩展的item显示状态=====================
      showTutorial: true, //默认展开教程的扩展item
      showBlog: false,
      showQQ: false,
      showContact: false,
      //=======================控制扩展的item显示状态=====================
    };
  }

  //==============返回扩展的item头部 UI-============
  _item(data, isShow, key) {
    this.params = this.props.navigation.state.params;
    const {theme} = this.params; //=============取出自定义主题
    return ViewUtil.getSettingItem(
      () => {
        this.setState({
          [key]: !this.state[key], //展开和收起取反~~~~~~~~~~~！！！！！！！！！！！！！！！！1
        });
      },
      data.name,
      theme.themeColor,
      Ionicons,
      data.icon,
      isShow ? 'ios-arrow-up' : 'ios-arrow-down',
    );
  }
  //==============返回扩展的item头部 UI-============

  //==============返回扩展的item子类内容 UI-============

  /**
   * 显示列表数据
   * @param dic
   * @param isShowAccount
   */
  renderItems(dic, isShowAccount) {
    this.params = this.props.navigation.state.params;
    const {theme} = this.params; //=============取出自定义主题
    if (!dic) {
      return null;
    } //  npe 保护
    let views = [];
    for (let i in dic) {
      let title = isShowAccount //根据是否显示账号取出title
        ? dic[i].title + ':' + dic[i].account
        : dic[i].title;
      views.push(
        <View key={i}>
          {ViewUtil.getSettingItem(
            () => onclick(dic[i], theme),
            title,
            theme.themeColor,
          )}
          <View style={GlobalStyles.line} />
        </View>,
      );
    }
    return views;
  }
  //==============返回扩展的item子类内容 UI-============

  render():
    | React.ReactElement<any>
    | string
    | number
    | {}
    | React.ReactNodeArray
    | React.ReactPortal
    | boolean
    | null
    | undefined {
    const {navigation} = this.props;

    //============================item ui
    const content = (
      <View>
        {/*教程*/}
        {this._item(
          this.state.data.aboutMe.Tutorial,
          this.state.showTutorial,
          'showTutorial',
        )}
        <View style={GlobalStyles.line} />
        {this.state.showTutorial
          ? this.renderItems(this.state.data.aboutMe.Tutorial.items)
          : null}
        {/*技术博客*/}

        {this._item(
          this.state.data.aboutMe.Blog,
          this.state.showBlog,
          'showBlog',
        )}
        <View style={GlobalStyles.line} />
        {this.state.showBlog
          ? this.renderItems(this.state.data.aboutMe.Blog.items)
          : null}
        {/*    QQ*/}
        {this._item(this.state.data.aboutMe.QQ, this.state.showQQ, 'showQQ')}
        <View style={GlobalStyles.line} />
        {this.state.showQQ
          ? this.renderItems(this.state.data.aboutMe.QQ.items, true)
          : null}

        {/*    联系方式*/}
        {this._item(
          this.state.data.aboutMe.Contact,
          this.state.showContact,
          'showContact',
        )}
        <View style={GlobalStyles.line} />
        {this.state.showContact
          ? this.renderItems(this.state.data.aboutMe.Contact.items, true)
          : null}
      </View>
    );
    //============================item ui
    return (
      <View style={{flex: 1}}>
        {/*===============================沉浸式状态栏-============================*/}
        <StatusBar
          animated={true} //指定状态栏的变化是否应以动画形式呈现。目前支持这几种样式：backgroundColor, barStyle和hidden
          hidden={false} //是否隐藏状态栏。
          backgroundColor={'transparent'} //状态栏的背景色
          translucent={true} //指定状态栏是否透明。设置为true时，应用会在状态栏之下绘制（即所谓“沉浸式”——被状态栏遮住一部分）。常和带有半透明背景色的状态栏搭配使用。
          barStyle={'light-content'} // enum('default', 'light-content', 'dark-content')
        />
        {/*===============================沉浸式状态栏-============================*/}

        {this.aboutCommon.render(content, this.state.data.author)}
        <Toast ref={toast => (this.toast = toast)} position={'center'} />
      </View>
    );
  }
}
