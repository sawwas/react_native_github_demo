package com.github_rn.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.text.TextUtils;
import android.util.Log;

import com.github_rn.util.PermissionsUtil;


import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static android.content.Context.ACTIVITY_SERVICE;

/**
 * @author yangxin
 * @Description: TODO
 * @date 2016年6月8日 下午4:54:33
 */
public class AppUtil {
    /**
     * @param context
     * @return String 返回类型
     * @Author yangxin
     * @Description: 获取版本号 VersionName
     * @Date 2016年6月8日 下午4:57:01
     */
    public static String getVersionName(Context context) {
        try {
            PackageInfo pi = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return pi.versionName;
        } catch (NameNotFoundException e) {
            // e.printStackTrace();
            Log.e("获取版本号错误", e.getMessage());
            return "";
        }
    }

    /**
     * @param context
     * @return int 返回类型
     * @Author yangxin
     * @Description: 获取版本号(内部识别号) VersionCode
     * @Date 2016年6月8日 下午4:56:33
     */
    public static int getVersionCode(Context context) {
        if (context == null) {
            return PreferencesUtil.getInt(PreferencesUtil.APPVERSION, 0);
        } else {
            try {
                PackageInfo pi = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                PreferencesUtil.putInt(PreferencesUtil.APPVERSION, pi.versionCode);
                return pi.versionCode;
            } catch (NameNotFoundException e) {
                // e.printStackTrace();
                Log.e("获取版本号错误", e.getMessage());
                return PreferencesUtil.getInt(PreferencesUtil.APPVERSION, 0);
            }
        }

    }

    public static String getApplicationName(Activity activity) {
        PackageManager packageManager = null;
        ApplicationInfo applicationInfo = null;
        try {
            packageManager = activity.getApplicationContext().getPackageManager();
            applicationInfo = packageManager.getApplicationInfo(activity.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            applicationInfo = null;
        }
        String applicationName = (String) packageManager.getApplicationLabel(applicationInfo);
        return applicationName;
    }

    /**
     * 获取application中指定的meta-data
     *
     * @return 如果没有获取成功(没有对应值 ， 或者异常)，则返回值为""
     */
    public static String getAppMetaData(Context ctx, String key) {
        if (ctx == null || TextUtils.isEmpty(key)) {
            return "";
        }
        String resultData = "";
        try {
            PackageManager packageManager = ctx.getPackageManager();
            if (packageManager != null) {
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(ctx.getPackageName(), PackageManager.GET_META_DATA);
                if (applicationInfo != null) {
                    if (applicationInfo.metaData != null) {
                        resultData = applicationInfo.metaData.getString(key);
                    }
                }

            }
        } catch (PackageManager.NameNotFoundException e) {
            // e.printStackTrace();
        }

        return resultData;
    }

//    public static void signIn(final Context activity) {
//        if (RememberUserIdService.isMeLogin()) {
//            long data = PreferencesUtil.getLong("signIn110_" + RememberUserIdService.getLocalUserId());
//            if (data <= 0 || data < DateFormatUtil.getStartTime() || (System.currentTimeMillis() - data) > 3600000) {
//                Map<String, String> params = new HashMap<>();
//                params.put("userId", RememberUserIdService.getLocalUserId());
//                VolleyUtil.post(UrlConstants.SIGN_IN, params, new Listener<String>() {
//
//                    @Override
//                    public void onResponse(String response) {
//                        if (!StringUtil.isEmpty(response)) {
//                            Log.v("signIn", response);
//                            BaseDto dto = JSON.parseObject(response, BaseDto.class);
//                            if (dto != null && dto.success && dto.data != null) {
//                                ToastUtil.show(activity, dto.data.toString());
//                                // PreferencesUtil.putString("signIn_" +
//                                // RememberUserIdService.getLocalUserId(),
//                                // DateFormatUtil.formatToStringTimeDay(DateFormatUtil.getCurrentDate()));
//                                PreferencesUtil.putLong("signIn110_" + RememberUserIdService.getLocalUserId(), System.currentTimeMillis());
//                            }
//                        }
//                    }
//                }, new ErrorListener() {
//
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                    }
//                });
//            }
//        }
//    }

    /**
     * 应用程序是否已安装
     *
     * @param context
     * @param packageName 应用程序的包名
     * @return
     */
    public static boolean isInstalled(Context context, String packageName) {
        if (packageName == null || "".equals(packageName))
            return false;
        try {
            context.getPackageManager().getApplicationInfo(packageName, PackageManager.GET_UNINSTALLED_PACKAGES);
            return true;
        } catch (NameNotFoundException e) {
            return false;
        }
    }

//    public static boolean isOnlineUrl() {
//        return "http://activity.mgtvhd.com/".equals(UrlConstants.INTERACTION_URL);
//    }

    /**
     * 获取APP name
     *
     * @param context
     * @param pID
     * @return
     */
    public static String getAppName(Context context, int pID) {
        String processName = null;
        ActivityManager am = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
        List l = am.getRunningAppProcesses();
        Iterator i = l.iterator();
        PackageManager pm = context.getPackageManager();
        while (i.hasNext()) {


            ActivityManager.RunningAppProcessInfo info = (ActivityManager.RunningAppProcessInfo) (i.next());
            try {
                if (info.pid == pID) {
                    processName = info.processName;
                    return processName;
                }
            } catch (Exception e) {
                // Log.d("Process", "Error>> :"+ e.toString());
            }
        }
        return processName;
    }
}
