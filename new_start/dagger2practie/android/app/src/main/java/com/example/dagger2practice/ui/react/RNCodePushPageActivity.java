package com.example.dagger2practice.ui.react;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.facebook.react.ReactActivity;

public class RNCodePushPageActivity extends ReactActivity {

    private static String moduleName;

    public static void start(Activity activity, String moduleNames) {
        moduleName = moduleNames;
        activity.startActivity(new Intent(activity, RNCodePushPageActivity.class));
    }
    /**
     * Returns the name of the main component registered from JavaScript. This is used to schedule
     * rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return moduleName;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    //======================开发者setting===================

}
