import React from 'react';
import {AsyncStorage} from 'react-native';

import langs from '../../res/data/langs';
import keys from '../../res/data/keys';
export const FLAG_LANGUAGE = {
  flag_language: 'language_dao_language',
  flag_key: 'language_dao_key',
};
/**
 * 自定义语言工具类
 */
export default class LanguageDao {
  constructor(flag) {
    this.flag = flag; //区分是最热模块还是趋势模块调用
  }

  //=============================返回json文件被反序列化成的对象===========================
  fetch() {
    return new Promise((resolve, reject) => {
      /**
       * 获取语言或标签
       */
      AsyncStorage.getItem(this.flag, (error, result) => {
        if (error) {
          reject(error);
          return;
        }
        if (!result) {
          // 如果result为null 则保存到数据库
          //
          let data = this.flag === FLAG_LANGUAGE.flag_language ? langs : keys;
          this.save(data);
          resolve(data); //返回给调用者
        } else {
          try {
            resolve(JSON.parse(result)); //如果result存在，则直接返回解析之后的对象给调用者
          } catch (e) {
            reject(error);
          }
        }
      });
    });
  }
  //=============================返回json文件被反序列化成的对象===========================

  /**
   * 保存语言或标签
   * @param objectData
   */
  save(objectData) {
    let stringData = JSON.stringify(objectData);
    AsyncStorage.setItem(this.flag, stringData, (error, result) => {});
  }
}
