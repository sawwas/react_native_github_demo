/**
 *FetchDemo
 */
import React, {Component} from 'react';
import {Button, StyleSheet, Text, TextInput, View} from 'react-native';

//=============================================fetch请求=============================================

function loadData(ts) {
  //https://api.github.com/search/repositories?q=java
  let url = 'https://api.github.com/search/repositories?q=' + `${ts.searchKey}`;
  fetch(url) //发送get网络请求
    .then(response => response.text()) //将response转化为string
    .then(responseText => {
      //保存文字
      ts.setState({
        showText: responseText, //结果渲染到text组件上
      });
    });
}

function loadData2(ts) {
  //https://api.github.com/search/repositories?q=java
  let url = 'https://api.github.com/search/repositories?q=' + `${ts.searchKey}`;
  fetch(url) //发送get网络请求
    .then(response => {
      if (response.ok) {
        return response.text();
      }
      throw new Error('Network reponse was not ok');
    }) //将response转化为string
    .then(responseText => {
      //保存文字
      ts.setState({
        showText: responseText, //结果渲染到text组件上
      });
    })
    .catch(e => {
      ts.setState({
        showText: e.toString(),
      });
    });
}

//=============================================fetch请求=============================================

export default class FetchDemoPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showText: '', //代表搜索结果
    };
  }
  render():
    | React.ReactElement<any>
    | string
    | number
    | {}
    | React.ReactNodeArray
    | React.ReactPortal
    | boolean
    | null
    | undefined {
    const {navigation} = this.props;

    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>fetch 使用</Text>
        <View style={styles.input_container}>
          <TextInput
            style={styles.input}
            onChangeText={text => {
              this.searchKey = text; //接收用户输入的文字
            }}
          />
          <Button
            title={'获取'}
            onPress={() => {
              loadData2(this);
            }}
          />
        </View>
        <Text>{this.state.showText}</Text>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#F5FcFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  input: {
    height: 50,
    flex: 1,
    borderColor: 'black',
    borderWidth: 1,
    marginRight: 10,
  },
  input_container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
