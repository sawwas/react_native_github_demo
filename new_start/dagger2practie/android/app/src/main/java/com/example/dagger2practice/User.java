package com.example.dagger2practice;

import javax.inject.Singleton;

@Singleton
public class User {

    private int id;
    private String username;

    public User(int id, String username) {
        this.id = id;
        this.username = username;
    }
}
