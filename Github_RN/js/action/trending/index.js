import Types from '../types';
import DataStore, {FLAG_STORAGE} from '../../expand/dao/DataStore';
import {handleData, _projectModel} from '../ActionUtil';

/**
 * 监听获取趋势下拉刷新数据的 异步action 函数 显示loading
 * @param storeName (Android , IOS,PHP  tab标签名称)
 * @param url
 * @param pageSize 每页显示多少条数据
 * @param favoriteDao
 */
export function onRefreshTrending(storeName, url, pageSize, favoriteDao) {
  return dispatch => {
    //dispatch 刷新的action  哪一个标签在刷新传入storename
    dispatch({type: Types.TRENDING_REFRESH, storeName: storeName});
    //离线缓存
    let dataStore = new DataStore();
    dataStore
      .fetchData(url, FLAG_STORAGE.flag_trending) //异步action 和数据流
      .then(data => {
        handleData(
          Types.TRENDING_REFRESH_SUCCESS,
          dispatch,
          storeName,
          data,
          pageSize,
          favoriteDao,
        );
      })
      .catch(error => {
        console.log(error);
        dispatch({types: Types.TRENDING_REFRESH_FAIL, storeName, error}); //es7新语法直接设置key value
      });
  };
}

/**
 * 加载更多
 * @param storeName
 * @param pageIndex 第几页
 * @param pageSize 每页显示条数
 * @param dataArray 原始数据
 * @param favoriteDao
 * @param callback 回调函数 ，可以通过回调函数来向调用页面通信：比如异常信息的显示，没有更多等待
 */
export function onLoadMoreTrending(
  storeName,
  pageIndex,
  pageSize,
  dataArray = [],
  favoriteDao,
  callback,
) {
  return dispatch => {
    setTimeout(() => {
      //模拟网络请求
      // 上一次加载完所有的数据
      if ((pageIndex - 1) * pageSize >= dataArray.length) {
        if (typeof callback === 'function') {
          callback('no more'); //没有更多数据了
        }
        dispatch({
          type: Types.TRENDING_LOAD_MORE_FAIL,
          error: 'no more',
          storeName: storeName,
          pageIndex: --pageIndex,
          projectModels: dataArray,
        });
      } else {
        //算出本次可载入的最大数据量
        let max =
          pageSize * pageIndex > dataArray.length
            ? dataArray.length
            : pageSize * pageIndex;
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~原有data 基础上加入收藏key功能~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        _projectModel(
          dataArray.slice(0, max), //返回子数组 用于前端分页
          favoriteDao,
          data => {
            dispatch({
              type: Types.TRENDING_LOAD_MORE_SUCCESS,
              storeName,
              pageIndex,
              projectModels: data,
            });
          },
        );
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~原有data 基础上加入收藏key功能~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      }
    }, 500);
  };
}
/**
 + * 刷新收藏状态
 + * @param storeName
 + * @param pageIndex 第几页
 + * @param pageSize 每页展示条数
 + * @param dataArray 原始数据
 + * @param favoriteDao
 + * @returns {function(*)}
 + */
export function onFlushTrendingFavorite(
  storeName,
  pageIndex,
  pageSize,
  dataArray = [],
  favoriteDao,
) {
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~原有data 基础上加入收藏key功能~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  return dispatch => {
    //算出本次可载入的最大数据量
    let max =
      pageSize * pageIndex > dataArray.length
        ? dataArray.length
        : pageSize * pageIndex;
    _projectModel(
      dataArray.slice(0, max), //返回子数组 用于前端分页
      favoriteDao,
      data => {
        dispatch({
          type: Types.TRENDING_FLUSH_FAVORITE,
          storeName,
          pageIndex,
          projectModels: data,
        });
      },
    );
  };
}
