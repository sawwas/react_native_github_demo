/**
 * 全局样式
 */
import {Dimensions} from 'react-native';
const {height, width} = Dimensions.get('window');
const BACKGROUND_COLOR = '#f3f3f4';
export default {
  //  分割线样式
  line: {
    height: 0.5,
    alpha: 0.5,
    backgroundColor: 'darkgray',
  },
  //页面背景颜色
  root_container: {
    flex: 1,
    backgroundColor: BACKGROUND_COLOR,
  },
  backgroundColor: BACKGROUND_COLOR,

  nav_bar_height_ios: 44,
  nav_bar_height_android: 50,
  window_height: height,
};
