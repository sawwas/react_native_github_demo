package com.example.dagger2practice.ui.react;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;

import androidx.appcompat.app.AppCompatActivity;

import com.example.dagger2practice.BaseActivity;
import com.example.dagger2practice.BaseApplication;
import com.example.dagger2practice.R;
import com.facebook.infer.annotation.Assertions;
import com.facebook.react.BuildConfig;
import com.facebook.react.ReactActivity;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactRootView;
import com.facebook.react.common.LifecycleState;
import com.facebook.react.devsupport.DoubleTapReloadRecognizer;
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler;
import com.facebook.react.shell.MainReactPackage;
import com.microsoft.codepush.react.CodePush;

public class ReactPageActivity extends AppCompatActivity implements DefaultHardwareBackBtnHandler {

    private ReactRootView mReactRootView;
    private ReactInstanceManager mReactInstanceManager;
    private boolean mDeveloperSupport = true;
    private DoubleTapReloadRecognizer mDoubleTapReloadRecognizer;

    private static String moduleName;
    private CodePush MainCodePush = null;

    public static void start(Activity activity, String moduleNames) {
        moduleName = moduleNames;
        activity.startActivity(new Intent(activity, ReactPageActivity.class));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rnpage);

        //        mPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//        mPreferences.edit().putString("debug_http_host","localhost:8081").commit();//pc端ip地址

//        mReactRootView = new ReactRootView(this);
        mReactRootView = findViewById(R.id.react_root_view1);


        MainCodePush = new CodePush(getResources().getString(R.string.CodePushDeploymentKey),
                BaseApplication.getContext(), BuildConfig.DEBUG);

        mReactInstanceManager = ReactInstanceManager.builder()
                .setCurrentActivity(this)
                .setApplication(getApplication())
                .setBundleAssetName("index.android.bundle")
                .setJSMainModulePath("index")
                .setJSBundleFile(CodePush.getJSBundleFile())
                .addPackage(new MainReactPackage())
                .addPackage(MainCodePush)
                .setUseDeveloperSupport(true)
                .setInitialLifecycleState(LifecycleState.RESUMED)
                .build();

        mReactRootView.startReactApplication(mReactInstanceManager, moduleName, null);
        mDoubleTapReloadRecognizer = new DoubleTapReloadRecognizer();//双击对象
//        setContentView(mReactRootView);


    }

    @Override
    public void invokeDefaultOnBackPressed() {
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostPause(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostResume(this);
        }
    }

    @Override
    public void onBackPressed() {

        if (mReactInstanceManager != null) {
            mReactInstanceManager.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mReactInstanceManager != null) {
            mReactInstanceManager.onHostDestroy(this);
        }

        if (mReactRootView != null) {
            mReactRootView.unmountReactApplication();
        }
    }

    //======================开发者setting===================
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (getUseDeveloperSupport()) {
            if (keyCode == KeyEvent.KEYCODE_MENU) {
                mReactInstanceManager.showDevOptionsDialog();
                return true;
            }
            boolean didDoubleTapR = Assertions.assertNotNull(mDoubleTapReloadRecognizer).didDoubleTapR(keyCode, getCurrentFocus());
            if (didDoubleTapR) {
                //双击R重新加载js
                mReactInstanceManager.getDevSupportManager().handleReloadJS();
                return true;
            }
        }
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private boolean getUseDeveloperSupport() {
        if (mReactInstanceManager != null && mDeveloperSupport) {
            return true;
        } else {
            return false;
        }
    }
    //======================开发者setting===================

}
