/**
 *AsyncStorageDemo
 */
import React, {Component} from 'react';
import {AsyncStorage, StyleSheet, Text, TextInput, View} from 'react-native';

//=============================================本地存储=============================================

const KEY = 'save_key';

async function doSave(value) {
  //用法一
  await AsyncStorage.setItem(KEY, value, error => {
    error && console.log(error.toString());
  });
}

async function doRemove(value) {
  //用法一
  await AsyncStorage.removeItem(KEY, error => {
    error && console.log(error.toString());
  });
}

async function getData(ts, value) {
  //用法一
  await AsyncStorage.getItem(KEY, (error, result) => {
    ts.setState({
      showText: result,
    });
    console.log(result);
    error && console.log(error.toString());
  });
}

//=============================================本地存储=============================================

export default class AsyncStorageDemoPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showText: '', //代表存储结果
    };
  }

  render():
    | React.ReactElement<any>
    | string
    | number
    | {}
    | React.ReactNodeArray
    | React.ReactPortal
    | boolean
    | null
    | undefined {
    const {navigation} = this.props;

    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>fetch 使用</Text>
        <TextInput
          style={styles.input}
          onChangeText={text => {
            this.value = text; //接收用户输入的文字
          }}
        />
        <View style={styles.input_container}>
          <Text
            onPress={() => {
              doSave(this.value);
            }}>
            存储
          </Text>
          <Text
            onPress={() => {
              doRemove(this.value);
            }}>
            删除
          </Text>
          <Text
            onPress={() => {
              getData(this, this.value);
            }}>
            获取
          </Text>
        </View>
        <Text style={styles.welcome}>{this.state.showText}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#F5FcFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  input: {
    height: 50,
    // flex: 1,
    borderColor: 'black',
    borderWidth: 1,
    marginRight: 10,
  },
  input_container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
});
