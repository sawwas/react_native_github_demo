import React, {Component} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import HTMLView from 'react-native-htmlview';
import {PropTypes} from 'prop-types';

/**
 * 最热和趋势Item 的收藏按钮UI 布局 抽象
 */
export default class BaseItem extends Component {
  static propTypes = {
    projectModel: PropTypes.object,
    onSelect: PropTypes.func,
    onFavorite: PropTypes.func,
  };
  constructor(props) {
    super(props);
    this.state = {
      isFavorite: this.props.projectModel.isFavorite,
    };
  }

  /**
   * 牢记：https://github.com/reactjs/rfcs/blob/master/text/0006-static-lifecycle-methods.md
   * componentWillReceiveProps在新版React中不能再用了
   * @param nextProps
   * @param prevState
   * @returns {*}
   */
  static getDerivedStateFromProps(nextProps, prevState) {
    //======================实时获取props 的状态state 变更================
    const isFavorite = nextProps.projectModel.isFavorite;
    if (prevState.isFavorite !== isFavorite) {
      return {
        isFavorite: isFavorite,
      };
    }
    return null;
  }

  //========================设置收藏状态====================== 不设置参数也行！！！！！！
  setFavoriteState(isFavorite) {
    this.props.projectModel.isFavorite = isFavorite;
    this.setState({
      isFavorite: isFavorite,
    });
  }
  //========================设置收藏状态====================== 不设置参数也行！！！！！！

  onPressFavorite() {
    this.setFavoriteState(!this.state.isFavorite); // 设置收藏状态 当前收藏状态取反
    this.props.onFavorite(this.props.projectModel.item, !this.state.isFavorite); //将收藏状态传回对应的最热和趋势 页面 当前收藏状态取反
  }
  //===================返回收藏按钮的ui====================
  _favoriteIcon() {
    const {theme} = this.props; //================!!!!!!!@@@@@@@@@@@~~~~~~~~~~~~~~取出自定义主题
    return (
      <TouchableOpacity
        style={{padding: 6}}
        underlayColor="transparent"
        onPress={() => this.onPressFavorite()}>
        <FontAwesome
          name={this.state.isFavorite ? 'star' : 'star-o'}
          size={26}
          style={{color: theme.themeColor}}
        />
      </TouchableOpacity>
    );
  }
  //===================返回收藏按钮的ui====================

  //===================点击item事件监听====================
  onItemClick() {
    this.props.onSelect(isFavorite => {
      this.setFavoriteState(isFavorite); // BaseItem 事件
    });
  }
  //===================点击item事件监听====================
}
