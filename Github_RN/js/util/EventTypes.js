/**
 * EventBus 跨组件通信
 */
export default {
  bottom_tab_select: 'bottom_tab_select', //底部导航按钮的变化

  //============收藏页面 收藏状态发生变化
  favorite_changed_popular: 'favorite_changed_popular',
  favoriteChanged_trending: 'favoriteChanged_trending',
  //============收藏页面 收藏状态发生变化
};
