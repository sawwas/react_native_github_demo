import React, {Component} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import BaseItem from './BaseItem';

/**
 * 最热Item UI 布局
 */
export default class PopularItem extends BaseItem {
  render():
    | React.ReactElement<any>
    | string
    | number
    | {}
    | React.ReactNodeArray
    | React.ReactPortal
    | boolean
    | null
    | undefined {
    const {projectModel} = this.props; // ============================ 继承BaseItem 取出projectModel 里面的item=====================
    const {item} = projectModel;
    //如果没有则返回null
    if (!item || !item.owner) {
      return null;
    }
    //===================（作废 由父类方法继承使用）设置点击按钮=====================
    // let favoriteButton = (
    //   <TouchableOpacity
    //     style={{padding: 6}}
    //     underlayColor={'transparent'} // 按下去的按钮背景颜色
    //     onPress={() => {}}>
    //     <FontAwesome name={'star-o'} size={20} style={'red'} />
    //   </TouchableOpacity>
    // );
    //===================（作废 由父类方法继承使用）设置点击按钮=====================

    //调用上一个页面传入的onSelect
    return (
      <TouchableOpacity onPress={() => this.onItemClick()}>
        {/*BaseItem 事件*/}
        {/*外层布局容器*/}
        <View style={styles.cell_container}>
          {/*作者名称*/}
          <Text style={styles.title}>{item.full_name}</Text>
          {/*说明*/}
          <Text style={styles.description}>{item.description}</Text>
          {/*子布局*/}
          <View style={styles.row}>
            <View style={styles.row}>
              <Text>Author:</Text>
              <Image
                style={{height: 22, width: 22}}
                source={{uri: item.owner.avatar_url}}
              />
            </View>
            <View
              // style={[{flex: 1, justifyContent: 'space-between'}, styles.row]}>//================设置权重================
              style={[styles.row]}>
              <Text>Star:</Text>
              <Text>{item.stargazers_count}</Text>
            </View>
            {/*===============调用父类BaseItem================方法*/}
            {this._favoriteIcon()}
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    justifyContent: 'space-between', //平分空间
    flexDirection: 'row',
    alignItems: 'center', //一行里面居中
  },
  cell_container: {
    backgroundColor: 'white',
    padding: 10, //外层布局内边距
    marginLeft: 5, //外层布局外边距
    marginRight: 5,
    marginVertical: 3, //分割线的间距
    borderColor: '#dddddd', //外边框颜色
    borderWidth: 0.5,
    borderRadius: 2,
    shadowColor: 'gray', //以下都是ios设置阴影
    shadowOffset: {width: 0.5, height: 0.5},
    shadowOpacity: 0.4,
    shadowRadius: 1,
    elevation: 2, //android 设置阴影
  },
  title: {
    fontSize: 16,
    marginBottom: 2,
    color: '#212121',
  },
  description: {
    fontSize: 14,
    marginBottom: 2,
    color: '#757575',
  },
});
