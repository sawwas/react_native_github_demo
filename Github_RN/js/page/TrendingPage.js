/**
 *趋势
 */
import React, {Component} from 'react';
import {
  ActivityIndicator,
  DeviceInfo,
  FlatList,
  RefreshControl,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  DeviceEventEmitter,
} from 'react-native';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';
import {createAppContainer} from 'react-navigation';
import {PlatformSelects} from '../utils/PlatformSelects';
import {connect} from 'react-redux'; //使得组件和 redux 关联
import actions from '../action/index';
import TrendingItem from '../common/TrendingItem';
import Toast from 'react-native-easy-toast';
import NavigationBar from '../common/NavigationBar';
import TrendingDialog, {TimeSpans} from '../common/TrendingDialog';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import NavigationUtil from '../navigator/NavigationUtil';
import FavoriteDao from '../expand/dao/FavoriteDao';
import {FLAG_STORAGE} from '../expand/dao/DataStore';
import FavoriteUtil from '../util/FavoriteUtil';
import EventBus from 'react-native-event-bus';
import EventTypes from '../util/EventTypes';
import {FLAG_LANGUAGE} from '../expand/dao/LanguageDao';
import ArrayUtil from '../util/ArrayUtil';
// import DeviceInfo from 'react-native-device-info';

const URL = 'https://github.com/trending/';
const QUERY_STR = '?since=daily';
const pageSize = 10; //每页加载的数量
let tabNav; // ====================避免 重复渲染 tab
const EVENT_TYPE_TIME_SPAN_CHANGE = 'EVENT_TYPE_TIME_SPAN_CHANGE';
const favoriteDao = new FavoriteDao(FLAG_STORAGE.flag_trending);

const styles = StyleSheet.create({
  //去掉：不然导航器显示不出来
  containerForIos: {
    flex: 1,
    marginTop: 30,
    // justifyContent: 'center',去掉：不然导航器显示不出来
    // alignItems: 'center',去掉：不然导航器显示不出来
    // backgroundColor: '#F5FcFF',去掉：不然导航器显示不出来
  },
  containerForAndroid: {
    flex: 1,
    marginTop: 0,
    // justifyContent: 'center',去掉：不然导航器显示不出来
    // alignItems: 'center',去掉：不然导航器显示不出来
    // backgroundColor: '#F5FcFF',去掉：不然导航器显示不出来
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  //  tab 内容最小宽度
  tabStyle: {
    // minWidth: 50, fix 导致tabStyle 初次加载时由小到大的闪烁
    padding: 0, //===================== !!!!!!!!!!!! fix修复初次加载tab由大到小
    alignItems: 'center',
  },
  // tab 指示器样式
  indicatorStyle: {
    height: 2,
    backgroundColor: 'white',
  },
  // tab 文字大小
  labelStyle: {
    fontSize: 13,
    marginTop: -18, //===================== !!!!!!!!!!!! fix修复初次加载tab由大到小
    // marginTop: 6,//===================== !!!!!!!!!!!! fix修复初次加载tab由大到小
    // marginBottom: 6,//===================== !!!!!!!!!!!! fix修复初次加载tab由大到小
  },
  // 上拉加载更多ui样式
  indicatorContainer: {
    alignItems: 'center',
  },
  indicator: {
    color: 'red',
    margin: 10,
  },
});

//====================================定义动态显示的tab顶部导航=================================
let tabNames = ['All', 'C', 'C#', 'PHP', 'JavaScript'];
let prop = [];
let tsMain = '';

//====================================顶部导航=================================
/**
 * ======================================顶部导航下的内容======================================
 */
class TrendingTab extends Component {
  constructor(props) {
    super(props);
    const {tabLabel, timeSpan} = this.props; // root tab 标签的标题 title
    this.storeName = tabLabel;
    this.timeSpan = timeSpan;
    this.isFavoriteChanged = false; //==================初始化是否监听收藏页面过来的收藏状态变化通知 (默认第一次没有变化)
  }

  componentDidMount(): void {
    loadData(this, false);
    //====================筛选日期完成 添加监听回调================
    this.timeSpanChangeListener = DeviceEventEmitter.addListener(
      EVENT_TYPE_TIME_SPAN_CHANGE,
      timeSpan => {
        this.timeSpan = timeSpan;
        loadData(this, false);
      },
    );
    //====================筛选日期完成 添加监听回调================

    //====================收藏页面添加 移除收藏按钮时 添加监听回调================
    EventBus.getInstance().addListener(
      EventTypes.favoriteChanged_trending,
      (this.favoriteChanged_trending = () => {
        this.isFavoriteChanged = true;
      }),
    );
    //====================收藏页面添加 移除收藏按钮时 添加监听回调================

    //====================页面添加 底部导航按钮发生状态变化时 添加监听回调================
    EventBus.getInstance().addListener(
      EventTypes.bottom_tab_select,
      (this.bottom_tab_select = data => {
        //收藏页面索引位置为2 当前页面的索引位置为1
        if (data.to === 1 && this.isFavoriteChanged) {
          loadData(this, false, true);
        }
      }),
    );
    //====================页面添加 底部导航按钮发生状态变化时 添加监听回调================
  }
  componentWillUnmount(): void {
    //===============释放时间移除监听资源
    if (this.timeSpanChangeListener) {
      this.timeSpanChangeListener.remove();
    }
    //===============释放时间移除监听资源

    EventBus.getInstance().removeListener(this.favoriteChanged_trending);
    EventBus.getInstance().removeListener(this.bottom_tab_select);
  }

  //====================================上拉加载更多UI组件=================================
  genIndicator(ts) {
    //是否隐藏上拉加载更多
    return _store(ts).hideLoadingMore ? null : (
      <View style={styles.indicatorContainer}>
        <ActivityIndicator style={styles.indicator} />
        <Text>正在加载更多</Text>
      </View>
    );
  }

  //====================================上拉加载更多UI组件=================================

  render():
    | React.ReactElement<any>
    | string
    | number
    | {}
    | React.ReactNodeArray
    | React.ReactPortal
    | boolean
    | null
    | undefined {
    prop = this.props;
    let store = _store(this); //初始化默认的state
    const {theme} = prop;
    return (
      <View style={styles.containerForAndroid}>
        {/*<Text*/}
        {/*  onPress={() => {*/}
        {/*    //============================================================================DetailPage 和 HomePage 在同一级，故此使用*/}
        {/*    NavigationUtil.goPage({}, 'DetailPage');*/}
        {/*  }}>*/}
        {/*  TrendingTab*/}
        {/*</Text>*/}
        <FlatList
          data={store.projectModels}
          keyExtractor={(item, index) => '' + index} //===============!!!============注意趋势模块 id 为空使用fullName
          refreshControl={
            <RefreshControl
              title={'Loading'} //刷新文字文案
              titleColor={theme.themeColor} // 刷新文字颜色
              colors={[theme.themeColor]} // 刷新样式颜色
              refreshing={store.isLoading} // 是否刷新的状态
              onRefresh={() => loadData(this, false)}
              tintColor={theme.themeColor}
            />
          }
          renderItem={data => renderItem(data, theme)}
          ListFooterComponent={() => this.genIndicator(this)} //上拉加载更多UI组件
          onEndReached={() => {
            console.log('============onEndReached===========');

            //=======修复上拉刷新调用2次和第一次进入页面时不用触发上拉加载 loadMore为true的问题--伴随reducer修改========================
            setTimeout(() => {
              //==========!!!===========延迟100毫秒再触发上拉加载，避免一直在打圈圈ui--onMomentumScrollBegin执行比较晚的情况
              if (this.canLoadMore) {
                loadData(this, true);
                this.canLoadMore = false;
              }
            }, 100);
          }}
          onEndReachedThreshold={0.5} //列表可见长度距离底部的比值
          onMomentumScrollBegin={() => {
            //列表刚开始滚动的时候让上拉加载设置为true
            this.canLoadMore = true;
            console.log('============onMomentumScrollBegin===========');
          }}
          //=======修复上拉刷新调用2次和第一次进入页面时不用触发上拉加载 loadMore为true的问题--伴随reducer修改========================
        />

        {/*  Toast */}
        <Toast ref={'toast'} position={'center'} />
      </View>
    );
  }
}

function createTabNav(ts, keys) {
  tabNav = createAppContainer(
    createMaterialTopTabNavigator(genTabs(tabNames, ts, keys), {
      //配置顶部导航样式
      tabBarOptions: {
        tabStyle: styles.tabStyle,
        upperCaseLabel: false,
        scrollEnabled: true, //可以多屏滚动
        style: {
          //背景颜色
          // backgroundColor: '#a67',
          backgroundColor: ts.props.theme.themeColor,
          height: 30, //===================== !!!!!!!!!!!! fix修复初次加载tab由大到小
        },
        indicatorStyle: styles.indicatorStyle,
        labelStyle: styles.labelStyle,
      },
      lazy: true, //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!!!!!设置懒加载模式~~~~~~~~~~~~~~~~~~
    }),
  );
}

//====================================顶部导航=================================

//====================================定义动态显示的tab顶部导航=================================
function genTabs(tabNames, ts, keys) {
  const tabs = {};
  ts.preKeys = keys; //~~~~~~~~~~~~~~~~~~!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!用于刷新上一次的keys 去重================
  // tabNames.forEach((item, index) => {
  keys.forEach((item, index) => {
    if (item.checked) {
      //只有选中状态下才去添加
      //动态设置路由名
      tabs[`tab${index}`] = {
        //给组件动态设置属性
        screen: () => (
          <TrendingTabPage
            {...ts.props}
            timeSpan={tsMain.state.timeSpan}
            tabLabel={item.name}
          />
        ),
        navigationOptions: {
          title: item.name,
        },
      };
    }
  });
  return tabs;
}

//====================================定义动态显示的tab顶部导航=================================

//====================================趋势=================================
class TrendingPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      timeSpan: TimeSpans[0], //初始化时默认选中 今天
    };
    //从connect的props中取出：onLoadLanguage
    const {onLoadLanguage} = this.props;
    onLoadLanguage(FLAG_LANGUAGE.flag_language); //dispatch action 加载标签action函数

    this.preKeys = []; // 上一次从数据库langs.json加载的数据keys是否和当前相同，用于去重渲染tabs
  }

  //=====================================标题筛选 日期========================================
  renderTitleView() {
    return (
      <View>
        <TouchableOpacity
          underlayColor="transparent"
          onPress={() => this.dialog.show()}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text
              style={{
                fontSize: 18,
                color: '#FFFFFF',
                fontWeight: '400',
              }}>
              趋势 {this.state.timeSpan.showText}
            </Text>
            <MaterialIcons
              name={'arrow-drop-down'}
              size={22}
              style={{color: 'white'}}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  //=============选中其中一个tab============回调=============
  onSelectTimeSpan(tab) {
    this.dialog.dismiss(); //=====点击item关闭dialog
    this.setState({
      timeSpan: tab,
    });
    //====================筛选日期完成================
    DeviceEventEmitter.emit(EVENT_TYPE_TIME_SPAN_CHANGE, tab);
    //====================筛选日期完成================

    // loadData(this, false);
  }

  //=============选中其中一个tab============回调=============

  //========!!!为了调用dialog 内部的方法使用ref 显示弹出框============
  renderTrendingDialog() {
    return (
      <TrendingDialog
        ref={dialog => (this.dialog = dialog)} //========!!!为了调用dialog 内部的方法使用ref
        onSelect={tab => this.onSelectTimeSpan(tab)} //=============选中其中一个tab============回调=============
      />
    );
  }

  //========!!!为了调用dialog 内部的方法使用ref 显示弹出框============

  //=====================================标题筛选 日期========================================

  //=========================筛选避免重复渲染tab标签=======================
  _tabNav(keys) {
    const {theme} = this.props;
    if (
      theme !== this.theme ||
      !tabNav ||
      !ArrayUtil.isEqual(this.preKeys, this.props.keys)
    ) {
      this.theme = theme; //自定义主题避免重复渲染
      //去重渲染并区分是否数据库的keys是改变过就渲染
      createTabNav(this, keys);
    }
    return tabNav;
  }
  //=========================筛选避免重复渲染tab标签=======================

  render():
    | React.ReactElement<any>
    | string
    | number
    | {}
    | React.ReactNodeArray
    | React.ReactPortal
    | boolean
    | null
    | undefined {
    //+自定义主题
    const {theme} = this.props;
    //+自定义主题
    //=========================导入自定义顶部导航栏=======================
    let statusBar = {
      backgroundColor: theme.themeColor,
      barStyle: 'light-content', // 高亮模式
    };
    let navigationBar = (
      <NavigationBar
        title={'趋势'}
        titleView={this.renderTitleView()}
        statusBar={statusBar}
        style={{backgroundColor: theme.themeColor}}
      />
    );
    //=========================导入自定义顶部导航栏=======================

    //=========================传递趋势main this=======================
    tsMain = this;
    //=========================传递趋势main this=======================

    //=========================导入自定义顶部导航栏 从数据库langs.json=======================
    const {keys} = this.props;
    const TabNavigator = keys.length ? this._tabNav(keys) : null; //筛选避免重复渲染tab标签 和如果数据库langs.json没有数据则显示null
    //=========================导入自定义顶部导航栏 从数据库langs.json=======================

    return (
      <View
        style={
          PlatformSelects()
            ? styles.containerForAndroid
            : DeviceInfo.isIPhoneX_deprecated //=========！！！！！！！！！！判断当前设备是否使用iphonex
            ? // ? styles.containerForIos //===================由于设置了SafeAreaViewPlus 在HomePage页面 所以不用marginTop:30
              styles.containerForAndroid
            : styles.containerForAndroid
        }>
        {navigationBar}
        {/*当自定义标签keys大于0 才使用tabs控件 此处判断TabNavigator是否为null*/}
        {TabNavigator && <TabNavigator />}
        {this.renderTrendingDialog()}
      </View>
    );
  }
}
//====================================趋势=================================

//====================================onRefreshTrending使用dispatch创建函数 (方法一)  //创建actions和props关联 =================================
function loadData(ts, loadMore, refreshFavorited) {
  const {
    onRefreshTrending,
    onLoadMoreTrending,
    onFlushTrendingFavorite,
  } = ts.props;
  const store = _store(ts);
  const url = genFetchUrl(ts, ts.storeName);
  if (loadMore) {
    onLoadMoreTrending(
      ts.storeName,
      ++store.pageIndex, //==========================翻到下一页
      pageSize,
      store.items,
      favoriteDao,
      callback => {
        ts.refs.toast.show('没有更多了'); //==================action 回调函数
      }, //没有更多数据的情况了
    );
  } else if (refreshFavorited) {
    //====~~~~~~~~~~~~~~~~~~刷新收藏页面的收藏状态变化到当前页面的影响===============

    onFlushTrendingFavorite(
      ts.storeName,
      store.pageIndex,
      pageSize,
      store.items,
      favoriteDao,
    );
  } else {
    // store.pageIndex, //==========================翻到第一页 在action/popular的handleData 方法中定义了pageIndex: 1
    onRefreshTrending(ts.storeName, url, pageSize, favoriteDao); //使用dispatch创建函数 (方法一)  //创建actions和props关联 ，传入storeName和url
  }
}

function genFetchUrl(ts, key) {
  if (key === 'All') {
    return URL + ts.timeSpan.searchText;
  }
  return URL + key + ts.timeSpan.searchText;
}

function _store(ts) {
  const {trending} = ts.props;
  let store = trending[ts.storeName]; //storeName 不是固定的 需要动态获取
  if (!store) {
    //store为null 则初始化  reducer/popular/index/ 的属性defaultState  没有设置默认值

    store = {
      items: [], //原始数据
      isLoading: false,
      projectModels: [], //要显示的数据
      hideLoadingMore: true, //默认隐藏加载更多
    };
  }
  return store;
}

//====================================onRefreshPopular使用dispatch创建函数 (方法一)  //创建actions和props关联 =================================

//====================================flatlist item ui=================================
function renderItem(data, theme) {
  const item = data.item;

  //============查看返回的全部数据
  // <View style={{marginBottom: 10}}>
  //   <Text style={{backgroundColor: '#faa'}}>{JSON.stringify(item)}</Text>
  // </View>
  //============查看返回的全部数据

  return (
    //===================自定义UI Component================
    <TrendingItem
      theme={theme}
      projectModel={item} //传递到父类BaseItem
      onSelect={callback => {
        // BaseItem       {/*BaseItem 事件*/}
        NavigationUtil.goPage(
          {
            theme, //========================!!!!!!!!!!!!将自定义主题属性传入 h5 页面
            projectModel: item,
            flag: FLAG_STORAGE.flag_trending,
            callback,
          }, //传递收藏状态flag // 作为一个参数传回刷新当前的item 收藏状态
          'DetailPage',
        );
      }}
      onFavorite={(
        item,
        isFavorite, //父类BaseItem 传回
      ) => {
        FavoriteUtil.onFavorite(
          favoriteDao,
          item,
          isFavorite,
          FLAG_STORAGE.flag_trending,
        );
      }}
    />
    //===================自定义UI Component================
  );
}

//====================================flatlist item ui=================================

//====================================redux + flatlist=================================

const mapStateToProps = state => ({
  //整个store树里面的popular的state
  trending: state.trending,
});
const mapDispatchToProps = dispatch => ({
  //====================dispatch action创建函数
  onRefreshTrending: (storeName, url, pageSize, favoriteDao) =>
    //下拉刷新
    dispatch(actions.onRefreshTrending(storeName, url, pageSize, favoriteDao)), //创建actions和props关联 ，传入storeName和url
  onLoadMoreTrending: (
    storeName,
    pageIndex,
    pageSize,
    items,
    favoriteDao,
    callback,
  ) =>
    //  上拉加载
    dispatch(
      actions.onLoadMoreTrending(
        storeName,
        pageIndex,
        pageSize,
        items,
        favoriteDao,
        callback,
      ),
    ),

  //收藏页面改变收藏状态时同步到趋势页面
  onFlushTrendingFavorite: (
    storeName,
    pageIndex,
    pageSize,
    items,
    favoriteDao,
  ) =>
    dispatch(
      actions.onFlushTrendingFavorite(
        storeName,
        pageIndex,
        pageSize,
        items,
        favoriteDao,
      ),
    ),
});

const TrendingTabPage = connect(
  mapStateToProps,
  mapDispatchToProps,
)(TrendingTab); //第二个参数就是返回值  （用于让组件订阅store树）
//====================================redux + flatlist=================================

//====================================redux + trendingPage=================================
const mapTrendingStateToProps = state => ({
  keys: state.language.languages,
  theme: state.theme.theme,
});
const mapTrendingDispatchToProps = dispatch => ({
  onLoadLanguage: flag => dispatch(actions.onLoadLanguage(flag)),
});

export default connect(
  mapTrendingStateToProps,
  mapTrendingDispatchToProps,
)(TrendingPage);
//====================================redux + trendingPage=================================
