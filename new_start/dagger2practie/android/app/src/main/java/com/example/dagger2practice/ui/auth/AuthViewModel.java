package com.example.dagger2practice.ui.auth;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import androidx.lifecycle.ViewModel;

import com.example.dagger2practice.SessionManager;
import com.example.dagger2practice.models.User;
import com.example.dagger2practice.network.auth.AuthApi;

import javax.inject.Inject;

import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class AuthViewModel extends ViewModel {

    private static final String TAG = "AuthViewModel";

    private final AuthApi authApi;

    //    private MediatorLiveData<AuthResource<User>> authUser = new MediatorLiveData<>();
    private SessionManager sessionManager;

    //    public LiveData<AuthResource<User>> observeUser() {
//        return authUser;
//    }
    public LiveData<AuthResource<User>> observeAuthState() {
        return sessionManager.getAuthUser();
    }

    public void authenticateWithId(int userId) {
//        authUser.setValue(AuthResource.loading((User) null));
//        final LiveData<AuthResource<User>> source = LiveDataReactiveStreams.fromPublisher(
//                authApi.getUser(userId)
//                        .onErrorReturn(new Function<Throwable, User>() {
//                            @Override
//                            public User apply(Throwable throwable) throws Exception {
//                                User errorUser = new User();
//                                errorUser.setId(-1);
//                                return errorUser;
//                            }
//                        })
//                        .map(new Function<User, AuthResource<User>>() {
//                            @Override
//                            public AuthResource<User> apply(User user) throws Exception {
//                                if (user.getId() == -1) {
//                                    return AuthResource.error("Could not authenticate", null);
//                                }
//                                return AuthResource.authenticated(user);
//                            }
//                        })
//                        .subscribeOn(Schedulers.io())
//        );
        Log.d(TAG, "authenticateWithId: attempting to login");
//        authUser.addSource(source, new Observer<AuthResource<User>>() {
//            @Override
//            public void onChanged(AuthResource<User> user) {
//                authUser.setValue(user);
//                authUser.removeSource(source);
//            }
//        });
        sessionManager.authenticateWithId(queryUserId(userId));
    }

    private LiveData<AuthResource<User>> queryUserId(int userId) {
        final LiveData<AuthResource<User>> source = LiveDataReactiveStreams.fromPublisher(
                authApi.getUser(userId)
                        .onErrorReturn(new Function<Throwable, User>() {
                            @Override
                            public User apply(Throwable throwable) throws Exception {
                                User errorUser = new User();
                                errorUser.setId(-1);
                                return errorUser;
                            }
                        })
                        .map(new Function<User, AuthResource<User>>() {
                            @Override
                            public AuthResource<User> apply(User user) throws Exception {
                                if (user.getId() == -1) {
                                    return AuthResource.error("Could not authenticate", null);
                                }
                                return AuthResource.authenticated(user);
                            }
                        })
                        .subscribeOn(Schedulers.io())
        );
        return source;
    }

    @Inject
    public AuthViewModel(AuthApi authApi, SessionManager sessionManager) {
        this.authApi = authApi;
        this.sessionManager = sessionManager;
        Log.d(TAG, "AuthViewModel: viewmodel is working ...");
//        if (this.authApi == null) {
//            Log.d(TAG, "AuthViewModel: authapi is null");
//        } else {
//            Log.d(TAG, "AuthViewModel: authapi is not null");
//        }
//        authApi.getUser(1)
//                .toObservable()
//                .subscribeOn(Schedulers.io())
//                .subscribe(new Observer<User>() {
//                    @Override
//                    public void onSubscribe(Disposable d) {
//
//                    }
//
//                    @Override
//                    public void onNext(User user) {
//                        Log.d(TAG, "onNext: " + user.getEmail());
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        Log.e(TAG, "onError: ", e);
//
//                    }
//
//                    @Override
//                    public void onComplete() {
//
//                    }
//                });
    }
}
