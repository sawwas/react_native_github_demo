package com.example.dagger2practice.ui.main.profile;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.dagger2practice.R;
import com.example.dagger2practice.models.User;
import com.example.dagger2practice.ui.auth.AuthResource;
import com.example.dagger2practice.ui.react.RNCodePushPageActivity;
import com.example.dagger2practice.ui.react.RNPageActivity;
import com.example.dagger2practice.ui.react.ReactPageActivity;
import com.example.dagger2practice.viewmodels.ViewModelProviderFactory;
import com.facebook.react.ReactActivity;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

public class ProfileFragment extends DaggerFragment implements View.OnClickListener {
    private static final String TAG = "ProfileFragment";

    private ProfileViewModel viewModel;

    @Inject
    ViewModelProviderFactory providerFactory;

    private TextView email, username, website;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //   Toast.makeText(getActivity(), "profile fragment", Toast.LENGTH_SHORT).show();
        return inflater.inflate(R.layout.fragment_profile, container, false);


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated: profileFragment was created...");

        email = view.findViewById(R.id.email);
        username = view.findViewById(R.id.username);
        website = view.findViewById(R.id.website);

        email.setOnClickListener(this);
        website.setOnClickListener(this);

        viewModel = ViewModelProviders.of(this, providerFactory).get(ProfileViewModel.class);
        subscribeObservers();
    }

    private void subscribeObservers() {
        viewModel.getAuthenticatedUser().removeObservers(getViewLifecycleOwner());
        viewModel.getAuthenticatedUser().observe(getViewLifecycleOwner(), new Observer<AuthResource<User>>() {
            @Override
            public void onChanged(AuthResource<User> userAuthResource) {
                if (userAuthResource != null) {
                    switch (userAuthResource.status) {
                        case AUTHENTICATED:
                            setUserDetails(userAuthResource.data);
                            break;
                        case ERROR:
                            setErrorDetails(userAuthResource.message);
                            break;
                    }
                }
            }
        });
    }

    private void setErrorDetails(String message) {
        email.setText(message);
        username.setText("error");
        website.setText("error");
    }

    private void setUserDetails(User data) {
        email.setText(data.getEmail());
        username.setText(data.getUsername());
        website.setText(data.getWebsite());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.email:
//                RNPageActivity.start(getActivity(), "App2");
                RNCodePushPageActivity.start(getActivity(), "CodePushPage");
                break;

            case R.id.website:
                ReactPageActivity.start(getActivity(), "App1");
//                RNPageActivity.start(getActivity(), "App2");


                break;
        }
    }
}
