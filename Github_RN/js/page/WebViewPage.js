/**
 *H5页面
 */
import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity, View, DeviceInfo} from 'react-native';
import NavigationBar from '../common/NavigationBar';
import ViewUtil from '../util/ViewUtil';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import NavigationUtil from '../navigator/NavigationUtil';
import {WebView} from 'react-native-webview';
import {PlatformSelects} from '../utils/PlatformSelects';
import BackPressComponent from '../common/BackPressComponent';
import {NavigationActions} from 'react-navigation';
import FavoriteDao from '../expand/dao/FavoriteDao';
import SafeAreaViewPlus from '../common/SafeAreaViewPlus';

const TRENDING_URL = 'https://github.com/';
//=============================================顶部导航===========================================
//==============返回监听==============
function onBack(ts) {
  if (ts.state.canGoBack) {
    ts.webView.goBack(); //==================返回H5历史导航记录里面的上一页=======================
  } else {
    //===============返回堆栈的上一页================
    NavigationUtil.goBack(ts.props.navigation);
  }
}

//==============返回监听==============

//==============右键监听收藏状态==============
function onFavoriteButtonClick(ts) {
  const {projectModel, callback} = ts.params;
  const isFavorite = (projectModel.isFavorite = !projectModel.isFavorite); // 收藏取反
  callback(isFavorite); //更新Item的收藏状态 传回上一个页面对应的item  // BaseItem 事件
  ts.setState({
    isFavorite: isFavorite,
  });
  let key = projectModel.item.fullName
    ? projectModel.item.fullName
    : projectModel.item.id.toString();
  if (projectModel.isFavorite) {
    //保存当前的项目item
    ts.favoriteDao.saveFavoriteItem(key, JSON.stringify(projectModel.item));
  } else {
    //移除当前的项目item
    ts.favoriteDao.removeFavoriteItem(key);
  }
}
//==============右键监听收藏状态==============

//=============================================顶部导航===========================================

//=============================================监听h5是否有上一页===========================================

function onNavigationStateChange(ts, navState) {
  ts.setState({
    canGoBack: navState.canGoBack,
    url: navState.url,
  });
}

//=============================================监听h5是否有上一页===========================================

export default class WebViewPage extends Component {
  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params; //!!!!!!!取出导航器里面的params

    const {title, url} = this.params;
    this.state = {
      title: title,
      url: url,
      canGoBack: false, //H5 页面是否有上级页面
    };
    this.backPress = new BackPressComponent({
      backPress: () => this.onBackPress(),
    });
  }

  componentDidMount(): void {
    this.backPress.componentDidMount();
  }
  componentWillUnmount(): void {
    this.backPress.componentWillUnmount();
  }

  /**
   * 处理 Android 中的物理返回键
   * https://reactnavigation.org/docs/en/redux-integration.html#handling-the-hardware-back-button-in-android
   * @returns {boolean}
   */
  onBackPress() {
    onBack(this);
    return true; //===============================防止返回事件继续传递下去=============！！！！！！！！！！！！！！！
  }

  render():
    | React.ReactElement<any>
    | string
    | number
    | {}
    | React.ReactNodeArray
    | React.ReactPortal
    | boolean
    | null
    | undefined {
    const {navigation} = this.props;
    this.params = this.props.navigation.state.params; //!!!!!!!取出导航器里面的params
    const {theme, marginTops} = this.params; //===============取出自定义主题属性=============
    //=========================导入自定义顶部导航栏=======================
    let statusBar = {
      backgroundColor: theme.themeColor,
      barStyle: 'light-content', // 高亮模式
    };
    //==============标题长度 安全距离==============
    const titleLayoutStyle =
      this.state.title.length > 20 ? {paddingRight: 30} : null;
    //==============标题长度 安全距离==============
    let navigationBar = (
      <NavigationBar
        title={this.state.title}
        statusBar={statusBar}
        style={{
          backgroundColor: theme.themeColor,
          // marginTop: marginTops ? marginTops : 0, //~~~~~~~~~~~~~~~~~~~!!!@@@处理是否从关于跳入的传入25顶部处理 (safeAreaView 替换)
        }} //==================android h5  25 适配
        leftButton={ViewUtil.getLeftBackButton(() => this.onBackPress())}
      />
    );
    //=========================导入自定义顶部导航栏=======================
    return (
      <SafeAreaViewPlus topColor={theme.themeColor}>
        {navigationBar}
        <WebView
          ref={webView => (this.webView = webView)}
          startInLoadingState={true} // 开始加载时显示进度条
          onNavigationStateChange={e => onNavigationStateChange(this, e)} //监听h5是否有上一页
          source={{uri: this.state.url}}
        />

        {/*<Text style={styles.welcome}>DetailPage</Text>*/}
        {/*<Button*/}
        {/*  title={'修改主题'}*/}
        {/*  onPress={() => {*/}
        {/*    navigation.setParams({*/}
        {/*      theme: {*/}
        {/*        tintColor: 'red',*/}
        {/*        updateTime: new Date().getTime(),*/}
        {/*      },*/}
        {/*    });*/}
        {/*  }}*/}
        {/*/>*/}
      </SafeAreaViewPlus>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    marginTop: PlatformSelects() ? 0 : DeviceInfo.isIPhoneX_deprecated ? 30 : 0,
    // alignItems: 'center',
    backgroundColor: '#F5FcFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});
