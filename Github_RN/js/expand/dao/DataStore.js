import Trending from 'GitHubTrending';
import AsyncStorage from '@react-native-community/async-storage';

//======================标识区分数据来源于最热还是趋势=========================
export const FLAG_STORAGE = {
  flag_popular: 'popular',
  flag_trending: 'trending',
};
//======================标识区分数据来源于最热还是趋势=========================

/**
 * 离线缓存
 */
export default class DataStore {
  /**
   * 获取数据 优先获取本地数据 如果无本地数据或本地数据过期 则获取网络数据
   * @param url
   * @param flag
   * @returns {Promise<R>}
   */
  fetchData(url, flag) {
    return new Promise((resolve, reject) => {
      this.fetchLocalData(url)
        .then(wrapData => {
          //wrapData  为fetchLocalData 方法中JSON.parse(data) 返回的对象
          if (wrapData && DataStore.checkTimestampValid(wrapData.timestamp)) {
            resolve(wrapData);
          } else {
            this.fetchNetData(url, flag)
              .then(data => {
                resolve(this._wrapData(data));
              })
              .catch(error => {
                reject(error);
              });
          }
        })
        .catch(error => {
          this.fetchNetData(url, flag)
            .then(data => {
              resolve(this._wrapData(data));
            })
            .catch(error => {
              reject(error);
            });
        });
    });
  }

  /**
   * 存储数据
   * @param url
   * @param data
   * @param callback
   */
  saveData(url, data, callback) {
    if (!data || !url) {
      return;
    }
    AsyncStorage.setItem(url, JSON.stringify(this._wrapData(data)), callback);
  }

  _wrapData(data) {
    return {data: data, timestamp: new Date().getTime()};
  }

  /**
   * 获取本地数据
   * @param url
   * @returns {Promise<R>}
   */
  fetchLocalData(url) {
    return new Promise((resolve, reject) => {
      AsyncStorage.getItem(url, (error, result) => {
        if (!error) {
          try {
            resolve(JSON.parse(result));
          } catch (e) {
            reject(e);
            console.error(e);
          }
        } else {
          reject(error);
          console.error(error);
        }
      });
    });
  }

  /**
   * 获取网络数据
   * @param url
   * @param flag
   * @returns {Promise<R>}
   */
  fetchNetData(url, flag) {
    return new Promise((resolve, reject) => {
      if (flag !== FLAG_STORAGE.flag_trending) {
        //最热模块请求数据
        fetch(url)
          .then(response => {
            if (response.ok) {
              return response.json();
            }
            throw new Error('NetWork response was not ok');
          })
          .then(responseData => {
            this.saveData(url, responseData); //保存到本地
            resolve(responseData); //返回到ui
          })
          .catch(error => {
            reject(error);
          });
      } else {
        new Trending()
          .fetchTrending(url)
          .then(items => {
            if (!items) {
              //判断数据舒服为空
              throw new Error('responseData is null');
            }
            this.saveData(url, items);
            resolve(items);
          })
          .catch(error => {
            reject(error);
          });
      }
    });
  }

  /**
   * 检查timerstamp是否在有效期内
   * @param timestamp 项目更新时间
   * @returns {boolean} true 不需要更新 false 需要更新
   */
  static checkTimestampValid(timestamp) {
    const currentDate = new Date();
    const targetDate = new Date();
    targetDate.setTime(timestamp);
    if (currentDate.getMonth() !== targetDate.getMonth()) {
      return false;
    }
    if (currentDate.getDate() !== targetDate.getDate()) {
      return false;
    }
    if (currentDate.getHours() - targetDate.getHours() > 4) {
      return false;
    }

    return true;
  }
}
