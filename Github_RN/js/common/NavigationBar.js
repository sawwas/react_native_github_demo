import React, {Component} from 'react';
import {
  StatusBar,
  StyleSheet,
  Text,
  View,
  ViewPropTypes,
  Platform,
  DeviceInfo,
} from 'react-native';
import {PropTypes} from 'prop-types';
import {PlatformSelects} from '../utils/PlatformSelects';

const NAV_BAR_HEIGHT_IOS = 44; //ios
const NAV_BAR_HEIGHT_ANDROID = 50; //android
const STATUS_BAR_HEIGHT =
  Platform.OS !== 'ios' || DeviceInfo.isIPhoneX_deprecated ? 0 : 20; //状态栏的高度; //状态栏高度 (已经使用SafeAreaViewPlus
// 设置顶部安全区域
//所以为0; // )

//=========================================状态栏属性接收设置=========================================
const StatusBarShape = {
  barStyle: PropTypes.oneOf(['light-content', 'default']),
  hidden: PropTypes.bool,
  backgroundColor: PropTypes.string,
};
//=========================================状态栏属性接收设置=========================================

//=========================================顶部导航栏左右按钮设置=========================================
function getButtonElement(data) {
  return <View style={styles.navBarButton}>{data ? data : null}</View>;
}

//=========================================顶部导航栏左右按钮设置=========================================

/**
 * 自定义导航栏组件
 */
export default class NavigationBar extends Component {
  //============设置组件向外界提供的属性============
  static propTyles = {
    //设置属性检查
    style: ViewPropTypes.style, //设置样式
    title: PropTypes.string,
    titleView: PropTypes.element,
    titleLayoutStyle: ViewPropTypes.style,
    hide: PropTypes.bool,
    statusBar: PropTypes.shape(StatusBarShape),
    rightButton: PropTypes.element,
    leftButton: PropTypes.element,
  };
  //============设置组件向外界提供的属性============

  //============设置默认属性============
  static defaultProps = {
    // title: 'Test',
    statusBar: {
      barStyle: 'light-content', //高亮设置
      hidden: false,
    },
  };

  //============设置默认属性============

  render():
    | React.ReactElement<any>
    | string
    | number
    | {}
    | React.ReactNodeArray
    | React.ReactPortal
    | boolean
    | null
    | undefined {
    let statusBar = !this.props.statusBar.hidden ? (
      <View style={styles.statusBar}>
        <StatusBar {...this.props.statusBar} />
      </View>
    ) : null;
    let titleView = this.props.titleView ? (
      this.props.titleView
    ) : (
      //省略号处理：tail结尾 middle中间 head头部 clip裁剪
      <Text ellipsizeMode={'tail'} numberOfLines={1} style={styles.title}>
        {this.props.title}
      </Text>
    );
    let content = this.props.hide ? null : (
      <View style={styles.navBar}>
        {getButtonElement(this.props.leftButton)}
        <View
          style={[styles.navBarTitleContainer, this.props.titleLayoutStyle]}>
          {titleView}
        </View>
        {getButtonElement(this.props.rightButton)}
      </View>
    );
    return (
      <View style={[styles.container, this.props.style]}>
        {statusBar}
        {content}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  //顶部导航栏左右按钮样式
  navBarButton: {
    alignItems: 'center',
  },
  //顶部导航栏布局整体样式
  navBar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: PlatformSelects ? NAV_BAR_HEIGHT_ANDROID : NAV_BAR_HEIGHT_IOS,
  },
  //顶部导航栏中间标题样式
  navBarTitleContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute', // 绝对居中
    left: 40, //左边距
    right: 40, //右边距
    top: 0,
    bottom: 0,
  },
  //顶部导航栏中间文字文案样式
  title: {
    fontSize: 20,
    color: 'white',
  },
  //顶部导航栏布局整体颜色
  container: {
    backgroundColor: '#2196f3',
  },
  //statusBar状态栏样式
  statusBar: {
    height: PlatformSelects ? 0 : STATUS_BAR_HEIGHT,
  },
});
