package com.github_rn.util;


/**
 * 作者：tryedhp on 2017/5/24/0024 11:52
 * 邮箱：try2017yx@163.com
 */

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.github_rn.R;
import com.github_rn.util.AppUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 权限控制工具类：
 * 为了适配API23，即Android M 在清单文件中配置use permissions后，还要在程序运行的时候进行申请。
 * <p/>
 * ***整个权限的申请与处理的过程是这样的：
 * *****1.进入主Activity，首先申请所有的权限；
 * *****2.用户对权限进行授权，有2种情况：
 * ********1).用户Allow了权限，则表示该权限已经被授权，无须其它操作；
 * ********2).用户Deny了权限，则下次启动Activity会再次弹出系统的Permisssions申请授权对话框。
 * *****3.如果用户Deny了权限，那么下次再次进入Activity，会再次申请权限，这次的权限对话框上，会有一个选项“dont ask me again”：
 * ********1).如果用户勾选了“dont ask me again”的checkbox，下次启动时就必须自己写Dialog或者Snackbar引导用户到应用设置里面去手动授予权限；
 * ********2).如果用户未勾选上面的选项，若选择了Allow，则表示该权限已经被授权，无须其它操作；
 * ********3).如果用户未勾选上面的选项，若选择了Deny，则下次启动Activity会再次弹出系统的Permisssions申请授权对话框。
 */
public class PermissionsUtil {

    // 状态码、标志位
    public static final int REQUEST_STATUS_CODE = 0x001;
    public static final int REQUEST_PERMISSION_SETTING = 0x002;

    //常量字符串数组，将需要申请的权限写进去，同时必须要在Androidmanifest.xml中声明。
    public static String[] PERMISSIONS_GROUP_SORT = {
//            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    private static PermissionCallbacks callbacks;

    public interface PermissionCallbacks {

        void onPermissionsGranted();//权限都有

        void onPermissionsDenied(int requestCode, List<String> perms);

    }

    public static void setPermissionsGroupSort(@NonNull String[] permissionsGroupSort) {
        PERMISSIONS_GROUP_SORT = permissionsGroupSort;
    }

    public static void checkAndRequestPermissions(final Activity activity, final Fragment fragment, @Nullable PermissionCallbacks callback, @NonNull String[] permissionsGroupSort) {
        PERMISSIONS_GROUP_SORT = permissionsGroupSort;
        callbacks = callback;
        if (Build.VERSION.SDK_INT >= 23) {
            callbacks = callback;
            // 一个list，用来存放没有被授权的权限
            ArrayList<String> denidArray = new ArrayList<>();

            // 遍历PERMISSIONS_GROUP，将没有被授权的权限存放进denidArray
            for (String permission : PERMISSIONS_GROUP_SORT) {
                int grantCode = 0;
                if (activity != null) {
                    grantCode = ActivityCompat.checkSelfPermission(activity, permission);
                } else if (fragment != null) {
                    grantCode = ContextCompat.checkSelfPermission(fragment.getContext(), permission);
                }
                if (grantCode == PackageManager.PERMISSION_DENIED) {
                    denidArray.add(permission);
                }
            }

            // 如果该字符串数组长度大于0，说明有未被授权的权限
            if (denidArray.size() > 0) {
                //循环处理所有未授权的权限，每次只添加一个权限进行获取
                ArrayList<String> denidArrayNew = new ArrayList<>();
                denidArrayNew.add(denidArray.get(0));
                // 将denidArray转化为字符串数组，方便下面调用requestPermissions来请求授权
                String[] denidPermissions = denidArrayNew.toArray(new String[denidArrayNew.size()]);
                requestPermissions(activity, fragment, denidPermissions);
            } else {
                //已授权
                if (callbacks != null) {
                    callbacks.onPermissionsGranted();
                }
            }

        } else {
            if (callbacks != null) {
                //6.0以下系统，不需要授权
                callbacks.onPermissionsGranted();
            }
        }
    }

    /**
     * 关于shouldShowRequestPermissionRationale函数的一点儿注意事项：
     * ***1).应用安装后第一次访问，则直接返回false；
     * ***2).第一次请求权限时，用户Deny了，再次调用shouldShowRequestPermissionRationale()，则返回true；
     * ***3).第二次请求权限时，用户Deny了，并选择了“dont ask me again”的选项时，再次调用shouldShowRequestPermissionRationale()时，返回false；
     * ***4).设备的系统设置中，禁止了应用获取这个权限的授权，则调用shouldShowRequestPermissionRationale()，返回false。
     */
    public static boolean showRationaleUI(Activity activity, String permission) {
        return ActivityCompat.shouldShowRequestPermissionRationale(activity, permission);
    }

    /**
     * 对权限字符串数组中的所有权限进行申请授权，如果用户选择了“dont ask me again”，则不会弹出系统的Permission申请授权对话框
     */
    public static void requestPermissions(Activity activity, Fragment fragment, String[] permissions) {
        if (activity != null) {
            ActivityCompat.requestPermissions(activity, permissions, REQUEST_STATUS_CODE);
        } else if (fragment != null) {
            fragment.requestPermissions(permissions, REQUEST_STATUS_CODE);
        }

    }

    /**
     * 用来判断，App是否是首次启动：
     * ***由于每次调用shouldShowRequestPermissionRationale得到的结果因情况而变，因此必须判断一下App是否首次启动，才能控制好出现Dialog和SnackBar的时机
     */
    public static boolean isAppFirstRun(Activity activity) {
        SharedPreferences sp = activity.getSharedPreferences("config", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        if (sp.getBoolean("first_run", true)) {
            editor.putBoolean("first_run", false);
            editor.commit();
            return true;
        } else {
            editor.putBoolean("first_run", false);
            editor.commit();
            return false;
        }
    }

    public interface LoadedAlertDialogCallbacks {

        void cancel();//取消

        void confirm();//确认

    }

    /**
     * 去设置dialog
     */
    public static void createLoadedAlertDialog(String title, final Activity activity, @Nullable final LoadedAlertDialogCallbacks callbacks) {
        final AlertDialog dialog = new AlertDialog.Builder(activity).create();

        if (!dialog.isShowing()) {
            dialog.show();
        }
        dialog.setCancelable(true);
        final Window window = dialog.getWindow();

        window.setContentView(R.layout.permissions_dialog);
        window.clearFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);

        TextView titleTv = (TextView) window.findViewById(R.id.title_tv);//内容
        TextView titleNoticeTv = (TextView) window.findViewById(R.id.title_notice_tv);//标题
        titleNoticeTv.setText("权限申请");
        titleTv.setText("在设置-应用-" + AppUtil.getApplicationName(activity) + "-权限中开启" + title + "权限，以正常使用App功能");
        TextView cancelTv = (TextView) window.findViewById(R.id.cancel_tv); // 取消点击
        TextView okTv = (TextView) window.findViewById(R.id.ok_tv); // 确认点击

        cancelTv.setText("取消");
        okTv.setText("去设置");

        // #1 取消键
        cancelTv.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.cancel();
                if (callbacks != null) {
                    callbacks.cancel();
                }

            }
        });
        // #2 确认键
        okTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
                intent.setData(uri);
                activity.startActivityForResult(intent, PermissionsUtil.REQUEST_PERMISSION_SETTING);
                dialog.cancel();
                if (callbacks != null) {
                    callbacks.confirm();
                }
            }

        });
    }
}

