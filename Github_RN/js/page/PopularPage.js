/**
 *最热
 */
import React, {Component} from 'react';
import {
  ActivityIndicator,
  FlatList,
  TouchableOpacity,
  RefreshControl,
  StyleSheet,
  Text,
  View,
  DeviceInfo,
} from 'react-native';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';
import {createAppContainer} from 'react-navigation';
import {PlatformSelects} from '../utils/PlatformSelects';
import {connect} from 'react-redux'; //使得组件和 redux 关联
import actions from '../action/index';
import PopularItem from '../common/PopularItem';
import Toast from 'react-native-easy-toast';
import NavigationBar from '../common/NavigationBar';
import NavigationUtil from '../navigator/NavigationUtil';
import FavoriteDao from '../expand/dao/FavoriteDao';
import {FLAG_STORAGE} from '../expand/dao/DataStore';
import FavoriteUtil from '../util/FavoriteUtil';
import EventBus from 'react-native-event-bus';
import EventTypes from '../util/EventTypes';
// import DeviceInfo from 'react-native-device-info';
import {FLAG_LANGUAGE} from '../expand/dao/LanguageDao';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AnalyticsUtil from '../util/AnalyticsUtil';

const URL = 'https://api.github.com/search/repositories?q=';
const QUERY_STR = '&sort=stars';
const pageSize = 10; //每页加载的数量
const favoriteDao = new FavoriteDao(FLAG_STORAGE.flag_popular); // 实例化一个收藏对象

const styles = StyleSheet.create({
  //去掉：不然导航器显示不出来
  containerForIos: {
    flex: 1,
    marginTop: 30,
    // justifyContent: 'center',去掉：不然导航器显示不出来
    // alignItems: 'center',去掉：不然导航器显示不出来
    // backgroundColor: '#F5FcFF',去掉：不然导航器显示不出来
  },
  containerForAndroid: {
    flex: 1,
    marginTop: 0,
    // justifyContent: 'center',去掉：不然导航器显示不出来
    // alignItems: 'center',去掉：不然导航器显示不出来
    // backgroundColor: '#F5FcFF',去掉：不然导航器显示不出来
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  //  tab 内容最小宽度
  tabStyle: {
    // minWidth: 50, fix 导致tabStyle 初次加载时由小到大的闪烁
    padding: 0, //===================== !!!!!!!!!!!! fix修复初次加载tab由大到小
    alignItems: 'center',
  },
  // tab 指示器样式
  indicatorStyle: {
    height: 2,
    backgroundColor: 'white',
  },
  // tab 文字大小
  labelStyle: {
    fontSize: 13,
    marginTop: -18, //===================== !!!!!!!!!!!! fix修复初次加载tab由大到小
    // marginTop: 6, //===================== !!!!!!!!!!!! fix修复初次加载tab由大到小
    // marginBottom: 6, //===================== !!!!!!!!!!!! fix修复初次加载tab由大到小
  },
  // 上拉加载更多ui样式
  indicatorContainer: {
    alignItems: 'center',
  },
  indicator: {
    color: 'red',
    margin: 10,
  },
});

//====================================定义动态显示的tab顶部导航=================================
let tabNames = ['Java', 'Android', 'Ios', 'React', 'React Native', 'PHP'];

//====================================顶部导航=================================
/**
 * ======================================顶部导航下的内容======================================
 */
class PopularTab extends Component {
  constructor(props) {
    super(props);
    const {tabLabel} = this.props; // root tab 标签的标题 title
    this.storeName = tabLabel;
    this.isFavoriteChanged = false; //==================初始化是否监听收藏页面过来的收藏状态变化通知 (默认第一次没有变化)
  }

  componentDidMount(): void {
    loadData(this, false);
    //========================EVENTBUS切换收藏页面收藏按钮之前的状态 、 切换之后的状态 的监听================
    EventBus.getInstance().addListener(
      EventTypes.favorite_changed_popular,
      (this.favoriteChangeListener = () => {
        this.isFavoriteChanged = true;
      }),
    );
    //========================EVENTBUS切换收藏页面收藏按钮之前的状态 、 切换之后的状态 的监听================

    //========================EVENTBUS切换底部按钮之前的状态 、 切换之后的状态 的监听================
    EventBus.getInstance().addListener(
      EventTypes.bottom_tab_select,
      (this.bottomTabSelectListener = data => {
        if (data.to === 0 && this.isFavoriteChanged) {
          //收藏页面索引位置为2 当前页面的索引位置为0
          loadData(this, false, true);
        }
      }),
    );
    //========================EVENTBUS切换底部按钮之前的状态 、 切换之后的状态 的监听================
  }

  componentWillUnmount(): void {
    //========================移除 EVENTBUS切换底部按钮之前的状态 、 切换之后的状态 的监听================

    EventBus.getInstance().removeListener(this.bottomTabSelectListener);
    //========================移除 EVENTBUS切换底部按钮之前的状态 、 切换之后的状态 的监听================
    EventBus.getInstance().removeListener(this.favoriteChangeListener);
  }

  //====================================上拉加载更多UI组件=================================
  genIndicator(ts) {
    //是否隐藏上拉加载更多
    return _store(ts).hideLoadingMore ? null : (
      <View style={styles.indicatorContainer}>
        <ActivityIndicator style={styles.indicator} />
        <Text>正在加载更多</Text>
      </View>
    );
  }

  //====================================上拉加载更多UI组件=================================

  render():
    | React.ReactElement<any>
    | string
    | number
    | {}
    | React.ReactNodeArray
    | React.ReactPortal
    | boolean
    | null
    | undefined {
    let store = _store(this); //初始化默认的state

    //+ 取出自定义主题
    const {theme} = this.props;
    //+ 取出自定义主题
    return (
      <View style={styles.containerForAndroid}>
        {/*<Text*/}
        {/*  onPress={() => {*/}
        {/*    //============================================================================DetailPage 和 HomePage 在同一级，故此使用*/}
        {/*    NavigationUtil.goPage({}, 'DetailPage');*/}
        {/*  }}>*/}
        {/*  PopularTab*/}
        {/*</Text>*/}
        <FlatList
          data={store.projectModels}
          keyExtractor={item => '' + item.item.id} // item 对应projectModel
          refreshControl={
            <RefreshControl
              title={'Loading'} //刷新文字文案
              titleColor={theme.themeColor} // 刷新文字颜色
              colors={[theme.themeColor]} // 刷新样式颜色
              refreshing={store.isLoading} // 是否刷新的状态
              onRefresh={() => loadData(this, false)}
              tintColor={theme.themeColor}
            />
          }
          renderItem={data => renderItem(data, this)}
          ListFooterComponent={() => this.genIndicator(this)} //上拉加载更多UI组件
          onEndReached={() => {
            console.log('============onEndReached===========');

            //=======修复上拉刷新调用2次和第一次进入页面时不用触发上拉加载 loadMore为true的问题--伴随reducer修改========================
            setTimeout(() => {
              //==========!!!===========延迟100毫秒再触发上拉加载，避免一直在打圈圈ui--onMomentumScrollBegin执行比较晚的情况
              if (this.canLoadMore) {
                loadData(this, true);
                this.canLoadMore = false;
              }
            }, 100);
          }}
          onEndReachedThreshold={0.5} //列表可见长度距离底部的比值
          onMomentumScrollBegin={() => {
            //列表刚开始滚动的时候让上拉加载设置为true
            this.canLoadMore = true;
            console.log('============onMomentumScrollBegin===========');
          }}
          //=======修复上拉刷新调用2次和第一次进入页面时不用触发上拉加载 loadMore为true的问题--伴随reducer修改========================
        />

        {/*  Toast */}
        <Toast ref={'toast'} position={'center'} />
      </View>
    );
  }
}

//====================================顶部导航=================================

//====================================定义动态显示的tab顶部导航=================================
function genTabs(tabNames, ts) {
  //tabNames作废被替换为keys + 自定义主题
  const {keys, theme} = ts.props;
  const tabs = {};
  //=================遍历的keys是keys.json中的所有数据==================
  keys.forEach((item, index) => {
    if (item.checked) {
      //只有选中状态下才去添加
      //动态设置路由名
      tabs[`tab${index}`] = {
        //给组件动态设置属性
        screen: () => (
          <PopularTabPage {...ts.props} tabLabel={item.name} theme={theme} />
        ),
        navigationOptions: {
          title: item.name,
        },
      };
    }
  });
  return tabs;
}

//====================================定义动态显示的tab顶部导航=================================

//====================================最热=================================
class PopularPage extends Component {
  constructor(props) {
    super(props);

    //从connect的props中取出：onLoadLanguage
    const {onLoadLanguage} = this.props;
    onLoadLanguage(FLAG_LANGUAGE.flag_key); //dispatch action 加载标签action函数
  }

  //===========================跳转到搜索页面===========================
  renderRightButton() {
    const {theme} = this.props;
    return (
      <TouchableOpacity
        onPress={() => {
          AnalyticsUtil.track('SearchButtonClick');
          NavigationUtil.goPage({theme}, 'SearchPage');
        }}>
        <View style={{padding: 5, marginRight: 8}}>
          <Ionicons
            name={'ios-search'}
            size={24}
            style={{
              marginRight: 8,
              alignSelf: 'center',
              color: 'white',
            }}
          />
        </View>
      </TouchableOpacity>
    );
  }
  //===========================跳转到搜索页面===========================

  render():
    | React.ReactElement<any>
    | string
    | number
    | {}
    | React.ReactNodeArray
    | React.ReactPortal
    | boolean
    | null
    | undefined {
    // + 取出自定义主题
    const {theme} = this.props;
    // + 取出自定义主题
    //=========================导入自定义顶部导航栏=======================
    let statusBar = {
      backgroundColor: theme.themeColor,
      barStyle: 'light-content', // 高亮模式
    };
    let navigationBar = (
      <NavigationBar
        title={'最热123'}
        statusBar={statusBar}
        style={theme.styles.navBar}
        rightButton={this.renderRightButton()}
      />
    );
    //=========================导入自定义顶部导航栏 从数据库keys.json=======================
    const {keys} = this.props;
    const TabNavigator = keys.length
      ? createAppContainer(
          createMaterialTopTabNavigator(genTabs(tabNames, this), {
            //配置顶部导航样式
            tabBarOptions: {
              tabStyle: styles.tabStyle,
              upperCaseLabel: false,
              scrollEnabled: true, //可以多屏滚动
              style: {
                //背景颜色
                // backgroundColor: '#a67',
                backgroundColor: theme.themeColor,
                height: 30, //===================== !!!!!!!!!!!! fix修复初次加载tab由大到小
              },
              indicatorStyle: styles.indicatorStyle,
              labelStyle: styles.labelStyle,
            },
            lazy: true, //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~!!!!!!!设置懒加载模式~~~~~~~~~~~~~~~~~~
          }),
        )
      : null;
    //=========================导入自定义顶部导航栏 从数据库keys.json=======================

    return (
      <View
        style={
          PlatformSelects()
            ? styles.containerForAndroid
            : DeviceInfo.isIPhoneX_deprecated //=========！！！！！！！！！！判断当前设备是否使用iphonex
            ? // ? styles.containerForIos  //===================由于设置了SafeAreaViewPlus 在HomePage页面 所以不用marginTop:30
              styles.containerForAndroid
            : styles.containerForAndroid
        }>
        {navigationBar}
        {/*当自定义标签keys大于0 才使用tabs控件 此处判断TabNavigator是否为null*/}
        {TabNavigator && <TabNavigator />}
      </View>
    );
  }
}

//====================================最热=================================

//====================================onRefreshPopular使用dispatch创建函数 (方法一)  //创建actions和props关联 =================================
function loadData(ts, loadMore, refreshFavorite) {
  const {
    onRefreshPopular,
    onLoadMorePopular,
    onFlushPopularFavorite,
  } = ts.props;
  const store = _store(ts);
  const url = genFetchUrl(ts.storeName);
  if (loadMore) {
    onLoadMorePopular(
      ts.storeName,
      ++store.pageIndex, //==========================翻到下一页
      pageSize,
      store.items,
      favoriteDao,
      callback => {
        ts.refs.toast.show('没有更多了'); //==================action 回调函数
      }, //没有更多数据的情况了
    );
  } else if (refreshFavorite) {
    //=====================刷新收藏页面的收藏状态  同步=================
    onFlushPopularFavorite(
      ts.storeName,
      store.pageIndex,
      pageSize,
      store.items,
      favoriteDao,
    );
  } else {
    // store.pageIndex, //==========================翻到第一页 在action/popular的handleData 方法中定义了pageIndex: 1
    onRefreshPopular(ts.storeName, url, pageSize, favoriteDao); //使用dispatch创建函数 (方法一)  //创建actions和props关联 ，传入storeName和url
  }
}

function genFetchUrl(key) {
  return URL + key + QUERY_STR;
}

function _store(ts) {
  const {popular} = ts.props;
  let store = popular[ts.storeName]; //storeName 不是固定的 需要动态获取
  if (!store) {
    //store为null 则初始化  reducer/popular/index/ 的属性defaultState  没有设置默认值

    store = {
      items: [], //原始数据
      isLoading: false,
      projectModels: [], //要显示的数据
      hideLoadingMore: true, //默认隐藏加载更多
    };
  }
  return store;
}

//====================================onRefreshPopular使用dispatch创建函数 (方法一)  //创建actions和props关联 =================================

//====================================flatlist item ui=================================
function renderItem(data, ts) {
  const item = data.item;
  const {theme} = ts.props; //==================取出自定义主题=================
  //============查看返回的全部数据
  // <View style={{marginBottom: 10}}>
  //   <Text style={{backgroundColor: '#faa'}}>{JSON.stringify(item)}</Text>
  // </View>
  //============查看返回的全部数据

  return (
    //===================自定义UI Component================
    <PopularItem
      theme={theme} //定义自定义主题
      projectModel={item} //传递到父类BaseItem
      onSelect={callback => {
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~跳转到H5详情页~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        NavigationUtil.goPage(
          {
            theme, //========================!!!!!!!!!!!!将自定义主题属性传入 h5 页面
            projectModel: item,
            flag: FLAG_STORAGE.flag_popular,
            callback,
          }, //传递收藏状态flag
          'DetailPage',
        );
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~跳转到H5详情页~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      }}
      onFavorite={(
        item,
        isFavorite, //父类BaseItem 传回
      ) =>
        FavoriteUtil.onFavorite(
          favoriteDao,
          item,
          isFavorite,
          FLAG_STORAGE.flag_popular,
        )
      }
    />
    //===================自定义UI Component================
  );
}

//====================================flatlist item ui=================================

//====================================redux + flatlist=================================

const mapStateToProps = state => ({
  //整个store树里面的popular的state
  popular: state.popular,
});
const mapDispatchToProps = dispatch => ({
  //====================dispatch action创建函数
  onRefreshPopular: (storeName, url, pageSize, favoriteDao) =>
    //下拉刷新
    dispatch(actions.onRefreshPopular(storeName, url, pageSize, favoriteDao)), //创建actions和props关联 ，传入storeName和url
  onLoadMorePopular: (
    storeName,
    pageIndex,
    pageSize,
    items,
    favoriteDao,
    callback,
  ) =>
    //  上拉加载
    dispatch(
      actions.onLoadMorePopular(
        storeName,
        pageIndex,
        pageSize,
        items,
        favoriteDao,
        callback,
      ),
    ),
  //=============刷新收藏状态================
  onFlushPopularFavorite: (
    storeName,
    pageIndex,
    pageSize,
    items,
    favoriteDao,
  ) =>
    dispatch(
      actions.onFlushPopularFavorite(
        storeName,
        pageIndex,
        pageSize,
        items,
        favoriteDao,
      ),
    ),
});
const PopularTabPage = connect(
  mapStateToProps,
  mapDispatchToProps,
)(PopularTab); //第二个参数就是返回值  （用于让组件订阅store树）
//====================================redux + flatlist=================================

//====================================redux + popularPage=================================
const mapPopularStateToProps = state => ({
  keys: state.language.keys,
  theme: state.theme.theme, //订阅自定义主题
});
const mapPopularDispatchToProps = dispatch => ({
  onLoadLanguage: flag => dispatch(actions.onLoadLanguage(flag)),
});
export default connect(
  mapPopularStateToProps,
  mapPopularDispatchToProps,
)(PopularPage);
//====================================redux + popularPage=================================
