import Types from '../../action/types';
import ThemeFactory, {ThemeFlags} from '../../res/styles/ThemeFactory';

/**
 * 1 创建一个reducer  action 函数
 */
const defaultState = {
  theme: ThemeFactory.createTheme(ThemeFlags.Default), //默认主题
  onShowCustomThemeView: false, //不显示主题弹框
}; //state 默认值 必须创建 否则报错显示不合法的reducer
export default function onAction(state = defaultState, action) {
  switch (action.type) {
    case Types.THEME_CHANGE: //主题变化
      //不能修改state ，只能返回新的state 或之前的state
      return {...state, theme: action.theme};
    case Types.SHOW_THEME_VIEW: //主题选择弹框
      return {...state, customThemeViewVisible: action.customThemeViewVisible};
    default:
      return state;
  }
}
