/**
 *关于
 */
import React, {Component} from 'react';
import {
  ScrollView,
  Button,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  StatusBar,
  Linking,
} from 'react-native';
import actions from '../../action';
import {connect} from 'react-redux';
import NavigationUtil from '../../navigator/NavigationUtil';
import NavigationBar from '../../common/NavigationBar';
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {PlatformSelects} from '../../utils/PlatformSelects';
import {MORE_MENU} from '../../common/MORE_MENU';
import GlobalStyles from '../../res/styles/GlobalStyles';

let themes = '';
import ViewUtil from '../../util/ViewUtil';
import AboutCommon, {FLAG_ABOUT} from './AboutCommon';
import config from '../../res/data/config';

function onclick(menu, theme) {
  let RouteName, //将要跳转到的页面名称
    params = {};

  switch (menu) {
    case MORE_MENU.Tutorial: //跳转到H5
      RouteName = 'WebViewPage';
      params.theme = theme;
      params.title = '教程';
      params.marginTops = 25; // android 适配
      params.url = 'http://www.hao123.com';
      break;
    case MORE_MENU.Feedback: //跳转到邮箱
      const url = 'mailto:sandaohaizi@gmail.com';
      Linking.canOpenURL(url) //判断用户是否安装了邮箱app
        .then(support => {
          if (!support) {
            //boolean 值
            console.log("Can't handle url:" + url);
          } else {
            Linking.openURL(url);
          }
        })
        .catch(e => {
          console.error('AN error occurred ', e);
        });
      break;

    case MORE_MENU.About_Author: //跳转到作者
      RouteName = 'AboutMePage';
      params.theme = theme;
      break;
  }
  if (RouteName) {
    NavigationUtil.goPage(params, RouteName);
  }
}

export default class AboutPage extends Component {
  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;
    //=========================设置组件====================
    this.aboutCommon = new AboutCommon(
      {
        ...this.params,
        navigation: this.props.navigation,
        flagAbout: FLAG_ABOUT.flag_about,
      },
      data =>
        this.setState({
          // 将AboutCommon 中的数据传递到当前
          ...data,
        }),
    );
    this.state = {
      data: config, //获取AboutCommon中的网络数据之前默认显示本地数据
    };
  }

  getItem(menu) {
    this.params = this.props.navigation.state.params.theme.props;
    const {theme} = this.params; //=============取出自定义主题
    return ViewUtil.getMenuItem(
      () => onclick(menu, theme),
      menu,
      theme.themeColor,
    );
  }

  render():
    | React.ReactElement<any>
    | string
    | number
    | {}
    | React.ReactNodeArray
    | React.ReactPortal
    | boolean
    | null
    | undefined {
    const {navigation} = this.props;

    //============================item ui
    const content = (
      <View>
        {/*教程*/}
        {this.getItem(MORE_MENU.Tutorial)}
        <View style={GlobalStyles.line} />
        {/*关于作者*/}
        {this.getItem(MORE_MENU.About_Author)}
        <View style={GlobalStyles.line} />
        {this.getItem(MORE_MENU.Feedback)}
      </View>
    );
    //============================item ui
    return (
      <View style={{flex: 1}}>
        {/*===============================沉浸式状态栏-============================*/}
        <StatusBar
          animated={true} //指定状态栏的变化是否应以动画形式呈现。目前支持这几种样式：backgroundColor, barStyle和hidden
          hidden={false} //是否隐藏状态栏。
          backgroundColor={'transparent'} //状态栏的背景色
          translucent={true} //指定状态栏是否透明。设置为true时，应用会在状态栏之下绘制（即所谓“沉浸式”——被状态栏遮住一部分）。常和带有半透明背景色的状态栏搭配使用。
          barStyle={'light-content'} // enum('default', 'light-content', 'dark-content')
        />
        {/*===============================沉浸式状态栏-============================*/}

        {this.aboutCommon.render(content, this.state.data.app)}
      </View>
    );
  }
}
