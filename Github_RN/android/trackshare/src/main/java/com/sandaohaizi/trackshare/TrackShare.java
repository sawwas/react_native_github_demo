package com.sandaohaizi.trackshare;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;

import com.sandaohaizi.trackshare.util.Constants;
import com.umeng.commonsdk.UMConfigure;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.umeng.socialize.PlatformConfig;

/**
 * https://github.com/umeng/React_Native_Compent/blob/master/android/app/src/main/java/com/umeng/soexample/invokenative/RNUMConfigure.java
 */
public class TrackShare {
    public static void init(Context context, String appkey, String channel, int type, String secret) {

        PlatformConfig.setWeixin(Constants.KEY_WEIXIN, Constants.SECRET_WEIXIN);
        PlatformConfig.setSinaWeibo(Constants.KEY_WEIBO, Constants.SECRET_WEIBO, "http://sns.whalecloud.com");
        PlatformConfig.setQQZone(Constants.KEY_QQ, Constants.SECRET_QQ);

        initRN("react-native", "2.0");
        UMConfigure.init(context, appkey, channel, type, secret);
        UMConfigure.setLogEnabled(BuildConfig.DEBUG);//================启用log;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private static void initRN(String v, String t) {
        Method method = null;
        try {
            Class<?> config = Class.forName("com.umeng.commonsdk.UMConfigure");
            method = config.getDeclaredMethod("setWraperType", String.class, String.class);
            method.setAccessible(true);
            method.invoke(null, v, t);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
