/**
 *自定义标签 自定义语言 移除标签
 */
import React, {Component} from 'react';
import {Alert, ScrollView, StyleSheet, View} from 'react-native';
import {connect} from 'react-redux'; //使得组件和 redux 关联
import actions from '../action/index';
import PopularItem from '../common/PopularItem';
import NavigationBar from '../common/NavigationBar';
import NavigationUtil from '../navigator/NavigationUtil';
import FavoriteDao from '../expand/dao/FavoriteDao';
import {FLAG_STORAGE} from '../expand/dao/DataStore';
import FavoriteUtil from '../util/FavoriteUtil';
// import DeviceInfo from 'react-native-device-info';
import LanguageDao, {FLAG_LANGUAGE} from '../expand/dao/LanguageDao';
import BackPressComponent from '../common/BackPressComponent';
import ViewUtil from '../util/ViewUtil';
import CheckBox from 'react-native-check-box';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ArrayUtil from '../util/ArrayUtil';
import SafeAreaViewPlus from '../common/SafeAreaViewPlus';


const URL = 'https://api.github.com/search/repositories?q=';
const QUERY_STR = '&sort=stars';
const pageSize = 10; //每页加载的数量
const favoriteDao = new FavoriteDao(FLAG_STORAGE.flag_popular); // 实例化一个收藏对象

const styles = StyleSheet.create({
    //去掉：不然导航器显示不出来
    containerForIos: {
        flex: 1,
        marginTop: 30,
        // justifyContent: 'center',去掉：不然导航器显示不出来
        // alignItems: 'center',去掉：不然导航器显示不出来
        // backgroundColor: '#F5FcFF',去掉：不然导航器显示不出来
    },
    containerForAndroid: {
        flex: 1,
        marginTop: 0,
        // justifyContent: 'center',去掉：不然导航器显示不出来
        // alignItems: 'center',去掉：不然导航器显示不出来
        // backgroundColor: '#F5FcFF',去掉：不然导航器显示不出来
    },
    item: {
        flexDirection: 'row',
    },
    line: {
        flex: 1,
        height: 0.3,
        backgroundColor: 'darkgray',
    },
});

class CustomKeyPage extends Component {
    constructor(props) {
        super(props);

        this.params = this.props.navigation.state.params;
        this.backPress = new BackPressComponent({
            backPress: e => this.onBackPress(e), //安卓物理返回键监听
        });
        this.changeValues = []; //保存选中和未选中的值
        this.isRemoveKey = !!this.params.isRemoveKey; // 显示是否显示标签移除的功能
        this.languageDao = new LanguageDao(this.params.flag); //创建languageDao实例
        this.state = {
            keys: [], //保存所有的标签key
        };
    }

    componentWillUnmount(): void {
        this.backPress.componentWillUnmount();
    }

    //=============================发送actions 接收不到 需要重写getDerivedStateFromProps==========================
    static getDerivedStateFromProps(nextProps, preState) {
        // 新 旧 props 不同则更新为新的propsPower Cente
        if (preState.keys !== CustomKeyPage._keys(nextProps, null, preState)) {
            return {
                keys: CustomKeyPage._keys(nextProps, null, preState),
            };
        }
        return null; //state 不发生变化必须return null
    }

    //=============================发送actions 接收不到 需要重写getDerivedStateFromProps==========================

    componentDidMount(): void {
        this.backPress.componentDidMount();
        //如果props中标签为空则从本地存储中获取标签
        if (CustomKeyPage._keys(this.props).length === 0) {
            let {onLoadLanguage} = this.props;
            onLoadLanguage(this.params.flag); //发送actions 接收不到 需要重写getDerivedStateFromProps
        }
        this.setState({
            keys: CustomKeyPage._keys(this.props),
        });
    }

    //===============处理安卓物理返回键===============

    onBack() {
        //=================如果改变了列表的选中状态则弹出是否保存的对话款================
        if (this.changeValues.length > 0) {
            Alert.alert('提示', '要保存修改吗？', [
                {
                    text: '否',
                    onPress: () => {
                        NavigationUtil.goBack(this.props.navigation);
                    },
                },
                {
                    text: '是',
                    onPress: () => {
                        this.onSave();
                    },
                },
            ]);
        }
        //=================如果改变了列表的选中状态则弹出是否保存的对话款================
        else {
            NavigationUtil.goBack(this.props.navigation);
        }
    }

    onBackPress(e) {
        this.onBack();
        return true;
    }

    //===============处理安卓物理返回键===============

    /**
     * 获取标签
     * @param props
     * @param original 移除标签时使用，是否从props获取原始对的标签
     * @param state 移除标签时使用
     * @returns {*}
     * @private
     */
    static _keys(props, original, state) {
        const {flag, isRemoveKey} = props.navigation.state.params; //取出flag（从最热还是趋势），isRemoveKey是否显示移除标签功能
        let key = flag === FLAG_LANGUAGE.flag_key ? 'keys' : 'languages';
        //========================标签移除处理====================
        if (isRemoveKey && !original) {
            //如果state中的keys为空则从props中取
            return (
                (state && state.keys && state.keys.length !== 0 && state.keys) ||
                props.language[key].map(val => {
                    return {
                        //注意：不直接修改props，copy一份
                        ...val,
                        checked: false,
                    };
                })
            );
        } else {
            //取出原始数据
            return props.language[key];
        }
    }

    //=================保存此次的变更================
    onSave() {
        //未做任何操作直接返回上一页
        if (this.changeValues.length === 0) {
            NavigationUtil.goBack(this.props.navigation);
            return;
        }
        //==========移除标签的特殊处理=========
        let keys; //暂存移除标签的原始数据
        if (this.isRemoveKey) {
            for (let i = 0, l = this.changeValues.length; i < l; i++) {
                ArrayUtil.remove(
                    (keys = CustomKeyPage._keys(this.props, true)),
                    this.changeValues[i],
                    'name', //将keys.json 数据中的name作为唯一标识
                );
            }
        }
        //==========移除标签的特殊处理=========

        //更新本地数据
        this.languageDao.save(keys || this.state.keys); //keys不为null则使用keys说明是标签移除，否则使用state保存数据结果

        //=============将保存的结果实时同步到最热和趋势的tab标签中=============
        const {onLoadLanguage} = this.props;
        //更新store
        onLoadLanguage(this.params.flag);
        //=============将保存的结果实时同步到最热和趋势的tab标签中=============

        //保存完成后返回上一页
        NavigationUtil.goBack(this.props.navigation);
    }

    //=================保存此次的变更================

    //==================item ui=====================
    renderView() {
        let dataArray = this.state.keys;
        if (!dataArray || dataArray.length === 0) {
            return;
        }
        let len = dataArray.length;
        let views = [];
        for (let i = 0, l = len; i < l; i += 2) {
            // for循环添加2个元素
            views.push(
                <View keys={i}>
                    <View style={styles.item}>
                        {/*渲染第i个元素*/}
                        {this.renderCheckBox(dataArray[i], i)}
                        {/*渲染第i+1个元素*/}
                        {i + 1 < len && this.renderCheckBox(dataArray[i + 1], i + 1)}
                    </View>
                    {/*分割线*/}
                    <View style={styles.line}/>
                </View>,
            );
        }
        return views; //!!!!!!!!!!!!!讲列表的view 返回出去
    }

    /**
     * 生成checkbox
     * @param data
     * @param index
     * @returns {*}
     */
    onClick(data, index) {
        //==============处理CheckBox 选中状态===================
        data.checked = !data.checked;
        //如果changeValues存在data则移除，否则添加
        ArrayUtil.updateArray(this.changeValues, data);
        this.state.keys[index] = data; //更新第几个keys
        this.setState({
            keys: this.state.keys,
        });

        //==============处理CheckBox 选中状态===================
    }

    _checkedImage(checked) {
        const {theme} = this.params;
        return (
            <Ionicons
                name={checked ? 'ios-checkbox' : 'md-square-outline'}
                size={20}
                style={{
                    color: theme.themeColor,
                }}
            />
        );
    }

    renderCheckBox(data, index) {
        return !data ? null : (
            <CheckBox
                style={{flex: 1, padding: 10}}
                onClick={() => this.onClick(data, index)}
                isChecked={data.checked}
                leftText={data.name}
                checkedImage={this._checkedImage(true)}
                unCheckedImage={this._checkedImage(false)}
            />
        );
    }

    //==================item ui=====================
    render():
        | React.ReactElement<any>
        | string
        | number
        | {}
        | React.ReactNodeArray
        | React.ReactPortal
        | boolean
        | null
        | undefined {
        this.params = this.props.navigation.state.params;
        const {theme} = this.params; //=================取出自定义主题
        //=========================导入自定义顶部导航栏=======================
        let statusBar = {
            backgroundColor: theme.themeColor,
            barStyle: 'light-content', // 高亮模式
        };
        let title = this.isRemoveKey ? '标签移除' : '自定义标签';
        title =
            this.params.flag === FLAG_LANGUAGE.flag_language ? '自定义语言' : title;
        let rightButtonTitle = this.isRemoveKey ? '移除' : '保存';
        let navigationBar = (
            <NavigationBar
                title={title}
                statusBar={statusBar}
                style={{backgroundColor: theme.themeColor}}
                leftButton={ViewUtil.getLeftBackButton(() => this.onBack())}
                rightButton={ViewUtil.getRightButton(rightButtonTitle, () =>
                    this.onSave(),
                )}
            />
        );

        return (
            <SafeAreaViewPlus topColor={theme.themeColor}>

                {navigationBar}
                <ScrollView>{this.renderView()}</ScrollView>
            </SafeAreaViewPlus>
        );
    }
}

//====================================最热=================================

//====================================onRefreshPopular使用dispatch创建函数 (方法一)  //创建actions和props关联 =================================
function loadData(ts, loadMore, refreshFavorite) {
    const {
        onRefreshPopular,
        onLoadMorePopular,
        onFlushPopularFavorite,
    } = ts.props;
    const store = _store(ts);
    const url = genFetchUrl(ts.storeName);
    if (loadMore) {
        onLoadMorePopular(
            ts.storeName,
            ++store.pageIndex, //==========================翻到下一页
            pageSize,
            store.items,
            favoriteDao,
            callback => {
                ts.refs.toast.show('没有更多了'); //==================action 回调函数
            }, //没有更多数据的情况了
        );
    } else if (refreshFavorite) {
        //=====================刷新收藏页面的收藏状态  同步=================
        onFlushPopularFavorite(
            ts.storeName,
            store.pageIndex,
            pageSize,
            store.items,
            favoriteDao,
        );
    } else {
        // store.pageIndex, //==========================翻到第一页 在action/popular的handleData 方法中定义了pageIndex: 1
        onRefreshPopular(ts.storeName, url, pageSize, favoriteDao); //使用dispatch创建函数 (方法一)  //创建actions和props关联 ，传入storeName和url
    }
}

function genFetchUrl(key) {
    return URL + key + QUERY_STR;
}

function _store(ts) {
    const {popular} = ts.props;
    let store = popular[ts.storeName]; //storeName 不是固定的 需要动态获取
    if (!store) {
        //store为null 则初始化  reducer/popular/index/ 的属性defaultState  没有设置默认值

        store = {
            items: [], //原始数据
            isLoading: false,
            projectModels: [], //要显示的数据
            hideLoadingMore: true, //默认隐藏加载更多
        };
    }
    return store;
}

//====================================onRefreshPopular使用dispatch创建函数 (方法一)  //创建actions和props关联 =================================

//====================================flatlist item ui=================================
function renderItem(data) {
    const item = data.item;
    //============查看返回的全部数据
    // <View style={{marginBottom: 10}}>
    //   <Text style={{backgroundColor: '#faa'}}>{JSON.stringify(item)}</Text>
    // </View>
    //============查看返回的全部数据

    return (
        //===================自定义UI Component================
        <PopularItem
            projectModel={item} //传递到父类BaseItem
            onSelect={callback => {
                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~跳转到H5详情页~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                NavigationUtil.goPage(
                    {projectModel: item, flag: FLAG_STORAGE.flag_popular, callback}, //传递收藏状态flag
                    'DetailPage',
                );
                //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~跳转到H5详情页~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            }}
            onFavorite={(
                item,
                isFavorite, //父类BaseItem 传回
            ) =>
                FavoriteUtil.onFavorite(
                    favoriteDao,
                    item,
                    isFavorite,
                    FLAG_STORAGE.flag_popular,
                )
            }
        />
        //===================自定义UI Component================
    );
}

//====================================flatlist item ui=================================

//====================================redux + popularPage=================================
const mapPopularStateToProps = state => ({
    language: state.language, //取最外层的节点，既包含keys又包含language
});
const mapPopularDispatchToProps = dispatch => ({
    onLoadLanguage: flag => dispatch(actions.onLoadLanguage(flag)),
});
export default connect(
    mapPopularStateToProps,
    mapPopularDispatchToProps,
)(CustomKeyPage);
//====================================redux + popularPage=================================
