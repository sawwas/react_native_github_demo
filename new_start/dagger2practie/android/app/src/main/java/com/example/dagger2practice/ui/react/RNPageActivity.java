package com.example.dagger2practice.ui.react;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;

import androidx.appcompat.app.AppCompatActivity;

import com.example.dagger2practice.MainActivity;
import com.example.dagger2practice.R;
import com.example.dagger2practice.ui.auth.AuthActivity;
import com.facebook.infer.annotation.Assertions;
import com.facebook.react.ReactActivity;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactRootView;
import com.facebook.react.common.LifecycleState;
import com.facebook.react.devsupport.DoubleTapReloadRecognizer;
import com.facebook.react.modules.core.DefaultHardwareBackBtnHandler;
import com.facebook.react.shell.MainReactPackage;

public class RNPageActivity  extends ReactActivity {

    private static String moduleName;

    public static void start(Activity activity, String moduleNames) {
        moduleName = moduleNames;
        activity.startActivity(new Intent(activity, RNPageActivity.class));
    }
    /**
     * Returns the name of the main component registered from JavaScript. This is used to schedule
     * rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return moduleName;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    //======================开发者setting===================

}
