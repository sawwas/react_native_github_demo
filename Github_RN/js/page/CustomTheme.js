import React, {Component} from 'react';
import {
  Modal,
  Text,
  TouchableOpacity,
  StyleSheet,
  View,
  Platform,
  DeviceInfo,
  ScrollView,
  TouchableHighlight,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import TimeSpan from '../model/TimeSpan';
import ThemeDao from '../expand/dao/ThemeDao';
import GlobalStyles from '../res/styles/GlobalStyles';
import ThemeFactory, {ThemeFlags} from '../res/styles/ThemeFactory';
import {connect} from 'react-redux';
import actions from '../action';
/**
 * 自定义主题弹出框 UI 布局
 */
class CustomTheme extends Component {
  constructor(props) {
    super(props);
    this.themeDao = new ThemeDao();
  }

  show() {
    this.setState({
      visible: true,
    });
  }

  dismiss() {
    this.setState({
      visible: false,
    });
  }

  onSelectTheme(themeKey) {
    this.props.onClose(); //告诉外层调用关闭了本弹出框
    this.themeDao.save(ThemeFlags[themeKey]);
    const {onThemeChange} = this.props;
    onThemeChange(ThemeFactory.createTheme(ThemeFlags[themeKey]));
  }

  //======================渲染ui=================
  /**
   * 创建主题Item
   * @param themeKey
   */
  getThemeItem(themeKey) {
    return (
      <TouchableHighlight
        style={{flex: 1}}
        underlayColor="white"
        onPress={() => this.onSelectTheme(themeKey)}>
        <View
          style={[{backgroundColor: ThemeFlags[themeKey]}, styles.themeItem]}>
          <Text style={styles.themeText}>{themeKey}</Text>
        </View>
      </TouchableHighlight>
    );
  }
  renderThemeItems() {
    const views = [];
    for (
      let i = 0, keys = Object.keys(ThemeFlags), l = keys.length;
      i < l;
      i += 3
    ) {
      const key1 = keys[i],
        key2 = keys[i + 1],
        key3 = keys[i + 2];
      views.push(
        <View key={i} style={{flexDirection: 'row'}}>
          {this.getThemeItem(key1)}
          {this.getThemeItem(key2)}
          {this.getThemeItem(key3)}
        </View>,
      );
    }
    return views;
  }
  renderContentView() {
    return (
      <Modal
        animationType={'slide'}
        transparent={true}
        visible={this.props.visible}
        onRequestClose={() => {
          this.props.onClose();
        }}>
        <View style={styles.modalContainer}>
          <ScrollView>{this.renderThemeItems()}</ScrollView>
        </View>
      </Modal>
    );
  }
  //======================渲染ui=================
  render():
    | React.ReactElement<any>
    | string
    | number
    | {}
    | React.ReactNodeArray
    | React.ReactPortal
    | boolean
    | null
    | undefined {
    let view = this.props.visible ? (
      <View style={GlobalStyles.root_container}>
        {this.renderContentView()}
      </View>
    ) : null;
    return view;
  }
}

const styles = StyleSheet.create({
  //=====设置灰色蒙版==============
  container: {
    backgroundColor: 'rgba(0,0,0,0.6)',
    flex: 1,
    alignItems: 'center',
    paddingTop: DeviceInfo.isIPhoneX_deprecated ? 30 : 0,
  },
  //=====设置带箭头背景=============

  arrow: {
    marginTop: 40,
    color: 'white',
    padding: 0,
    margin: -15,
  },
  //=====弹框内容布局=============
  content: {
    backgroundColor: 'white',
    borderRadius: 3,
    paddingTop: 3,
    paddingBottom: 3,
    marginRight: 3,
  },
  //=====弹框文字布局=============
  text_container: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  //=====弹框文字样式=============
  text: {
    fontSize: 16,
    color: 'black',
    fontWeight: '400', //文字粗细
    padding: 8,
    paddingLeft: 26,
    paddingRight: 26,
  },
  //=====弹框文字下划线样式=============
  line: {
    height: 0.3,
    backgroundColor: 'darkgray',
  },
  modalContainer: {
    flex: 1,
    margin: 10,
    marginTop: Platform.OS === 'ios' ? 20 : 10,
    backgroundColor: 'white',
    borderRadius: 3,
    shadowColor: 'gray',
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 0.5,
    shadowRadius: 2,
    padding: 3,
  },
  themeItem: {
    flex: 1,
    height: 120,
    margin: 3,
    padding: 3,
    borderRadius: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },

  themeText: {
    color: 'white',
    fontWeight: '500',
    fontSize: 16,
  },
});

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({
  onThemeChange: theme => dispatch(actions.onThemeChange(theme)),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CustomTheme);
