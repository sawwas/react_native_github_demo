import Types from './types';
import ProjectModel from '../model/ProjectModel';
import Utils from '../util/Utils';

/**
 * 处理下拉刷新的数据
 * @param actionType
 * @param dispatch
 * @param storeName
 * @param data
 * @param pageSize
 * @param favoriteDao
 * @param params 其他参数
 */
export function handleData(
  actionType,
  dispatch,
  storeName,
  data,
  pageSize,
  favoriteDao,
  params,
) {
  let fixItems = [];
  // 判断数据不为null
  if (data && data.data) {
    if (Array.isArray(data.data)) {
      fixItems = data.data;
    } else if (Array.isArray(data.data.items)) {
      fixItems = data.data.items;
    }
  }
  //第一次要加载的数据
  const showItems =
    pageSize > fixItems.length ? fixItems : fixItems.slice(0, pageSize);
  _projectModel(showItems, favoriteDao, projectModels => {
    //返回 dispatch action 数据流
    dispatch({
      type: actionType,
      items: fixItems, //======================!!!====================修复下拉刷新同时显示上拉加载ui
      projectModels: projectModels,
      storeName,
      pageIndex: 1, //==================关键 ！！！请求第一条数据==================~！！！
      ...params, //================解构赋值 将所有的参数赋值给items=============
    });
  });
}
//==================关键 ！！！将原始数据再包一层用于存储收藏的key==================~！！！
/**
 * 通过本地的收藏状态来包装item
 * @param showItems
 * @param favoriteDao
 * @param callback
 * @returns {Promise<void>}
 */
export async function _projectModel(showItems, favoriteDao, callback) {
  let keys = [];
  try {
    //获取收藏的key
    keys = await favoriteDao.getFavoriteKeys(); //~~~~~~~~~~~~~~~~~~~Promise 异步转同步 async await=~~~~~~~~~~~~~~~~~~
  } catch (e) {
    console.log(e);
  }

  //==================将 keys 和当前要显示的showItems 组合成projectModel-----=====================
  let projectModels = [];
  for (let i = 0, len = showItems.length; i < len; i++) {
    projectModels.push(
      new ProjectModel(showItems[i], Utils.checkFavorite(showItems[i], keys)), //============================检查该Item是否被收藏
    );
  }

  if (typeof callback === 'function') {
    doCallBack(callback, projectModels);
  }
}
//==================关键 ！！！将原始数据再包一层用于存储收藏的key==================~！！！

/**
 * action 函数 回调封装
 * @param callBack
 * @param object
 */
export const doCallBack = (callBack, object) => {
  if (typeof callBack === 'function') {
    callBack(object);
  }
};
