import Types from '../types';
import ThemeDao from '../../expand/dao/ThemeDao';

/**
 * 监听主题改变 同步action 函数
 * @param theme 传入主题
 */
export function onThemeChange(theme) {
  return {type: Types.THEME_CHANGE, theme: theme}; //返回纯函数
}

/**
 * 初始化主题
 * @returns {Function}
 */
export function onThemeInit() {
  return dispatch => {
    new ThemeDao().getTheme().then(data => {
      dispatch(onThemeChange(data));
    });
  };
}
/**
 * 显示自定义主题浮层
 * @param show
 * @returns {{type: *, customThemeViewVisible: *}}
 */
export function onShowCustomThemeView(show) {
  return {type: Types.SHOW_THEME_VIEW, customThemeViewVisible: show};
}
