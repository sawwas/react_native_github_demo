import React from 'react';
import {AsyncStorage} from 'react-native';

import langs from '../../res/data/langs';
import keys from '../../res/data/keys';
import ThemeFactory, {ThemeFlags} from '../../res/styles/ThemeFactory';

const THEME_KEY = 'theme_key'; //从数据库中获取主题
/**
 * 自定义主题工具类
 */
export default class ThemeDao {
  //=============================返回json文件被反序列化成的对象===========================
  //获取当前的主题
  getTheme() {
    return new Promise((resolve, reject) => {
      /**
       * 获取语言或标签
       */
      AsyncStorage.getItem(THEME_KEY, (error, result) => {
        if (error) {
          reject(error);
          return;
        }
        if (!result) {
          this.save(ThemeFlags.Default);
          result = ThemeFlags.Default;
        }
        resolve(ThemeFactory.createTheme(result)); //如果result存在，则直接返回解析之后的对象给调用者
      });
    });
  }
  //=============================返回json文件被反序列化成的对象===========================

  /**
   * 保存主题标识
   * @param themeFlag
   */
  save(themeFlag) {
    AsyncStorage.setItem(THEME_KEY, themeFlag, error => {});
  }
}
