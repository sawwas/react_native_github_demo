import React, {Component} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import CommonPage from './CommonPage';

/**
 * 引导页
 */
export default class App1 extends Component {
  componentDidMount(): void {
    this.timer = setTimeout(() => {
      // SplashScreen.hide(); //==============隐藏白屏优化=================
      //跳转到首页
      // NavigationUtil.resetToHomePage(this.props);
    }, 200);
  }

  componentWillUnmount(): void {
    //页面销毁时，清空计时器
    // this.timer && clearTimeout(this.timer);
  }

  render():
    | React.ReactElement<any>
    | string
    | number
    | {}
    | React.ReactNodeArray
    | React.ReactPortal
    | boolean
    | null
    | undefined {
    return (
      <View style={{flex: 1}}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TouchableOpacity underlayColor={'transparent'}>
            <Text style={{height: 50, fontWeight: '100', fontSize: 16}}>
              左上角
            </Text>
          </TouchableOpacity>

          <TouchableOpacity underlayColor={'transparent'}>
            <Text style={{height: 50, fontWeight: '100', fontSize: 16}}>
              右上角
            </Text>
          </TouchableOpacity>
        </View>

        <CommonPage
          name={'app1第五次更新123123'}
          style={{backgroundColor: 'transparent'}}
        />

        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
          <TouchableOpacity underlayColor={'transparent'}>
            <Text style={{height: 50, fontWeight: '100', fontSize: 16}}>
              左下角
            </Text>
          </TouchableOpacity>

          <TouchableOpacity underlayColor={'transparent'}>
            <Text style={{height: 50, fontWeight: '100', fontSize: 16}}>
              右下角
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
