import React, {Component} from 'react';
import {Provider} from 'react-redux';
import AppNavigator from './navigator/AppNavigators';
import store from './store';

export default class App extends Component {
  render():
    | React.ReactElement<any>
    | string
    | number
    | {}
    | React.ReactNodeArray
    | React.ReactPortal
    | boolean
    | null
    | undefined {
    /**
     * 3 将store传递给app框架
     */
    return (
      <Provider store={store}>
        <AppNavigator />
      </Provider>
    );
  }
}
