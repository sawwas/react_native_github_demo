package com.github_rn.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.github_rn.MainApplication;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PreferencesUtil {

    public static final String PREFERENCE_NAME = "MGTVCommon";
    public static final String ERROR_LIST = "ERROR";

    /**
     * APP版本号
     */
    public static final String APPVERSION = "versionCode";

    public static boolean putString(String key, String value) {
//        LogUtil.i("hanjian", "===in  putString=====" + key + " ,context=" + MainApplication.getContext());
        SharedPreferences settings = MainApplication.getContext().getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        return editor.commit();
    }

    public static String getString(String key) {
        return getString(key, "");
    }

    public static String getString(String key, String defaultValue) {
        SharedPreferences settings = MainApplication.getContext().getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        return settings.getString(key, defaultValue);
    }

    public static boolean putInt(String key, int value) {
        SharedPreferences settings = MainApplication.getContext().getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, value);
        return editor.commit();
    }

    public static int getInt(String key) {
        return getInt(key, -1);
    }

    public static int getInt(String key, int defaultValue) {
        SharedPreferences settings = MainApplication.getContext().getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        return settings.getInt(key, defaultValue);
    }

    public static boolean putLong(String key, long value) {
        SharedPreferences settings = MainApplication.getContext().getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(key, value);
        return editor.commit();
    }

    public static long getLong(String key) {
        return getLong(key, -1);
    }

    public static long getLong(String key, long defaultValue) {
        SharedPreferences settings = MainApplication.getContext().getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        return settings.getLong(key, defaultValue);
    }

    public static boolean putFloat(String key, float value) {
        SharedPreferences settings = MainApplication.getContext().getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putFloat(key, value);
        return editor.commit();
    }

    public static float getFloat(String key) {
        return getFloat(key, -1);
    }

    public static float getFloat(String key, float defaultValue) {
        SharedPreferences settings = MainApplication.getContext().getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        return settings.getFloat(key, defaultValue);
    }

    public static boolean putBoolean(String key, boolean value) {
        SharedPreferences settings = MainApplication.getContext().getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(key, value);
        return editor.commit();
    }

    public static boolean getBoolean(String key) {
        return getBoolean(key, false);
    }

    public static boolean getBoolean(String key, boolean defaultValue) {
        if (MainApplication.getContext() == null) {
            return false;
        }
        SharedPreferences settings;
        settings = MainApplication.getContext().getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);

        return settings.getBoolean(key, defaultValue);
    }

    public static void removeErrorKey(String key) {
        SharedPreferences settings = MainApplication.getContext().getSharedPreferences(ERROR_LIST, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(key).commit();

    }

    public static Map getAllErrorKey() {
        SharedPreferences settings = MainApplication.getContext().getSharedPreferences(ERROR_LIST, Context.MODE_PRIVATE);
        return settings.getAll();

    }

    public static boolean putErrorString(String key, String value) {
        SharedPreferences settings = MainApplication.getContext().getSharedPreferences(ERROR_LIST, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        return editor.commit();
    }

    public static boolean putStringList(String key, List<String> strings) {
        SharedPreferences settings = MainApplication.getContext().getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        JSONArray jsonArray = new JSONArray(strings);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, jsonArray.toString());
        return editor.commit();
    }

    public static List<String> getStringList(String key, List<String> defValues) {
        if (defValues == null) {
            defValues = new ArrayList<String>();
        }
        SharedPreferences settings = MainApplication.getContext().getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        try {
            JSONArray jsonArray = new JSONArray(settings.getString(key, ""));
            defValues = new ArrayList<String>();
            for (int i = 0; i < jsonArray.length(); i++) {
                defValues.add(jsonArray.getString(i));
            }
        } catch (JSONException e) {
            return defValues;
        }
        return defValues;
    }

    public static List<String> getStringList(String key) {
        return getStringList(key, new ArrayList<String>());
    }

}
