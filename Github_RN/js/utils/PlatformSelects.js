import {Platform} from 'react-native';

/**
 * 判断平台 android  ios
 * @returns {boolean}
 * @constructor
 */
export function PlatformSelects(): boolean {
  if (Platform.OS === 'android') {
    //Android平台需要运行的代码
    return true;
  } else {
    //iOS平台需要运行的代码
    return false;
  }
}
