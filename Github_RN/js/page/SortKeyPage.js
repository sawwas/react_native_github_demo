/**
 *自定义标签 自定义语言 移除标签
 */
import React, {Component} from 'react';
import {
  TouchableHighlight,
  ActivityIndicator,
  FlatList,
  RefreshControl,
  StyleSheet,
  Text,
  View,
  Alert,
  DeviceInfo,
  ScrollView,
} from 'react-native';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';
import {createAppContainer} from 'react-navigation';
import {PlatformSelects} from '../utils/PlatformSelects';
import {connect} from 'react-redux'; //使得组件和 redux 关联
import actions from '../action/index';
import PopularItem from '../common/PopularItem';
import Toast from 'react-native-easy-toast';
import NavigationBar from '../common/NavigationBar';
import NavigationUtil from '../navigator/NavigationUtil';
import FavoriteDao from '../expand/dao/FavoriteDao';
import {FLAG_STORAGE} from '../expand/dao/DataStore';
import FavoriteUtil from '../util/FavoriteUtil';
import EventBus from 'react-native-event-bus';
import EventTypes from '../util/EventTypes';
// import DeviceInfo from 'react-native-device-info';
import LanguageDao, {FLAG_LANGUAGE} from '../expand/dao/LanguageDao';
import BackPressComponent from '../common/BackPressComponent';
import ViewUtil from '../util/ViewUtil';
import CheckBox from 'react-native-check-box';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import ArrayUtil from '../util/ArrayUtil';
import SortableListView from 'react-native-sortable-listview';
import SafeAreaViewPlus from '../common/SafeAreaViewPlus';

const URL = 'https://api.github.com/search/repositories?q=';
const QUERY_STR = '&sort=stars';
const pageSize = 10; //每页加载的数量
const favoriteDao = new FavoriteDao(FLAG_STORAGE.flag_popular); // 实例化一个收藏对象

const styles = StyleSheet.create({
  //去掉：不然导航器显示不出来
  containerForIos: {
    flex: 1,
    marginTop: 30,
    // justifyContent: 'center',去掉：不然导航器显示不出来
    // alignItems: 'center',去掉：不然导航器显示不出来
    // backgroundColor: '#F5FcFF',去掉：不然导航器显示不出来
  },
  containerForAndroid: {
    flex: 1,
    marginTop: 0,
    // justifyContent: 'center',去掉：不然导航器显示不出来
    // alignItems: 'center',去掉：不然导航器显示不出来
    // backgroundColor: '#F5FcFF',去掉：不然导航器显示不出来
  },
  hidden: {
    height: 0,
  },
  item: {
    backgroundColor: '#F8F8F8',
    borderBottomWidth: 1,
    borderColor: '#eee',
    height: 50,
    justifyContent: 'center',
  },
  line: {
    flex: 1,
    height: 0.3,
    backgroundColor: 'darkgray',
  },
});

//====================================定义动态显示的tab顶部导航=================================
let tabNames = ['Java', 'Android', 'Ios', 'React', 'React Native', 'PHP'];

//====================================顶部导航=================================

//====================================顶部导航=================================

//====================================最热=================================
class SortKeyPage extends Component {
  constructor(props) {
    super(props);

    this.params = this.props.navigation.state.params;
    this.backPress = new BackPressComponent({
      backPress: e => this.onBackPress(e), //安卓物理返回键监听
    });
    this.languageDao = new LanguageDao(this.params.flag); //创建languageDao实例
    this.state = {
      checkedArray: SortKeyPage._keys(this.props), //存放已经选择的标签集合
    };
  }
  componentWillUnmount(): void {
    this.backPress.componentWillUnmount();
  }
  //=============================发送actions 接收不到 需要重写getDerivedStateFromProps==========================
  static getDerivedStateFromProps(nextProps, preState) {
    const checkedArray = SortKeyPage._keys(nextProps, null, preState);
    // 新 旧 props 不同则更新为新的propsPower Cente
    if (preState.keys !== checkedArray) {
      return {
        //state 同步 checkedArray
        keys: checkedArray,
      };
    }
    return null; //state 不发生变化必须return null
  }
  //=============================发送actions 接收不到 需要重写getDerivedStateFromProps==========================

  componentDidMount(): void {
    this.backPress.componentDidMount();
    //如果props中标签为空则从本地存储中获取标签
    if (SortKeyPage._keys(this.props).length === 0) {
      let {onLoadLanguage} = this.props;
      onLoadLanguage(this.params.flag); //发送actions 接收不到 需要重写getDerivedStateFromProps
    }
  }
  //===============处理安卓物理返回键===============

  onBack() {
    //=================如果改变了列表的顺序则弹出是否保存的对话款================
    if (
      !ArrayUtil.isEqual(SortKeyPage._keys(this.props), this.state.checkedArray)
    ) {
      Alert.alert('提示', '要保存修改吗？', [
        {
          text: '否',
          onPress: () => {
            NavigationUtil.goBack(this.props.navigation);
          },
        },
        {
          text: '是',
          onPress: () => {
            this.onSave(true);
          },
        },
      ]);
    }
    //=================如果改变了列表的选中状态则弹出是否保存的对话款================
    else {
      NavigationUtil.goBack(this.props.navigation);
    }
  }
  onBackPress(e) {
    this.onBack();
    return true;
  }
  //===============处理安卓物理返回键===============

  /**
   * 获取排序后的标签结果
   * @returns {Array}
   */
  getSortResult() {
    const flag = SortKeyPage._flag(this.props);
    //从原始数据中复制一份数据出来，以便对这份数据进行进行排序
    let sortResultArray = ArrayUtil.clone(this.props.language[flag]);
    //获取排序之前的排列顺序
    const originalCheckedArray = SortKeyPage._keys(this.props);
    //遍历排序之前的数据，用排序后的数据checkedArray进行替换
    for (let i = 0, j = originalCheckedArray.length; i < j; i++) {
      let item = originalCheckedArray[i];
      //找到要替换的元素所在位置
      let index = this.props.language[flag].indexOf(item);
      //进行替换
      sortResultArray.splice(index, 1, this.state.checkedArray[i]);
    }
    return sortResultArray;
  }

  /**
   * 获取标签
   * @param props
   * @param state
   * @returns {*}
   * @private
   */
  static _keys(props, state) {
    //如果state中有checkedArray则使用state中的checkedArray
    if (state && state.checkedArray && state.checkedArray.length) {
      return state.checkedArray;
    }
    //否则从原始数据中获取checkedArray
    const flag = SortKeyPage._flag(props);
    let dataArray = props.language[flag] || []; //如果数组为空 则使用空数组
    let keys = [];
    for (let i = 0, j = dataArray.length; i < j; i++) {
      let data = dataArray[i];
      if (data.checked) {
        //============返回checked为true===========
        keys.push(data);
        //============返回checked为true===========
      }
    }
    return keys;
  }
  //=从原始数据中获取checkedArray=
  static _flag(props) {
    const {flag} = props.navigation.state.params;
    return flag === FLAG_LANGUAGE.flag_key ? 'keys' : 'languages';
  }
  //=从原始数据中获取checkedArray=

  //=================保存此次的变更================
  onSave(hasChecked) {
    //======================返回按钮弹出框操作===============
    if (!hasChecked) {
      //如果没有排序则直接返回
      if (
        ArrayUtil.isEqual(
          SortKeyPage._keys(this.props),
          this.state.checkedArray,
        )
      ) {
        //未做任何操作直接返回上一页
        NavigationUtil.goBack(this.props.navigation);
        return;
      }
    }
    //======================返回按钮弹出框操作===============

    //======================右上角保存按钮操作===============

    //保存排序后的数据
    //获取排序后的数据

    //更新本地数据
    this.languageDao.save(this.getSortResult());

    //=============将保存的结果实时同步到最热和趋势的tab标签中=============
    const {onLoadLanguage} = this.props;
    //更新store
    onLoadLanguage(this.params.flag);
    //=============将保存的结果实时同步到最热和趋势的tab标签中=============

    //保存完成后返回上一页
    NavigationUtil.goBack(this.props.navigation);
    //======================右上角保存按钮操作===============
  }
  //=================保存此次的变更================
  render():
    | React.ReactElement<any>
    | string
    | number
    | {}
    | React.ReactNodeArray
    | React.ReactPortal
    | boolean
    | null
    | undefined {
    this.params = this.props.navigation.state.params;
    const {theme} = this.params; //自定义主题取出
    //=========================导入自定义顶部导航栏=======================
    let statusBar = {
      backgroundColor: theme.themeColor,
      barStyle: 'light-content', // 高亮模式
    };
    let title =
      this.params.flag === FLAG_LANGUAGE.flag_language
        ? '语言排序'
        : '标签排序';
    let navigationBar = (
      <NavigationBar
        title={title}
        statusBar={statusBar}
        style={{backgroundColor: theme.themeColor}}
        leftButton={ViewUtil.getLeftBackButton(() => this.onBack())}
        rightButton={ViewUtil.getRightButton('保存', () => this.onSave())}
      />
    );

    return (
      <SafeAreaViewPlus topColor={theme.themeColor}>
        {navigationBar}
        <SortableListView
          data={this.state.checkedArray}
          /*取出所有的keys作为order*/
          order={Object.keys(this.state.checkedArray)}
          /*拖拽结束之后调用*/
          onRowMoved={e => {
            //对应的元素替换到指定位置
            this.state.checkedArray.splice(
              e.to,
              0,
              this.state.checkedArray.splice(e.from, 1)[0],
            );
            //强制更新ui
            this.forceUpdate();
          }}
          renderRow={row => (
            <SortCell data={row} {...this.params} theme={theme} />
          )}
        />
      </SafeAreaViewPlus>
    );
  }
}
//====================================排序=================================
class SortCell extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const {theme} = this.props;
    return (
      <TouchableHighlight
        underlayColor={'#eee'}
        style={this.props.data.checked ? styles.item : styles.hidden}
        {...this.props.sortHandlers}>
        <View style={{marginLeft: 10, flexDirection: 'row'}}>
          <MaterialCommunityIcons
            name={'sort'}
            size={16}
            style={{marginRight: 10, color: theme.themeColor}}
          />
          <Text>{this.props.data.name}</Text>
        </View>
      </TouchableHighlight>
    );
  }
}
//====================================排序=================================

//====================================最热=================================

//====================================onRefreshPopular使用dispatch创建函数 (方法一)  //创建actions和props关联 =================================
function loadData(ts, loadMore, refreshFavorite) {
  const {
    onRefreshPopular,
    onLoadMorePopular,
    onFlushPopularFavorite,
  } = ts.props;
  const store = _store(ts);
  const url = genFetchUrl(ts.storeName);
  if (loadMore) {
    onLoadMorePopular(
      ts.storeName,
      ++store.pageIndex, //==========================翻到下一页
      pageSize,
      store.items,
      favoriteDao,
      callback => {
        ts.refs.toast.show('没有更多了'); //==================action 回调函数
      }, //没有更多数据的情况了
    );
  } else if (refreshFavorite) {
    //=====================刷新收藏页面的收藏状态  同步=================
    onFlushPopularFavorite(
      ts.storeName,
      store.pageIndex,
      pageSize,
      store.items,
      favoriteDao,
    );
  } else {
    // store.pageIndex, //==========================翻到第一页 在action/popular的handleData 方法中定义了pageIndex: 1
    onRefreshPopular(ts.storeName, url, pageSize, favoriteDao); //使用dispatch创建函数 (方法一)  //创建actions和props关联 ，传入storeName和url
  }
}

function genFetchUrl(key) {
  return URL + key + QUERY_STR;
}

function _store(ts) {
  const {popular} = ts.props;
  let store = popular[ts.storeName]; //storeName 不是固定的 需要动态获取
  if (!store) {
    //store为null 则初始化  reducer/popular/index/ 的属性defaultState  没有设置默认值

    store = {
      items: [], //原始数据
      isLoading: false,
      projectModels: [], //要显示的数据
      hideLoadingMore: true, //默认隐藏加载更多
    };
  }
  return store;
}

//====================================onRefreshPopular使用dispatch创建函数 (方法一)  //创建actions和props关联 =================================

//====================================redux + popularPage=================================
const mapPopularStateToProps = state => ({
  language: state.language, //取最外层的节点，既包含keys又包含language
});
const mapPopularDispatchToProps = dispatch => ({
  onLoadLanguage: flag => dispatch(actions.onLoadLanguage(flag)),
});
export default connect(
  mapPopularStateToProps,
  mapPopularDispatchToProps,
)(SortKeyPage);
//====================================redux + popularPage=================================
