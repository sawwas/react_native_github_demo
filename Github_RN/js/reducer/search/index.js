import Types from '../../action/types';

/**
 * 1 创建一个reducer  action 函数
 */
const defaultState = {
  showText: '搜索', // 右上角文案显示默认为搜索
  items: [],
  isLoading: false,
  projectModels: [], //要显示的数据
  hideLoadingMore: true, //默认隐藏加载更多
  showBottomButton: false,
}; //state 默认值 必须创建 否则报错显示不合法的reducer
/**
 *
 *     java:{
 *         items:[],
 *         isLoading:false
 *     },
 *     ios:{
 *         items:[],
 *         isLoading:false
 *     },
 *
 * @param state
 * @param action
 * @returns {{}|{theme: *}}
 */
export default function onAction(state = defaultState, action) {
  switch (action.type) {
    case Types.SEARCH_REFRESH: //下拉刷新搜索数据
      //不能修改state ，只能返回新的state 或之前的state
      return {
        ...state,
        //动态设置store（storekey 不固定）
        isLoading: true,
        hideLoadingMore: true, //是否隐藏底部的加载更多
        pageIndex: action.pageIndex, //===============！！！======== 用于接收PopularPage 设置defaultState 时传入的pageIndex，否则NAN
        showText: '取消', //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~开始搜索时要显示成取消
      }; //不能修改state ，只能返回新的state 或之前的state

    case Types.SEARCH_REFRESH_SUCCESS: //获取数据成功
      return {
        ...state,
        isLoading: false,
        hideLoadingMore: false,
        showBottomButton: action.showBottomButton, //是否显示底部button
        items: action.items,
        projectModels: action.projectModels,
        pageIndex: action.pageIndex, //===============！！！======== 用于接收PopularPage 设置defaultState 时传入的pageIndex，否则NAN
        showText: '搜索', //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~搜索完成时要显示成搜索
        inputKey: action.inputKey,
      }; //复制之前的节点副本 做一个mergin 新的覆盖老的
    case Types.SEARCH_FAIL: //下拉刷新失败
      return {
        ...state,
        isLoading: false,
        showText: '搜索',
        hideLoadingMore: true, //=========！！！==========!!! 下拉刷新失败时必须隐藏 上拉加载ui
      }; //复制之前的节点副本 做一个mergin 新的覆盖老的

    case Types.SEARCH_CANCEL: //取消搜索
      return {
        ...state,
        isLoading: false,
        showText: '搜索',
        hideLoadingMore: true, //=========！！！==========!!! 下拉刷新失败时必须隐藏 上拉加载ui
      };
    case Types.SEARCH_LOAD_MORE_SUCCESS: //上拉加载更多成功
      //不能修改state ，只能返回新的state 或之前的state
      return {
        ...state,
        //动态设置store（storekey 不固定）
        projectModels: action.projectModels,
        hideLoadingMore: false, //是否隐藏底部的加载更多
        pageIndex: action.pageIndex, //===============！！！======== 用于接收PopularPage 设置defaultState 时传入的pageIndex，否则NAN
      }; //不能修改state ，只能返回新的state 或之前的state

    case Types.SEARCH_LOAD_MORE_FAIL: //上拉加载更多失败
      //不能修改state ，只能返回新的state 或之前的state
      return {
        ...state,
        //动态设置store（storekey 不固定）
        hideLoadingMore: true, //是否隐藏底部的加载更多
        pageIndex: action.pageIndex, //===============！！！======== 用于接收PopularPage 设置defaultState 时传入的pageIndex，否则NAN
      }; //不能修改state ，只能返回新的state 或之前的state

    default:
      return state;
  }
}
