/**
 *最热
 */
import React, {Component} from 'react';
import {
  ActivityIndicator,
  FlatList,
  RefreshControl,
  StyleSheet,
  Text,
  View,
  DeviceInfo,
} from 'react-native';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';
import {createAppContainer} from 'react-navigation';
import {PlatformSelects} from '../utils/PlatformSelects';
import {connect} from 'react-redux'; //使得组件和 redux 关联
import actions from '../action/index';
import PopularItem from '../common/PopularItem';
import Toast from 'react-native-easy-toast';
import NavigationBar from '../common/NavigationBar';
import NavigationUtil from '../navigator/NavigationUtil';
import FavoriteDao from '../expand/dao/FavoriteDao';
import {FLAG_STORAGE} from '../expand/dao/DataStore';
import FavoriteUtil from '../util/FavoriteUtil';
import TrendingItem from '../common/TrendingItem';
import EventBus from 'react-native-event-bus';
import EventTypes from '../util/EventTypes';
// import DeviceInfo from 'react-native-device-info';

const URL = 'https://api.github.com/search/repositories?q=';
const QUERY_STR = '&sort=stars';
const pageSize = 10; //每页加载的数量
const styles = StyleSheet.create({
  //去掉：不然导航器显示不出来
  containerForIos: {
    flex: 1,
    marginTop: 30,
    // justifyContent: 'center',去掉：不然导航器显示不出来
    // alignItems: 'center',去掉：不然导航器显示不出来
    // backgroundColor: '#F5FcFF',去掉：不然导航器显示不出来
  },
  containerForAndroid: {
    flex: 1,
    marginTop: 0,
    // justifyContent: 'center',去掉：不然导航器显示不出来
    // alignItems: 'center',去掉：不然导航器显示不出来
    // backgroundColor: '#F5FcFF',去掉：不然导航器显示不出来
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  //  tab 内容最小宽度
  tabStyle: {
    // minWidth: 50, fix 导致tabStyle 初次加载时由小到大的闪烁
    padding: 0, //===================== !!!!!!!!!!!! fix修复初次加载tab由大到小
    alignItems: 'center',
  },
  // tab 指示器样式
  indicatorStyle: {
    height: 2,
    backgroundColor: 'white',
  },
  // tab 文字大小
  labelStyle: {
    fontSize: 13,
    marginTop: -18, //===================== !!!!!!!!!!!! fix修复初次加载tab由大到小
    // marginTop: 6, //===================== !!!!!!!!!!!! fix修复初次加载tab由大到小
    // marginBottom: 6, //===================== !!!!!!!!!!!! fix修复初次加载tab由大到小
  },
  // 上拉加载更多ui样式
  indicatorContainer: {
    alignItems: 'center',
  },
  indicator: {
    color: 'red',
    margin: 10,
  },
});

//====================================定义动态显示的tab顶部导航=================================
let tabNames = ['最热', '趋势'];
let prop = [];
let themes = '';

//====================================顶部导航=================================
/**
 * ======================================顶部导航下的内容======================================
 */
class FavoriteTab extends Component {
  constructor(props) {
    super(props);
    const {flag} = this.props; // root tab 标签的标题 title
    this.storeName = flag;
    this.favoriteDao = new FavoriteDao(flag);
  }

  componentDidMount(): void {
    loadData(this, false);
    //========================EVENTBUS切换底部按钮之前的状态 、 切换之后的状态 的监听================
    EventBus.getInstance().addListener(
      EventTypes.bottom_tab_select,
      (this.listener = data => {
        if (data.to === 2) {
          //收藏页面索引位置为2
          loadData(this, false);
        }
      }),
    );
    //========================EVENTBUS切换底部按钮之前的状态 、 切换之后的状态 的监听================
  }

  componentWillUnmount(): void {
    //========================移除 EVENTBUS切换底部按钮之前的状态 、 切换之后的状态 的监听================

    EventBus.getInstance().removeListener(this.listener);
    //========================移除 EVENTBUS切换底部按钮之前的状态 、 切换之后的状态 的监听================
  }

  render():
    | React.ReactElement<any>
    | string
    | number
    | {}
    | React.ReactNodeArray
    | React.ReactPortal
    | boolean
    | null
    | undefined {
    prop = this.props;
    let store = _store(this); //初始化默认的state

    const {theme} = this.props;

    return (
      <View style={styles.containerForAndroid}>
        {/*<Text*/}
        {/*  onPress={() => {*/}
        {/*    //============================================================================DetailPage 和 HomePage 在同一级，故此使用*/}
        {/*    NavigationUtil.goPage({}, 'DetailPage');*/}
        {/*  }}>*/}
        {/*  FavoriteTab*/}
        {/*</Text>*/}
        <FlatList
          data={store.projectModels}
          keyExtractor={item => '' + (item.item.id || item.item.fullName)} // item 对应projectModel 适配 最热 和趋势模块
          refreshControl={
            <RefreshControl
              title={'Loading'} //刷新文字文案
              titleColor={theme.themeColor} // 刷新文字颜色
              colors={[theme.themeColor]} // 刷新样式颜色
              refreshing={store.isLoading} // 是否刷新的状态
              onRefresh={() => loadData(this, true)}
              tintColor={theme.themeColor}
            />
          }
          renderItem={data => renderItem(this, data)}
        />

        {/*  Toast */}
        <Toast ref={'toast'} position={'center'} />
      </View>
    );
  }
}

//====================================顶部导航=================================

//====================================定义动态显示的tab顶部导航=================================
function genTabs(tabNames, prop) {
  const tabs = {};
  tabNames.forEach((item, index) => {
    //动态设置路由名
    tabs[`tab${index}`] = {
      //给组件动态设置属性
      screen: () => <FavoriteTabPage {...prop} tabLabel={item} />,
      navigationOptions: {
        title: item,
      },
    };
  });
  return tabs;
}

//====================================定义动态显示的tab顶部导航=================================

//====================================收藏=================================
class FavoritePage extends Component {
  constructor(props) {
    super(props);
  }

  render():
    | React.ReactElement<any>
    | string
    | number
    | {}
    | React.ReactNodeArray
    | React.ReactPortal
    | boolean
    | null
    | undefined {
    //+自定义主题
    const {theme} = this.props;
    themes = theme;
    //+自定义主题
    //=========================导入自定义顶部导航栏=======================
    let statusBar = {
      backgroundColor: theme.themeColor,
      barStyle: 'light-content', // 高亮模式
    };
    let navigationBar = (
      <NavigationBar
        title={'最热'}
        statusBar={statusBar}
        style={{backgroundColor: theme.themeColor}}
      />
    );
    //=========================导入自定义顶部导航栏=======================

    //====================================顶部导航(必须在组件里实现否则引起不显示)=================================
    const TabNavigator = createAppContainer(
      createMaterialTopTabNavigator(
        {
          Popular: {
            screen: props => (
              <FavoriteTabPage
                {...props}
                flag={FLAG_STORAGE.flag_popular}
                theme={themes}
              />
            ), //初始化Component时携带默认参数 @https://github.com/react-navigation/react-navigation/issues/2392
            navigationOptions: {
              title: '最热',
            },
          },
          Trending: {
            screen: props => (
              <FavoriteTabPage
                {...props}
                flag={FLAG_STORAGE.flag_trending}
                theme={themes}
              />
            ), //初始化Component时携带默认参数 @https://github.com/react-navigation/react-navigation/issues/2392
            navigationOptions: {
              title: '趋势',
            },
          },
        },
        {
          tabBarOptions: {
            tabStyle: styles.tabStyle,
            upperCaseLabel: false, //是否使标签大写，默认为true
            style: {
              backgroundColor: themes.themeColor, //TabBar 的背景颜色
              height: 30, //fix 开启scrollEnabled后再Android上初次加载时闪烁问题
            },
            indicatorStyle: styles.indicatorStyle, //标签指示器的样式
            labelStyle: styles.labelStyle, //文字的样式
          },
        },
      ),
    );
    //====================================顶部导航(必须在组件里实现否则引起不显示)=================================

    return (
      <View
        style={
          PlatformSelects()
            ? styles.containerForAndroid
            : DeviceInfo.isIPhoneX_deprecated //=========！！！！！！！！！！判断当前设备是否使用iphonex
            ? // ? styles.containerForIos //===================由于设置了SafeAreaViewPlus 在HomePage页面 所以不用marginTop:30
              styles.containerForAndroid
            : styles.containerForAndroid
        }>
        {navigationBar}
        {/*==================将自定义主题传入进去==================*/}
        <TabNavigator />
        {/*==================将自定义主题传入进去==================*/}
      </View>
    );
  }
}
//====================================最热=================================

//====================================onRefreshPopular使用dispatch创建函数 (方法一)  //创建actions和props关联 =================================
function loadData(ts, isShowLoading) {
  const {onLoadFavoriteData} = ts.props;
  onLoadFavoriteData(ts.storeName, isShowLoading);
}

// function genFetchUrl(key) {
//   return URL + key + QUERY_STR;
// }

function _store(ts) {
  const {favorite} = ts.props;
  let store = favorite[ts.storeName]; //storeName 不是固定的 需要动态获取
  if (!store) {
    //store为null 则初始化  reducer/popular/index/ 的属性defaultState  没有设置默认值

    store = {
      items: [], //原始数据
      isLoading: false,
      projectModels: [], //要显示的数据
    };
  }
  return store;
}

//====================================onRefreshPopular使用dispatch创建函数 (方法一)  //创建actions和props关联 =================================

//====================================flatlist item ui=================================
function renderItem(ts, data) {
  const {theme} = ts.props;
  const item = data.item;
  // 判断显示的item 属于最热还是趋势
  const Item =
    ts.storeName === FLAG_STORAGE.flag_popular ? PopularItem : TrendingItem;
  return (
    //===================自定义UI Component================
    <Item
      theme={theme}
      projectModel={item} //传递到父类BaseItem
      onSelect={callback => {
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~跳转到H5详情页~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        NavigationUtil.goPage(
          {
            theme, //========================!!!!!!!!!!!!将自定义主题属性传入 h5 页面
            projectModel: item,
            flag: ts.storeName,
            callback,
          }, //传递收藏状态flag
          'DetailPage',
        );
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~跳转到H5详情页~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      }}
      onFavorite={(
        item,
        isFavorite, //父类BaseItem 传回
      ) => onFavorite(ts, item, isFavorite)}
    />
    //===================自定义UI Component================
  );
}

//====================================flatlist item ui=================================

//====================================添加 移除收藏 + favorite发生变化的通知=================================
function onFavorite(ts, item, isFavorite) {
  FavoriteUtil.onFavorite(ts.favoriteDao, item, isFavorite, ts.storeName); //!!!!!!!!!!数据库注意保存不要到其他的数据库去了！！！
  if (ts.storeName === FLAG_STORAGE.flag_popular) {
    // 最热  收藏   变化 通知 PopularPage对应的EventBus监听器
    EventBus.getInstance().fireEvent(EventTypes.favorite_changed_popular);
  } else if (ts.storeName === FLAG_STORAGE.flag_trending) {
    // 趋势  收藏   变化 通知 TrendingPage对应的EventBus监听器
    EventBus.getInstance().fireEvent(EventTypes.favoriteChanged_trending);
  }
}
//====================================添加 移除收藏 + favorite发生变化的通知=================================
//====================================redux flatlist=================================

const mapStateToProps = state => ({
  //整个store树里面的favorite的state
  favorite: state.favorite,
});
const mapDispatchToProps = dispatch => ({
  //====================dispatch action创建函数
  onLoadFavoriteData: (storeName, isLoading) =>
    //下拉刷新
    dispatch(actions.onLoadFavoriteData(storeName, isLoading)), //创建actions和props关联 ，传入storeName和url
});
const FavoriteTabPage = connect(
  mapStateToProps,
  mapDispatchToProps,
)(FavoriteTab); //第二个参数就是返回值  （用于让组件订阅store树）
//====================================redux flatlist=================================

//====================================redux FavoritePage=================================
const mapStateFavoritePageToProps = state => ({
  theme: state.theme.theme, //订阅自定义主题
});
export default connect(mapStateFavoritePageToProps)(FavoritePage);

//====================================redux FavoritePage=================================
