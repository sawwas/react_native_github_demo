import PopularPage from '../page/PopularPage';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import TrendingPage from '../page/TrendingPage';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FavoritePage from '../page/FavoritePage';
import MyPage from '../page/MyPage';
import Entypo from 'react-native-vector-icons/Entypo';
import React, {Component} from 'react';
import {BottomTabBar, createBottomTabNavigator} from 'react-navigation-tabs';
import {createAppContainer} from 'react-navigation';
import {connect} from 'react-redux';
import EventBus from 'react-native-event-bus';
import EventTypes from '../util/EventTypes';
//=============================================这里配置页面的路由=============================================
const TABS = {
  PopularPage: {
    screen: PopularPage,
    navigationOptions: {
      tabBarLabel: '最热',
      tabBarIcon: ({tintColor, focused}) => (
        <MaterialIcons name={'whatshot'} size={26} style={{color: tintColor}} />
      ),
    },
  },
  TrendingPage: {
    screen: TrendingPage,
    navigationOptions: {
      tabBarLabel: '趋势',
      tabBarIcon: ({tintColor, focused}) => (
        <Ionicons
          name={'md-trending-up'}
          size={26}
          style={{color: tintColor}}
        />
      ),
    },
  },
  FavoritePage: {
    screen: FavoritePage,
    navigationOptions: {
      tabBarLabel: '收藏',
      tabBarIcon: ({tintColor, focused}) => (
        <MaterialIcons name={'favorite'} size={26} style={{color: tintColor}} />
      ),
    },
  },
  MyPage: {
    screen: MyPage,
    navigationOptions: {
      tabBarLabel: '我的',
      tabBarIcon: ({tintColor, focused}) => (
        <Entypo name={'user'} size={26} style={{color: tintColor}} />
      ),
    },
  },
};
function tabNavigator(ts, prop) {
  //优化频繁创建导航器 ： 如果导航器存在直接返回  BUG :如果改变主题 不会显示最新状态 需要注释掉才行  最新替换方法 使用this
  // if (Tabs !== '') {
  //   return Tabs;
  // }
  // if (ts.Tabs) {
  //   return ts.Tabs;
  // }

  //模拟服务端下发动态配置底部数目
  const {PopularPage, TrendingPage, FavoritePage, MyPage} = TABS;
  const tabs = {PopularPage, TrendingPage, FavoritePage, MyPage};
  //动态修改底部导航文字
  PopularPage.navigationOptions.tabBarLabel = '最热'; //动态修改tab 属性
  return (ts.Tabs = createAppContainer(
    createBottomTabNavigator(tabs, {
      //设置底部导航颜色 没有使用setParams 修改主题不会有作用
      tabBarComponent: props => {
        //携带主题参数到props theme={prop.theme} 必须加上 {...props}否则 Cannot read property 'state' of undefined
        return <TabBarComponent {...props} theme={prop.theme} />;
      },
    }),
  ));
}
//=============================================这里配置页面的路由=============================================

//=============================================这里设置底部导航组件=============================================
class TabBarComponent extends Component {
  render():
    | React.ReactElement<any>
    | string
    | number
    | {}
    | React.ReactNodeArray
    | React.ReactPortal
    | boolean
    | null
    | undefined {
    //重写bottomTabBar
    return (
      <BottomTabBar
        // 将它之前的属性赋值给它 如果注释掉 报 props.theme undefined object 没有找到navigation
        {...this.props}
        //优先从主题里面去颜色 如果主题里面没有去属性里面的颜色
        // activeTintColor={this.theme.tintColor || this.props.activeTintColor}
        activeTintColor={this.props.theme.themeColor} //========~~~~~~~~@@@@@@@@@使用自定义主题=============
      />
    );
  }
}
//=============================================这里设置底部导航组件=============================================

class DynamicTabNavigator extends Component {
  constructor(props) {
    super(props);
    console.disableYellowBox = true; //关闭黄色警告弹框
  }

  render():
    | React.ReactElement<any>
    | string
    | number
    | {}
    | React.ReactNodeArray
    | React.ReactPortal
    | boolean
    | null
    | undefined {
    // return tabNavigator();//BUG 应该使用jsx语法
    const Tab = tabNavigator(this, this.props);
    //正确写法
    return (
      // ================================================================{/*底部tab按钮发生切换时调用*/}
      <Tab
        onNavigationStateChange={(prevState, newState, action) => {
          //========================EVENTBUS切换之前的状态 、 切换之后的状态
          EventBus.getInstance().fireEvent(EventTypes.bottom_tab_select, {
            //发生底部tab切换的事件
            from: prevState.index,
            to: newState.index,
          });
          //========================EVENTBUS切换之前的状态 、 切换之后的状态
        }}
      />
    );
  }
}
//============================================redux的state到页面的props转换======================================
const mapStateToProps = state => ({
  theme: state.theme.theme, //redux 里面的theme 赋值给当前props的theme
});
export default connect(mapStateToProps)(DynamicTabNavigator); //当前组件到redux 的store连接
//============================================redux的state到页面的props转换======================================
