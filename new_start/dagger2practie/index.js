/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import App2 from './js/page/App2';
import App1 from './js/page/App1';
import CodePushPage from './js/page/CodePushPage';
import appName from './app.json';

AppRegistry.registerComponent(appName.name, () => App);
AppRegistry.registerComponent(appName.welcome, () => App1);
AppRegistry.registerComponent(appName.ReactPages, () => App2);
AppRegistry.registerComponent(appName.CodePushPage, () => CodePushPage);
