import Types from '../../action/types';

/**
 * 1 创建一个reducer  action 函数
 */
const defaultState = {}; //state 默认值 必须创建 否则报错显示不合法的reducer
/**
 * favorite:{
 *     popular:{
 *         projectModels:[],
 *         isLoading:false
 *     },
 *     trending:{
 *         projectModels:[],
 *         isLoading:false
 *     },
 *
 * }
 * @param state
 * @param action
 * @returns {{}|{theme: *}}
 */
export default function onAction(state = defaultState, action) {
  switch (action.type) {
    case Types.FAVORITE_LOAD_DATA: //获取收藏数据
      //不能修改state ，只能返回新的state 或之前的state
      return {
        ...state,
        //动态设置store（storekey 不固定）
        [action.storeName]: {
          ...state[action.storeName], //====================!!!===========================...延展修饰符 必须使用 否则每次刷新会白一下=================
          isLoading: true,
        },
      }; //不能修改state ，只能返回新的state 或之前的state

    case Types.FAVORITE_LOAD_SUCCESS: //下拉获取数据成功时
      return {
        ...state,
        [action.storeName]: {
          ...state[action.storeName],
          isLoading: false,
          projectModels: action.projectModels, // 此次要展示的数据
        },
      }; //复制之前的节点副本 做一个mergin 新的覆盖老的
    case Types.FAVORITE_LOAD_FAIL: //下拉刷新失败
      return {
        ...state,
        [action.storeName]: {
          ...state[action.storeName],
          isLoading: false,
        },
      }; //复制之前的节点副本 做一个mergin 新的覆盖老的

    default:
      return state;
  }
}
