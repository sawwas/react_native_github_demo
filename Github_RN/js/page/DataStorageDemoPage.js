/**
 *DataStorageDemo
 */
import React, {Component} from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';
import DataStore from '../expand/dao/DataStore';

//=============================================离线缓存框架=============================================

const KEY = 'save_key';

function loadData(ts) {
  let url = 'https://api.github.com/search/repositories?q=' + `${ts.value}`;
  ts.dataStore
    .fetchData(url)
    .then(data => {
      let showData = `初次数据加载时间：${new Date(
        data.timestamp,
      )}\n${JSON.stringify(data.data)}`;
      ts.setState({
        showText: showData,
      });
    })
    .catch(error => {
      error && console.log(error.toString());
    });
}

//=============================================离线缓存框架=============================================

export default class DataStorageDemoPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showText: '', //代表存储结果
    };
    this.dataStore = new DataStore();
  }

  render():
    | React.ReactElement<any>
    | string
    | number
    | {}
    | React.ReactNodeArray
    | React.ReactPortal
    | boolean
    | null
    | undefined {
    const {navigation} = this.props;

    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>离线缓存框架设计</Text>
        <TextInput
          style={styles.input}
          onChangeText={text => {
            this.value = text; //接收用户输入的文字
          }}
        />

        <Text
          onPress={() => {
            loadData(this);
          }}>
          获取
        </Text>
        <Text style={styles.welcome}>{this.state.showText}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#F5FcFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  input: {
    height: 50,
    // flex: 1,
    borderColor: 'black',
    borderWidth: 1,
    marginRight: 10,
  },
  input_container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
