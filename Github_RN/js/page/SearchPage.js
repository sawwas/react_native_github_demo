/**
 *最热
 */
import React, {Component} from 'react';
import {
  ActivityIndicator,
  FlatList,
  Platform,
  TextInput,
  RefreshControl,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  DeviceInfo,
} from 'react-native';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';
import {createAppContainer} from 'react-navigation';
import {PlatformSelects} from '../utils/PlatformSelects';
import {connect} from 'react-redux'; //使得组件和 redux 关联
import actions from '../action/index';
import PopularItem from '../common/PopularItem';
import Toast from 'react-native-easy-toast';
import NavigationBar from '../common/NavigationBar';
import NavigationUtil from '../navigator/NavigationUtil';
import FavoriteDao from '../expand/dao/FavoriteDao';
import {FLAG_STORAGE} from '../expand/dao/DataStore';
import FavoriteUtil from '../util/FavoriteUtil';
import EventBus from 'react-native-event-bus';
import EventTypes from '../util/EventTypes';
// import DeviceInfo from 'react-native-device-info';
import {FLAG_LANGUAGE} from '../expand/dao/LanguageDao';
import BackPressComponent from '../common/BackPressComponent';
import LanguageDao from '../expand/dao/LanguageDao';
import GlobalStyles from '../res/styles/GlobalStyles';
import ViewUtil from '../util/ViewUtil';
import Utils from '../util/Utils';

const URL = 'https://api.github.com/search/repositories?q=';
const QUERY_STR = '&sort=stars';
const pageSize = 10; //每页加载的数量
const favoriteDao = new FavoriteDao(FLAG_STORAGE.flag_popular); // 实例化一个收藏对象

const styles = StyleSheet.create({
  //去掉：不然导航器显示不出来
  containerForIos: {
    flex: 1,
    marginTop: 30,
    // justifyContent: 'center',去掉：不然导航器显示不出来
    // alignItems: 'center',去掉：不然导航器显示不出来
    // backgroundColor: '#F5FcFF',去掉：不然导航器显示不出来
  },
  containerForAndroid: {
    flex: 1,
    marginTop: 0,
    // justifyContent: 'center',去掉：不然导航器显示不出来
    // alignItems: 'center',去掉：不然导航器显示不出来
    // backgroundColor: '#F5FcFF',去掉：不然导航器显示不出来
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  //  tab 内容最小宽度
  tabStyle: {
    // minWidth: 50, fix 导致tabStyle 初次加载时由小到大的闪烁
    padding: 0, //===================== !!!!!!!!!!!! fix修复初次加载tab由大到小
    alignItems: 'center',
  },
  // 搜索加载过程中的指示器样式
  centering: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  // tab 文字大小
  labelStyle: {
    fontSize: 13,
    marginTop: -18, //===================== !!!!!!!!!!!! fix修复初次加载tab由大到小
    // marginTop: 6, //===================== !!!!!!!!!!!! fix修复初次加载tab由大到小
    // marginBottom: 6, //===================== !!!!!!!!!!!! fix修复初次加载tab由大到小
  },
  // 上拉加载更多ui样式
  indicatorContainer: {
    alignItems: 'center',
  },
  indicator: {
    color: 'red',
    margin: 10,
  },
  statusBar: {
    height: 20,
  },
  //朕收下了 按钮样式
  bottomButton: {
    alignItems: 'center',
    justifyContent: 'center',
    opacity: 0.9,
    height: 40,
    position: 'absolute',
    left: 10,
    top: GlobalStyles.window_height - 10,
    right: 10,
    borderRadius: 3,
  },
  //====输入框样式
  textInput: {
    flex: 1,
    height: Platform.OS === 'ios' ? 26 : 36,
    borderWidth: Platform.OS === 'ios' ? 1 : 0,
    borderColor: 'white',
    alignSelf: 'center',
    paddingLeft: 5,
    marginRight: 10,
    marginLeft: 5,
    borderRadius: 3,
    opacity: 0.7,
    color: 'white',
  },
  //右上角搜索按钮文字样式
  title: {
    fontSize: 16,
    fontWeight: '500',
    color: 'white',
  },
});
//====================================搜索=================================
class SearchPage extends Component {
  constructor(props) {
    super(props);
    this.params = this.props.navigation.state.params;
    this.backPress = new BackPressComponent({
      backPress: () => this.onBackPress(),
    });
    this.favoriteDao = new FavoriteDao(FLAG_STORAGE.flag_popular);
    this.languageDao = new LanguageDao(FLAG_LANGUAGE.flag_key);
    this.isKeyChange = false; // 用于判断 朕收下了 是否做了添加操作
  }
  /**
   * 处理 Android 中的物理返回键
   * https://reactnavigation.org/docs/en/redux-integration.html#handling-the-hardware-back-button-in-android
   * @returns {boolean}
   */
  onBackPress() {
    const {onSearchCancel, onLoadLanguage} = this.props;
    onSearchCancel(); //退出时取消搜索
    this.refs.input.blur(); //============================收起键盘========================
    //===============返回堆栈的上一页================
    NavigationUtil.goBack(this.props.navigation);
    if (this.isKeyChange) {
      //当 点击了朕收下了按钮之后 要调用该方法重新渲染最热模块页的tabs标签
      onLoadLanguage(FLAG_LANGUAGE.flag_key); //重新加载最热的tabs标签
    }
    return true; //===============================防止返回事件继续传递下去=============！！！！！！！！！！！！！！！
  }
  //==============返回监听==============

  componentDidMount(): void {
    this.backPress.componentDidMount();
  }
  componentWillUnmount(): void {
    this.backPress.componentWillUnmount();
  }

  //==============返回监听==============

  //==============上拉加载更多ui==============
  genIndicator(ts) {
    const {search} = ts.props;
    return search.hideLoadingMore ? null : (
      <View style={styles.indicatorContainer}>
        <ActivityIndicator style={styles.indicator} />
        <Text>正在加载更多</Text>
      </View>
    );
  }
  //==============上拉加载更多ui==============

  //==============添加标签key==============
  saveKey() {
    const {keys} = this.props;
    let key = this.inputKey;
    if (Utils.checkKeyIsExist(keys, key)) {
      this.toast.show(key + '已经存在');
    } else {
      key = {
        path: key,
        name: key,
        checked: true,
      };
      keys.unshift(key); //将key添加到数组的开头
      this.languageDao.save(keys);
      this.toast.show(key.name + '保存成功');
      this.isKeyChange = true;
    }
  }
  //==============添加标签key==============

  //==============右上角搜索action==============
  onRightButtonClick() {
    const {onSearchCancel, search} = this.props;
    if (search.showText === '搜索') {
      loadData(this, false);
    } else {
      onSearchCancel(this.searchToken);
    }
  }
  //==============右上角搜索action==============

  //==============顶部导航栏ui设置==============
  renderNavBar() {
    const {theme} = this.params;
    const {showText, inputKey} = this.props.search;
    const placeholder = inputKey || '请输入';
    let backButton = ViewUtil.getLeftBackButton(() => this.onBackPress());
    let inputView = (
      <TextInput
        ref="input"
        placeholder={placeholder}
        onChangeText={text => (this.inputKey = text.toString().trim())}
        style={styles.textInput}
      />
    );
    let rightButton = (
      <TouchableOpacity
        onPress={() => {
          this.refs.input.blur(); //收起键盘
          this.onRightButtonClick();
        }}>
        <View style={{marginRight: 10}}>
          <Text style={styles.title}>{showText}</Text>
        </View>
      </TouchableOpacity>
    );
    return (
      <View
        style={{
          backgroundColor: theme.themeColor,
          flexDirection: 'row',
          alignItems: 'center',
          height:
            Platform.OS === 'ios'
              ? GlobalStyles.nav_bar_height_ios
              : GlobalStyles.nav_bar_height_android,
        }}>
        {backButton}
        {inputView}
        {rightButton}
      </View>
    );
  }
  //==============顶部导航栏ui设置==============

  render():
    | React.ReactElement<any>
    | string
    | number
    | {}
    | React.ReactNodeArray
    | React.ReactPortal
    | boolean
    | null
    | undefined {
    //==============取出搜索的属性
    const {
      isLoading,
      projectModels,
      showBottomButton,
      hideLoadingMore,
    } = this.props.search;
    // + 取出自定义主题
    const {theme} = this.props;
    // + 取出自定义主题
    //=========================导入自定义顶部导航栏=======================
    // let statusBar = {
    //   backgroundColor: theme.themeColor,
    //   barStyle: 'light-content', // 高亮模式
    // };
    let statusBar = null;
    if (Platform.OS === 'ios') {
      statusBar = (
        <View style={[styles.statusBar, {backgroundColor: theme.themeColor}]} />
      );
    }
    // let navigationBar = (
    //   <NavigationBar
    //     title={'最热'}
    //     statusBar={statusBar}
    //     style={theme.styles.navBar}
    //   />
    // );

    //=========================搜索 在没有刷新时显示列表 在刷新时显示null=======================
    let listView = !isLoading ? (
      <FlatList
        data={projectModels}
        keyExtractor={item => '' + item.item.id} // item 对应projectModel
        contentInset={{
          bottom: 45, //防止 底部按钮遮住列表item ui
        }}
        refreshControl={
          <RefreshControl
            title={'Loading'} //刷新文字文案
            titleColor={theme.themeColor} // 刷新文字颜色
            colors={[theme.themeColor]} // 刷新样式颜色
            refreshing={isLoading} // 是否刷新的状态
            onRefresh={() => loadData(this, false)}
            tintColor={theme.themeColor}
          />
        }
        renderItem={data => renderItem(data, this)}
        ListFooterComponent={() => this.genIndicator(this)} //上拉加载更多UI组件
        onEndReached={() => {
          console.log('============onEndReached===========');

          //=======修复上拉刷新调用2次和第一次进入页面时不用触发上拉加载 loadMore为true的问题--伴随reducer修改========================
          setTimeout(() => {
            //==========!!!===========延迟100毫秒再触发上拉加载，避免一直在打圈圈ui--onMomentumScrollBegin执行比较晚的情况
            if (this.canLoadMore) {
              loadData(this, true);
              this.canLoadMore = false;
            }
          }, 100);
        }}
        onEndReachedThreshold={0.5} //列表可见长度距离底部的比值
        onMomentumScrollBegin={() => {
          //列表刚开始滚动的时候让上拉加载设置为true
          this.canLoadMore = true;
          console.log('============onMomentumScrollBegin===========');
        }}
        //=======修复上拉刷新调用2次和第一次进入页面时不用触发上拉加载 loadMore为true的问题--伴随reducer修改========================
      />
    ) : null;
    //=========================搜索 在没有刷新时显示列表 在刷新时显示null=======================

    //=========================底部 朕收下了的按钮=======================

    let bottomButton = showBottomButton ? (
      <TouchableOpacity
        style={[styles.bottomButton, {backgroundColor: theme.themeColor}]}
        onPress={() => {
          this.saveKey();
        }}>
        <View style={{justifyContent: 'center'}}>
          <Text style={styles.title}>朕收下了</Text>
        </View>
      </TouchableOpacity>
    ) : null;

    //=========================底部 朕收下了的按钮=======================

    //===========搜索过程ui处理===========

    //===========搜索过程中等待加载的指示器=======
    let indicatorView = isLoading ? (
      <ActivityIndicator
        style={styles.centering}
        size="large"
        animating={isLoading}
      />
    ) : null;
    //===========搜索过程中等待加载的指示器=======

    let resultView = (
      <View style={{flex: 1, marginBottom: 60}}>
        {indicatorView}
        {listView}
      </View>
    );
    //===========搜索过程ui处理===========
    return (
      <View
        style={
          PlatformSelects()
            ? styles.containerForAndroid
            : DeviceInfo.isIPhoneX_deprecated //=========！！！！！！！！！！判断当前设备是否使用iphonex
            ? styles.containerForIos
            : styles.containerForAndroid
        }>
        {statusBar}
        {this.renderNavBar()}
        {resultView}
        {bottomButton}
        {/*当自定义标签keys大于0 才使用tabs控件 此处判断TabNavigator是否为null*/}
        <Toast ref={toast => (this.toast = toast)} position={'center'} />
      </View>
    );
  }
}

//====================================最热=================================

//====================================onRefreshPopular使用dispatch创建函数 (方法一)  //创建actions和props关联 =================================
function loadData(ts, loadMore) {
  const {onLoadMoreSearch, onSearch, search, keys} = ts.props;
  if (loadMore) {
    //加载更多
    onLoadMoreSearch(
      ++search.pageIndex,
      pageSize,
      search.items,
      ts.favoriteDao,
      callback => {
        ts.toast.show('没有更多了'); //==================action 回调函数 (!!不能使用ts.refs.toast 因为refs已经设置为this.toast)
      }, //没有更多数据的情况了
    );
  } else {
    //发起搜索
    onSearch(
      ts.inputKey,
      pageSize,
      (ts.searchToken = new Date().getTime()), //设置当前的时间戳作为唯一的token
      ts.favoriteDao,
      keys,
      message => {
        ts.toast.show(message); //==================action 回调函数
      },
    ); //使用dispatch创建函数 (方法一)  //创建actions和props关联 ，传入storeName和url
  }
}
//====================================onRefreshPopular使用dispatch创建函数 (方法一)  //创建actions和props关联 =================================

//====================================flatlist item ui=================================
function renderItem(data, ts) {
  const item = data.item;
  const {theme} = ts.params; //==================取出自定义主题=================
  //============查看返回的全部数据
  // <View style={{marginBottom: 10}}>
  //   <Text style={{backgroundColor: '#faa'}}>{JSON.stringify(item)}</Text>
  // </View>
  //============查看返回的全部数据

  return (
    //===================自定义UI Component================
    <PopularItem
      theme={theme} //定义自定义主题
      projectModel={item} //传递到父类BaseItem
      onSelect={callback => {
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~跳转到H5详情页~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        NavigationUtil.goPage(
          {
            theme, //========================!!!!!!!!!!!!将自定义主题属性传入 h5 页面
            projectModel: item,
            flag: FLAG_STORAGE.flag_popular,
            callback,
          }, //传递收藏状态flag
          'DetailPage',
        );
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~跳转到H5详情页~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      }}
      onFavorite={(
        item,
        isFavorite, //父类BaseItem 传回
      ) =>
        FavoriteUtil.onFavorite(
          favoriteDao,
          item,
          isFavorite,
          FLAG_STORAGE.flag_popular,
        )
      }
    />
    //===================自定义UI Component================
  );
}

//====================================flatlist item ui=================================

//====================================redux + SearchPage=================================
const mapSearchStateToProps = state => ({
  search: state.search,
  keys: state.language.keys,
  theme: state.theme.theme, //订阅自定义主题
});
const mapSearchDispatchToProps = dispatch => ({
  onSearch: (inputKey, pageSize, token, favoriteDao, popularKeys, callBack) =>
    dispatch(
      actions.onSearch(
        inputKey,
        pageSize,
        token,
        favoriteDao,
        popularKeys,
        callBack,
      ),
    ),
  onSearchCancel: token => dispatch(actions.onSearchCancel(token)),
  onLoadMoreSearch: (pageIndex, pageSize, dataArray, favoriteDao, callBack) =>
    dispatch(
      actions.onLoadMoreSearch(
        pageIndex,
        pageSize,
        dataArray,
        favoriteDao,
        callBack,
      ),
    ),
  onLoadLanguage: flag => dispatch(actions.onLoadLanguage(flag)),
});
export default connect(
  mapSearchStateToProps,
  mapSearchDispatchToProps,
)(SearchPage);
//====================================redux + SearchPage=================================
