/**
 * 主页首页
 */
import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import DynamicTabNavigator from '../navigator/DynamicTabNavigator';
import NavigationUtil from '../navigator/NavigationUtil';
import BackPressComponent from '../common/BackPressComponent';
import actions from '../action';
import {NavigationActions} from 'react-navigation';
import CustomTheme from './CustomTheme';
import {connect} from 'react-redux';
import SafeAreaViewPlus from '../common/SafeAreaViewPlus';

class HomePage extends Component {
  constructor(props) {
    super(props);
    this.backPress = new BackPressComponent({backPress: this.onBackPress});
  }

  componentDidMount(): void {
    this.backPress.componentDidMount();
  }
  componentWillUnmount(): void {
    this.backPress.componentWillUnmount();
  }

  /**
   * 处理 Android 中的物理返回键
   * https://reactnavigation.org/docs/en/redux-integration.html#handling-the-hardware-back-button-in-android
   * @returns {boolean}
   */
  onBackPress = () => {
    const {dispatch, nav} = this.props;
    // if (nav.index === 0) {
    if (nav.routes && nav.routes[1].index === 0) {
      //如果RootNavigator中的MainNavigator的index为0，则不处理返回事件
      return false;
    }
    dispatch(NavigationActions.back());
    return true;
  };

  //============================Android物理返回键监听=========================

  //============================自定义主题弹出框=========================
  renderCustomThemeView() {
    const {customThemeViewVisible, onShowCustomThemeView} = this.props;
    return (
      <CustomTheme
        visible={customThemeViewVisible}
        {...this.props}
        onClose={() => onShowCustomThemeView(false)}
      />
    );
  }
  //============================自定义主题弹出框=========================

  /**
   * 获取底部导航器
   * @returns {NavigationContainer}
   */
  render():
    | React.ReactElement<any>
    | string
    | number
    | {}
    | React.ReactNodeArray
    | React.ReactPortal
    | boolean
    | null
    | undefined {
    // const Tab = this.tabNavigator();
    // return <Tab />;
    //FIX DynamicTabNavigator中的页面无法跳转到外层导航器页面的问题
    NavigationUtil.navigation = this.props.navigation;
    const {theme} = this.props;

    return (
      // <View style={{flex: 1}}>
      //  顶部状态栏颜色
      <SafeAreaViewPlus topColor={theme.themeColor}>
        <DynamicTabNavigator />
        {this.renderCustomThemeView()}
      </SafeAreaViewPlus>

      /*</View>*/
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FcFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});

//============================自定义主题弹出框 + redux=========================

const mapStateToProps = state => ({
  nav: state.nav,
  customThemeViewVisible: state.theme.customThemeViewVisible, //订阅action
  theme: state.theme.theme,
});
const mapDispatchToProps = dispatch => ({
  onShowCustomThemeView: show => dispatch(actions.onShowCustomThemeView(show)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HomePage);
//============================自定义主题弹出框 + redux=========================
