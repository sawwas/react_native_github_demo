import React, {Component} from 'react';
import {
  Modal,
  Text,
  TouchableOpacity,
  StyleSheet,
  View,
  Platform,
  DeviceInfo,
} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import TimeSpan from '../model/TimeSpan';

//弹框选择数据常量
export const TimeSpans = [
  new TimeSpan('今 天', '?since=daily'),
  new TimeSpan('本 周', '?since=weekly'),
  new TimeSpan('本 月', '?since=monthly'),
];

/**
 * 趋势 时间筛选弹出框 UI 布局
 */
export default class TrendingDialog extends Component {
  state = {
    visible: false,
  };

  show() {
    this.setState({
      visible: true,
    });
  }

  dismiss() {
    this.setState({
      visible: false,
    });
  }

  render():
    | React.ReactElement<any>
    | string
    | number
    | {}
    | React.ReactNodeArray
    | React.ReactPortal
    | boolean
    | null
    | undefined {
    const {onClose, onSelect} = this.props; //当用户关闭时 选择时 回调========================
    return (
      <Modal
        transparent={true}
        visible={this.state.visible}
        onRequestClose={() => onClose} //用户点击返回键调用
      >
        <TouchableOpacity
          onPress={() => this.dismiss()} //用户点击弹窗外部区域
          style={styles.container}>
          <MaterialIcons
            name={'arrow-drop-up'}
            size={36}
            style={styles.arrow}
          />
          <View style={styles.content}>
            {TimeSpans.map((result, i, arr) => {
              return (
                //用户点击弹窗内部item
                <TouchableOpacity
                  key={i} //需要有key 标识位置
                  onPress={() => onSelect(arr[i])}
                  underlayColor="red" //用户按下去的颜色
                >
                  <View style={styles.text_container}>
                    <Text style={styles.text}>{arr[i].showText}</Text>
                  </View>
                  {/*//!!!!!!!!!!===============设置下划线==================*/}
                  {i !== TimeSpans.length - 1 ? (
                    <View style={styles.line} />
                  ) : null}
                </TouchableOpacity>
              );
            })}
          </View>
        </TouchableOpacity>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  //=====设置灰色蒙版==============
  container: {
    backgroundColor: 'rgba(0,0,0,0.6)',
    flex: 1,
    alignItems: 'center',
    paddingTop: DeviceInfo.isIPhoneX_deprecated ? 30 : 0,
  },
  //=====设置带箭头背景=============

  arrow: {
    marginTop: 40,
    color: 'white',
    padding: 0,
    margin: -15,
  },
  //=====弹框内容布局=============
  content: {
    backgroundColor: 'white',
    borderRadius: 3,
    paddingTop: 3,
    paddingBottom: 3,
    marginRight: 3,
  },
  //=====弹框文字布局=============
  text_container: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  //=====弹框文字样式=============
  text: {
    fontSize: 16,
    color: 'black',
    fontWeight: '400', //文字粗细
    padding: 8,
    paddingLeft: 26,
    paddingRight: 26,
  },
  //=====弹框文字下划线样式=============
  line: {
    height: 0.3,
    backgroundColor: 'darkgray',
  },
});
